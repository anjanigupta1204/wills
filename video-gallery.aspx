﻿<%@ page language="C#" autoeventwireup="true" inherits="video_gallery, App_Web_b26udrjz" %>

<%@ Register TagPrefix="uc" TagName="navigation" Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Welcome to Wills Lifestyle</title>
    <meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
    <meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
    <!--Favicon for the website-->
    <link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
    <!--Main CSS, containing struction and formating-->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
        rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/loader.css">
</head>
<body>
    <form id="form1" runat="server">
    <uc:navigation ID="navigation1" runat="server" />
    <!--#########################Body Start Here#########################-->
    <div class="pageWidth">
        <section id="wrapper">

                <div class="container-fluid padding-0">
                    <div class="row">
                        <div class="col-md-8 padding-0">
                            <div class="about-left">
                                <div class="leftinner">
                                    <h1 class="black35">Video Gallery

                                    <!--
                                                <div class="videolist">
                                                    <div class="dropdown">
                                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Atumn Winter'12
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#">Atumn Winter'13</a></li>
                                        <li><a href="#">Atumn Winter'14</a></li>
                                      </ul>
                                    </div>
                                                </div> -->

                                    </h1>


                                    <nav class="famTreb pbprc">
                                        <a href="index.aspx">Home</a> | Video Gallery
                                    </nav>
                                   

                                    <div class="about-content content">


                                        <div class="videocontainer">


                                            <!--<div id="example3" class="slider-pro">
                                                <div class="sp-slides">
                                                    <asp:Repeater ID="rptVideo" runat="server">
                                                        <ItemTemplate>
                                                            <div class="sp-slide">
                                                           <div class="videogal-text">
                                        <p> <%#  Eval("VideoTitle")%></p>
                                    </div>

                                                                <iframe class="sp-video" src='<%# string.Concat( "https://www.youtube.com/embed/", Eval("VideoYoutubeId"),"?enablejsapi=1&theme=light&showinfo=0")%>' frameborder="0" allowfullscreen></iframe>

                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </div>

                                                <div class="sp-thumbnails">
                                                    <asp:Repeater ID="rptThumb" runat="server">
                                                        <ItemTemplate>
                                                            <img class="sp-thumbnail" src='<%# string.Concat( "http://img.youtube.com/vi/", Eval("VideoYoutubeId"),"/mqdefault.jpg")%>'/>
                                                               
                                                           
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </div>-->
                                            <div id="example3" class="slider-pro">
                                                <div class="sp-slides">
                                                  
                                                       <div class="sp-slide">
                                                        <div class="videogal-text">
                                                            <p> The Water Linen Collection - Wills Lifestyle </p>
                                                        </div>
                                                        <iframe class="sp-video" src='https://www.youtube.com/embed/N8Vk_mwnpTY?enablejsapi=1&theme=light&showinfo=0' frameborder="0" allowfullscreen></iframe>
                                                    </div>


                                                    <div class="sp-slide">
                                                        <div class="videogal-text">
                                                            <p> THE DUBLINER - Wills Lifestyle</p>
                                                        </div>
                                                        <iframe class="sp-video" src='https://www.youtube.com/embed/oaP3qnHoXxw?enablejsapi=1&theme=light&showinfo=0' frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                    
                                                    <div class="sp-slide">
                                                        <div class="videogal-text">
                                                            <p> Exude Charisma - Wills Lifestyle</p>
                                                        </div>
                                                         <iframe class="sp-video" src='https://www.youtube.com/embed/y40MWzSEKyc?enablejsapi=1&theme=light&showinfo=0' frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                    
                                                    <div class="sp-slide">
                                                        <div class="videogal-text">
                                                            <p> Fuel Desire - Wills Lifestyle</p>
                                                        </div>
                                                         <iframe class="sp-video" src='https://www.youtube.com/embed/6knj9yBTPyc?enablejsapi=1&theme=light&showinfo=0' frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                    
                                                    <div class="sp-slide">
                                                        <div class="videogal-text">
                                                            <p> Ignite Mystery - Wills Lifestyle</p>
                                                        </div>
                                                         <iframe class="sp-video" src='https://www.youtube.com/embed/i1lnbuDnwh8?enablejsapi=1&theme=light&showinfo=0' frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                    
                                                    <div class="sp-slide">
                                                        <div class="videogal-text">
                                                            <p> Spark Attraction - Wills Lifestyle</p>
                                                        </div>
                                                         <iframe class="sp-video" src='https://www.youtube.com/embed/t34uqsPBp_Q?enablejsapi=1&theme=light&showinfo=0' frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                     <div class="sp-slide">
                                                        <div class="videogal-text">
                                                            <p> Stay Bold - Wills Lifestyle</p>
                                                        </div>
                                                         <iframe class="sp-video" src='https://www.youtube.com/embed/bS_dPrL1s_g?enablejsapi=1&theme=light&showinfo=0' frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                    
                                                </div>

                                                <div class="sp-thumbnails">
                                                    <img class="sp-thumbnail" src='http://img.youtube.com/vi/N8Vk_mwnpTY/mqdefault.jpg'/>
                                                    <img class="sp-thumbnail" src='http://img.youtube.com/vi/oaP3qnHoXxw/mqdefault.jpg'/>
                                                    <img class="sp-thumbnail" src='http://img.youtube.com/vi/y40MWzSEKyc/mqdefault.jpg'/>
                                                    <img class="sp-thumbnail" src='http://img.youtube.com/vi/6knj9yBTPyc/mqdefault.jpg'/>
                                                    <img class="sp-thumbnail" src='http://img.youtube.com/vi/i1lnbuDnwh8/mqdefault.jpg'/>
                                                    <img class="sp-thumbnail" src='http://img.youtube.com/vi/t34uqsPBp_Q/mqdefault.jpg'/>
                                                    <img class="sp-thumbnail" src='http://img.youtube.com/vi/bS_dPrL1s_g/mqdefault.jpg'/>
                                                    
                                                    
                                                   
                                                </div>
                                            </div>
                                        </div>


                                    </div>



                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 padding-0 hideonmobile">
                            <div class="about-right">
                                <img src="img/videogallery.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </div>
    <!--#########################Body End Here#########################-->

    <script src="js/jquery-2.2.4.min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/loader.js"></script>

    <script type="text/javascript" src="js/customjs.js"></script>

    <script type="text/javascript">
        $(window).resize(function() {
            $('#wrapper').height($(window).height());
        });
        $(window).trigger('resize');

    </script>

    <!-- Owl Carousel Assets -->
    <link rel="stylesheet" type="text/css" href="css/slider-pro.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" media="screen" />

    <script type="text/javascript" src="js/jquery.sliderPro.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function($) {
            $('#example3').sliderPro({
                width: 960,
                height: 500,
                fade: true,
                arrows: true,
                buttons: false,
                fullScreen: false,
                shuffle: true,
                smallSize: 500,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false
            });

        });
    </script>

    </form>
</body>
</html>
