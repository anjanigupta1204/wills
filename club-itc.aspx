﻿<%@ page language="C#" autoeventwireup="true" inherits="club_itc, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />

<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css">
</head>
<body>
    <form id="form1" runat="server">
<uc:navigation id="navigation1"  runat="server"   />
<!--#########################Body Start Here#########################-->
  <div class="pageWidth"><section id="wrapper">
     
     <div class="container-fluid padding-0">
     <div class="row">
     	<div class="col-md-8 padding-0">
        <div class="about-left">
        	<div class="leftinner">
            <h1 class="black35"><img src="img/clubitc-logo.png"></h1>
           <nav class="famTreb pbprc"> 
                    	<a href="index.aspx">Home</a> | Club ITC
                    </nav>
            
            <div class="about-content content">
           
           <div class="willsaccordion">
           <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          About Club ITC
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
      <div class="scroll-content">
      <h3>Club ITC - A World Apart</h3>
       <p>Experience the grand coming together of fine fashion and fine living with Club ITC - a distinctive loyalty programme brought to you by ITC.
Club ITC’s privileges are manifold – from recognition led benefits to rewards and more… Your Point earnings will now accumulate faster as each time you stay at over 45 participating ITC Hotels or shop at over 100 Wills Lifestyle Stores, you will earn 'Green Points' on all eligible spends*, redeemable against a host of rewards. </p>
<p>Where every indulgence enriches the planet. Coming as it does from ITC Ltd., the only Enterprise in the world of its comparable size to be water positive, carbon positive and solid waste recycling positive. </p>
<p>Welcome to the world of Club ITC.</p>
</div>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Membership Eligibility
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
      
      <div class="scroll-content">
      
      <div class="accordiosection">
      <h3>Membership Levels</h3>
        <p class="last">Club ITC features 3 distinct membership tiers – Silver, Gold and Platinum.</p>
        </div>
        
        <div class="accordiosection">
      <h3>Eligibility criteria for Membership to Silver:</h3>
        <ul>
         <li>At least one valid stay at participating ITC Hotels Or</li>
  <li> Spend of Rs. 7500 at participating Wills Lifestyle stores</li>
        </ul>
        </div>
        
         <div class="accordiosection">
      <h3>Raise the Bar. Silver Turns to Gold. </h3>
        <ul>
         
          <li>A minimum of 8 stays Or 16 nights at ITC Hotels Or</li>
<li> Spend of Rs 25,000 at Wills Lifestyle stores Or</li>
<li> A combined spend at both of Rs 1 Lakh</li>
         
        </ul>
        </div>
        
           <div class="accordiosection">
      <h3>Raise the Bar. Silver Turns to Gold. </h3>
        <ul>
         
          <li>A minimum of 8 stays Or 16 nights at ITC Hotels Or</li>
<li> Spend of Rs 25,000 at Wills Lifestyle stores Or</li>
<li> A combined spend at both of Rs 1 Lakh</li>
         
        </ul>
        </div>
        
           <div class="accordiosection">
      <h3>And Gold to Platinum…  </h3>
        <ul>
         
          <li>A minimum of 16 stays Or 30 nights at ITC Hotels Or</li>
<li> Spend of Rs 50,000 at Wills Lifestyle Stores Or</li>
<li> A combined spend at both of Rs 2.5 Lakhs</li>
         
        </ul>
        </div>
        
          <div class="accordiosection last">
      <p class="last"><strong>Note:</strong> All the above activities to be completed within a period of 12 months.</p>
        </div>
        
        </div>
        
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          Membership Rewards
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
      
       <div class="scroll-content">  
               
       <div class="tablewrap">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td align="left" valign="top">Aspire for Club ITC Gold and Platinum</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                          <td width="64%" align="left" valign="middle" bgcolor="#e1e1e1"><strong>Benefits</strong></td>
                          <td width="12%" align="center" valign="middle" bgcolor="#999999" class="white-color"><strong>Silver</strong></td>
                          <td width="12%" align="center" valign="middle" bgcolor="#cc650a" class="white-color"><strong>Gold</strong></td>
                          <td width="12%" align="center" valign="middle" bgcolor="#333333" class="white-color"><strong>Platinum</strong></td>
                        </tr>
                      </tbody></table></td>
                    </tr>
                    <tr>
                      <td height="30" align="left" valign="middle"><strong>ITC Hotels</strong></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="rowborder">
                        <tbody><tr>
                          <td width="64%" align="left" valign="middle">Earning of Green Points on eligible spends<sup>#</sup></td>
                          <td width="12%" align="center" valign="top">3%</td>
                          <td width="12%" align="center" valign="top">4%</td>
                          <td width="12%" align="center" valign="top">5%</td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Personalised check-in</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Gift Certificate of ~500 on completion of 3rd stay<sup>**</sup></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Suite upgrade Certificate on completion of 5th stay<sup>**</sup></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Free weekend night Certificate on completion of 12th stay**</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Earn Green Points on fine dining experiences even as a non-resident guest(on a minimum eligible spend of <br>
                            <img src="img/rupee.gif" width="8" alt="Rs"> 2000)</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">No extra room charge for accompanying spouse</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">50% off on published room tariff of suites on weekends (nights of Fri/Sat)</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Priority on room reservations with a 72 hour notice</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Upgrade to Best Rooms, including Standard Suites, on availability, at check in</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">In-room welcome amenities</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Complimentary in-room hi-speed internet (HSIA)</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Compimentary in-room tea/coffee service throughout your stay</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Access to the exclusive Towers Club/Upper Crust Lounges, even as a non-resident guest</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Late check-out till 4 p.m. subject to availability</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                      </tbody></table></td>
                    </tr>

                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="30" align="left" valign="middle"><strong>Wills Lifestyle</strong></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="rowborder">
                        <tbody><tr>
                          <td width="64%" align="left" valign="middle">Earning of Green Points on eligible spends<sup>*</sup></td>
                          <td width="12%" align="center" valign="top">3%</td>
                          <td width="12%" align="center" valign="top">4%</td>
                          <td width="12%" align="center" valign="top">5%</td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Priority on garment availability</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Doorstep delivery of apparel</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Complimentary`1000 Gift Certificate on an annual spend of <img src="img/rupee.gif" width="8" alt="Rs">20,000</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Complimentary Monogramming service</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Shopping by appointment</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                      </tbody></table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="30" align="left" valign="middle"><strong>Other Benefits</strong></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="rowborder">
                        <tbody><tr>
                          <td width="64%" align="left" valign="middle">Complimentary Membership to King Club Gold<sup></sup></td>
                          <td width="12%" align="center" valign="top">&nbsp;</td>
                          <td width="12%" align="center" valign="top">&nbsp;</td>
                          <td width="12%" align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Green Fee Waiver on weekdays at Classic Golf Resort, Gurgaon (Tue-Fri)</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Complimentary subscription to Namaste and WelcomZest Magazines</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle">Complimentary access to ITC's Green Lounge at IGI Airport, New Delhi on presentation of your Card</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top">&nbsp;</td>
                          <td align="center" valign="top"><img src="img/right.gif" width="12" height="17" alt="Yes"></td>
                        </tr>
                      </tbody></table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top" class="size12">*Eligible spends at Wills Lifestyle are all expenses on apparel, accessories &amp; personal care products excluding purchase during End of Season Sale and purchase/redemption of all Gift Certificates.<br>
                        **Stays to be completed within 12 months from the date of enrolling onto Club ITC.<br>
                        #Eligible spends at participating ITC hotels are all expenses on your room or restaurant bills excluding paid outs, taxes, tips and banquet billing.</td>
                    </tr>
                  </tbody></table>
                  
                  </div>
                  </div>
        
      </div>
    </div>
  </div>
</div>
</div>
           </div>
           
         
           
            </div>
        </div>
        </div>
        <div class="col-md-4 padding-0">
        <div class="about-right">
        <img src="img/club-itc.jpg"></div></div>
     </div>
     </div>
  </section></div>
<!--#########################Body End Here#########################-->  

<script src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<!-- custom scrollbar plugin -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>


<script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
});
$(window).trigger('resize');

</script>
	
	<script>
	
	
	$(document).ready(function() {
  function setHeight() {
    windowHeight = $(window).innerHeight() - 160;
    	$('.about-content').css('height', windowHeight);
		$('.scroll-content').css('height', windowHeight - 200);
  };
  setHeight();
  
  $(window).resize(function() {
    setHeight();
  });
});
		
		

	
		(function($){
			$(window).on("load",function(){
				
				$(".scroll-content").mCustomScrollbar({
					theme:"minimal"
				});
				
			});
		})(jQuery);
		
	
	$(document).on('show','.accordion', function (e) {
         //$('.accordion-heading i').toggleClass(' ');
         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
        //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });
		
	</script>
    </form>
</body>
</html>