﻿<%@ page language="C#" autoeventwireup="true" inherits="personalcare, App_Web_b26udrjz" %>

<%@ Register TagPrefix="uc" TagName="navigation" Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>

<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head id="Head1">
<title>Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />

<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/slider.css" rel="stylesheet" type="text/css" />

<link href="lightbox/facebox.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/fakeLoader.css">

</head>

<body>
    <form id="form1" runat="server">
   
  <uc:navigation id="navigation1" runat="server" />

<!--#########################Body Start Here#########################-->

 <div class="cLeft" align="center">
 
 <%--  <div id="Clogo">
              <ul id="alogo">
                <li><a class="classic" href="classic.aspx">&nbsp;</a></li>
                <li><a href="sport.aspx" class="sport">&nbsp;</a></li>
                <li><a href="clublife.aspx" class="clublife">&nbsp;</a></li>
                <li><a href="signature.aspx" class="signature">&nbsp;</a></li>
                
                <!--<li><a href="accessories.html" class="accessories">&nbsp;</a></li>-->
                
              </ul>
            </div>--%>
 
            <div class="psRel">
              <div style="position:absolute; left:42px;" id="rghtThumb">
                <ul class="thumblisting">
                	<li style="display: list-item"><img src="img/Essezda-di.png" >
                     <p> Born of a deep understanding of fine living and discerning elegance, Essenza Di Wills’ exclusive fragrances and personal care range capture and emit a timeless radiance that will wrap you in a serene cocoon of luxurious distinction.<br>
                      For more information, visit <a href="http://www.essenzadiwills.com/" target="_blank">www.essenzadiwills.com</a></p>
                  </li>  
                  
                  <li><img src="img/Essezda-di.png" >
                  <p> Born of a deep understanding of fine living and discerning elegance, Essenza Di Wills’ exclusive fragrances and personal care range capture and emit a timeless radiance that will wrap you in a serene cocoon of luxurious distinction.<br>
                      For more information, visit <a href="http://www.essenzadiwills.com/" target="_blank">www.essenzadiwills.com</a></p>
                  </li>       
                  
                           <li><img src="img/Essezda-di.png">
                          <p> Born of a deep understanding of fine living and discerning elegance, Essenza Di Wills’ exclusive fragrances and personal care range capture and emit a timeless radiance that will wrap you in a serene cocoon of luxurious distinction.<br>
                      For more information, visit <a href="http://www.essenzadiwills.com/" target="_blank">www.essenzadiwills.com</a></p>
                  </li>
                  <li><img src="img/Essezda-di.png" >
                  <p> Born of a deep understanding of fine living and discerning elegance, Essenza Di Wills’ exclusive fragrances and personal care range capture and emit a timeless radiance that will wrap you in a serene cocoon of luxurious distinction.<br>
                      For more information, visit <a href="http://www.essenzadiwills.com/" target="_blank">www.essenzadiwills.com</a></p>
                  </li>
                  <li><img src="img/Essezda-di.png" >
                  <p> Born of a deep understanding of fine living and discerning elegance, Essenza Di Wills’ exclusive fragrances and personal care range capture and emit a timeless radiance that will wrap you in a serene cocoon of luxurious distinction.<br>
                      For more information, visit <a href="http://www.essenzadiwills.com/" target="_blank">www.essenzadiwills.com</a></p>
                  </li>
                  
                  <li><img src="img/fiama-di-men.jpg" >
                
                    <p>Presenting Revolutionary Gel Care for Men's Tough Skin. Fiama Di Wills Men products have been specifically designed for men's skin to keep it firm, clear and youthful<br>
<br>
For more information, visit <a href="http://www.fiamadiwillsmen.in/" target="_blank">www.fiamadiwillsmen.in</a></p>
                  </li>
                  <li><img src="img/fiama-di.jpg" >
                
                    <p>A premium range of personal care products crafted after years of extensive research at Labortaire Naturel, Fiama Di Wills products offer great sensorial delight and expert solutions for a youthful you.<br>
<br>
For more information, visit <a href="http://www.fiamadiwills.com/" target="_blank">www.fiamadiwills.com</a></p>
                  </li>
				   <li><img src="img/fiama-di.jpg" >
                  
                    <p>A premium range of personal care products crafted after years of extensive research at Labortaire Naturel, Fiama Di Wills products offer great sensorial delight and expert solutions for a youthful you.<br>
<br>
For more information, visit <a href="http://www.fiamadiwills.com/" target="_blank">www.fiamadiwills.com</a></p>
                  </li>
                  </li>
				   <li><img src="img/Vivelren.png" >
                  
                    <p>A world of premium derma expertise which fights everyday sources of skin damage.
An innovative skin care that provides vitamin nutrition at the cell level<br>
<br>
For more information, visit <a href="http://www.cellrenew.in/" target="_blank">www.cellrenew.in</a></p>
                  </li>
                </ul>
              </div>
            </div>
          
          </div>
 <div class="cRight">
            <div class="abmain">
              <ul id="cslider" class="sliderlisting">
              <li><a href="collection/personalcare/Large/6.jpg"  id="A6" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#6" ><img src="img/spacer.png" ></a><img src="collection/personalcare/6.jpg" ></li>
              <li><a href="collection/personalcare/Large/3.jpg"  id="A3" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#3" ><img src="img/spacer.png" ></a><img src="collection/personalcare/3.jpg" ></li>
              <li><a href="collection/personalcare/Large/5.jpg"  id="A5" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#5" ><img src="img/spacer.png" ></a><img src="collection/personalcare/5.jpg" ></li>
	      <li><a href="collection/personalcare/Large/mn-rn-2.jpg"  id="A1" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#1" ><img src="img/spacer.png" ></a><img src="collection/personalcare/mn-rn-2.jpg" ></li>
	      <li><a href="collection/personalcare/Large/1.jpg"  id="A1" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#1" ><img src="img/spacer.png" ></a><img src="collection/personalcare/1.jpg" ></li>
               <li><a href="collection/personalcare/Large/2.jpg"  id="A2" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#2" ><img src="img/spacer.png" ></a><img src="collection/personalcare/2.jpg" ></li>
                <li><a href="collection/personalcare/Large/4.jpg"  id="A4" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#4" ><img src="img/spacer.png" ></a><img src="collection/personalcare/4.jpg" ></li>
                <li><a href="collection/personalcare/Large/16.jpg"  id="A5" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#4" ><img src="img/spacer.png" ></a><img src="collection/personalcare/16.jpg" ></li>
                <li><a href="collection/personalcare/Large/renew.jpg"  id="A5" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#4" ><img src="img/spacer.png" ></a><img src="collection/personalcare/renew.jpg" ></li>
              </ul>
              <div class="thumbnails">
                <ul>
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/6.jpg" /></div>
                  </li>
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/3.jpg" /></div>
                  </li>
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/5.jpg" /></div>
                  </li>
				   <li>
                    <div><img src="collection/personalcare/Thumbnail/mn-rn-2.jpg" /></div>
                  </li>
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/1.jpg" /></div>
                  </li>
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/2.jpg" /></div>
                  </li>
                  
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/4.jpg" /></div>
                  </li>
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/16.jpg" /></div>
                  </li>
                  <li>
                    <div><img src="collection/personalcare/Thumbnail/renew.jpg" /></div>
                  </li>
                
                </ul>
              </div>
            </div>
          </div>

<!--#########################Body End Here#########################--> 

<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script src="js/loader.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<script type="text/javascript" src="lightbox/facebox.js"></script> 
<script type="text/javascript" src="js/abslider_lookbook.js"></script> 
<!--Zoom start here--> 
<script type="text/javascript" src="Gallery/magiczoomplus.js"></script>
<link rel="stylesheet" href="Gallery/magiczoomplus.css" type="text/css" />
<script src="js/master.js"></script> 
<script type="text/javascript">
$(function() {
	$('#cslider').cslider();
// end of main function
});
</script>

    </form>
</body>
</html>

