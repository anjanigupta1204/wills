﻿<%@ page language="C#" autoeventwireup="true" inherits="privacy_policy, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />

<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css"> 
</head>

<body>
    <form id="form1" runat="server">


<uc:navigation id="navigation1"  runat="server"   />


<!--#########################Body Start Here#########################-->
  <div class="pageWidth"><section id="wrapper">
     
     <div class="container-fluid padding-0">
     <div class="row">
     	<div class="col-md-8 padding-0">
        <div class="about-left">
        	<div class="leftinner">
            <h1 class="black35">Privacy Policy</h1>
           <nav class="famTreb pbprc"> 
                    	<a href="index.html">Home</a> | Privacy Policy
                    </nav>
            
            <div class="about-content content">
            <div class="scroll-content">
            <div class="privacy">
            	<div class="row">
            	<div class="col-md-12">
                		<div class="privacypolicy">
                        	<ul>
                            	<li>ITC Limited welcomes you to its Website and looks forward to a meaningful interaction with you. ITC Limited respects an individual’s right to privacy. This policy lays down our practices with respect to confidentiality and disclosure of personal information. Please familiarize yourself with our privacy practices, as given below. Should you choose to share your personal information with ITC Limited, it will be assumed that you have no objection to the terms of this privacy policy.<br><br>
<strong>Collection of Personal Information</strong><br><br>
ITC Limited collects personal information only when it is required for business purposes.
</li>
                                <li>While collecting any personal information, ITC Limited indicates the purpose of collection of such information and obtains the consent of the provider of the information in writing. ITC Limited gives the person providing the information with an option of not providing the information. He also has the option of withdrawing his consent given earlier. In such cases, ITC Limited may choose not to provide any services/supply goods for which the information was necessary.</li>



                                <li>While collecting information, ITC Limited or any entity authorized on its behalf to collect information takes such steps as are, in the circumstances, reasonable to ensure that the person providing the information has the knowledge of —<br>
the fact that the information is being collected<br>
the purpose for which the information is being collected; and<br>
the intended recipients of the information</li>

                                <li>The providers of information are permitted, as and when requested by them, to review the information they had provided.</li>
                                <li> <strong>Confidentiality and Disclosure of personal information</strong><br>
The terms of confidentiality and disclosure of personal information will be recorded in the non-disclosure agreement that may be entered into with the provider of the information.</li>
                                <li>ITC Limited has also adopted reasonable security practices and procedures comprising of managerial, technical, operational and physical security control measures with a view to ensure confidentiality and prevent unauthorized disclosure of personal information.</li>
                                
                                <li>ITC Limited uses the personal information only for the purpose for which the same has been furnished. ITC Limited does not retain information after the purpose for which it is furnished is accomplished unless the same is required to be retained under any law in force or for audit purposes. In such cases also, confidentiality of such information is maintained.</li>
                                
                                <li><strong>Policy for Website usage</strong><br>
Any personal information shared by you in this Website shall be kept confidential in accordance with ITC Limited’s reasonable security practices and procedures. Personal information, shared by you, shall be used for doing the intended business with you.</li>
<li>ITC Limited assures you that in the event of your personal information being shared by the Company with its subsidiaries, business associates etc., such sharing of information shall be for the purpose of doing the intended business with you. You hereby grant your consent to ITC Limited to share such information with its subsidiaries, business associates etc.</li>
<li>ITC Limited reserves its rights to collect, analyze and disseminate aggregate site usage patterns of all its visitors with a view to enhancing services to its visitors. This includes sharing the information with its subsidiaries and business associates as a general business practice.</li>
<li>In the course of its business ITC Limited may hold on-line contests and surveys as permitted by law and it reserves its right to use and disseminate the information so collected to enhance its services to the visitors. This shall also include sharing the information with its subsidiaries and business associates as a general business practice.</li>
<li>Cookies:- To personalize your experience on the Company’s Website or to support one of its promotions, ITC Limited may assign your computer browser a unique random number (cookie). "Cookies" enhance website performance in important ways like personalizing your experience, or making your visit more convenient. Your privacy and security will not be compromised when you accept a "cookie" from the Company’s Website.</li>
<li>ITC Limited shall not be responsible in any manner whatsoever for any violation or misuse of your personal information by unauthorised persons consequent to misuse of the internet environment.</li>
<li><strong>Grievance Officer</strong><br>
In accordance with the Information Technology Act, 2000 and the Rules framed thereunder, the name and contact details of the Grievance Officer are provided below:<br>
Mr. K T Prasad<br>
ITC Centre, 4 Russell Street, Kolkata - 700071<br>
Prasad.kt@itc.in<br>
+913322889371<br>
9AM – 5PM (on all working days)<br>
<strong>Changes to the Policy</strong><br>
ITC Limited reserves its right to revise this privacy policy from time to time at its sole discretion. Such policy will be available on the Company Website.</li>
<li>All words and expressions used herein and not defined, but defined in the Information Technology Act, 2000 and/or the Information Technology (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011 shall have the meanings respectively assigned to them thereunder.</li>
                                
                            </ul>
                        </div>
                </div>
                </div>
            </div>
            	
           </div>
            </div>
        </div>
        </div>
        </div>
        
        <div class="col-md-4 padding-0">
        <div class="about-right">
        <img src="img/shoponline3.jpg"></div></div>
     </div>
     </div>
  </section></div>
<!--#########################Body End Here#########################-->  
<script src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>
<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<!-- custom scrollbar plugin -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
});
$(window).trigger('resize');

</script>

	<script>
	
	
	$(document).ready(function() {
  function setScroll() {
	  if ($(window).width() > 992) {
		  
    windowHeight = $(window).innerHeight() - 160;
    	$('.about-content').css('max-height', windowHeight);
		$('.scroll-content').css('height', windowHeight);
		$(".scroll-content").mCustomScrollbar({
					theme:"minimal"
		});	
	  }
	  else{
		  $('#wrapper').height($(window).height());
		  $('.about-content').css('max-height','none');
		$('.scroll-content').css('height', 'auto');
		$(".scroll-content").mCustomScrollbar("disable",true);
		
	  }
  };
  setScroll();
  
  $(window).resize(function() {
    setScroll();
  });
});
		
		



	</script>



    </form>
</body>
</html>
