﻿<%@ page language="C#" autoeventwireup="true" inherits="gifting, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css">

</head>
<body>
    <form id="form1" runat="server">
 
<uc:navigation id="navigation1"  runat="server"   />

<!--#########################Body Start Here#########################-->
<div class="pageWidth">
  <section id="wrapper">
    <div class="container-fluid padding-0">
      <div class="row">
        <div class="col-md-8 padding-0">
          <div class="about-left">
            <div class="leftinner">
              <h1 class="black35">Gifting</h1>
              <nav class="famTreb pbprc"> <a href="index.html">Home</a> | Gifting </nav>
              <div class="about-content content">
                <div class="scroll-content">
                  <div class="row">
                    <div class="col-md-12"><img src="img/gift-certificate.png" class="img-responsive"></div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p>Wills Lifestyle presents "Occasions" - a beautiful, ready to present gift pack with an exquisitely designed gift certificate that can be redeemed against a range of apparel, accessories and personal care products.</p>
                      <p>Available in denominations of Rs 250, Rs 500, Rs 1000 & Rs 5000, it is the perfect gift for any occasion.</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="clicktobuy">
                        <div class="row">
                          <div class="col-md-6">
                            <p class="black-color">For corporate orders,<br>
                              contact customer care at <br>
                              0124 - 4101010.</p>
                          </div>
                          <div class="col-md-6">
                            <p class="linkwrap black-color"> <a href="http://www.giftcardsindia.in/products/wills-lifestyle-gift-voucher" target="_blank"> Click here to buy<br>
                              Wills Lifestyle gift certificates</a> </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 padding-0">
          <div class="about-right"> <img src="img/shoponline3.jpg"></div>
        </div>
      </div>
    </div>
  </section>
</div>

<!--#########################Body End Here#########################-->  
<script src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<!-- custom scrollbar plugin --> 
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
});
$(window).trigger('resize');

</script> 
<script>
	
	
	$(document).ready(function() {
  function setScroll() {
	  if ($(window).width() > 992) {
		  
    windowHeight = $(window).innerHeight() - 160;
    	$('.about-content').css('max-height', windowHeight);
		$('.scroll-content').css('height', windowHeight);
		$(".scroll-content").mCustomScrollbar({
					theme:"minimal"
		});	
	  }
	  else{
		  $('#wrapper').height($(window).height());
		  $('.about-content').css('max-height','none');
		$('.scroll-content').css('height', 'auto');
		$(".scroll-content").mCustomScrollbar("disable",true);
		
	  }
  };
  setScroll();
  
  $(window).resize(function() {
    setScroll();
  });
});

</script>

    </form>
</body>
</html>


