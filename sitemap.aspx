﻿<%@ page language="C#" autoeventwireup="true" inherits="sitemap, App_Web_b26udrjz" %>

<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css">
</head>

<body>
    <form id="form1" runat="server">
<uc:navigation id="navigation1"  runat="server"   />

<!--#########################Body Start Here#########################-->
<div class="pageWidth">
  <section id="wrapper">
    <div class="container-fluid padding-0">
      <div class="row">
        <div class="col-md-8 padding-0">
          <div class="about-left">
            <div class="leftinner">
              <h1 class="black35">Sitemap</h1>
              <nav class="famTreb pbprc"> <a href="index.aspx">Home</a> | Sitemap </nav>
              <div class="about-content content">
                <div class="sitemap">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-4">
                        <h3>Wills Lifestyle</h3>
                          <ul>

                            <li><a href="profile.aspx">About us</a></li>
                            <li><a href="store.aspx">Store Locator</a></li>
                            <%--<li><a href="#">The Debut</a></li>--%>
                            <li><a href="club-itc.aspx">Club ITC</a></li>
                            <li><a href="gifting.aspx">Gifting</a></li>
                          </ul>
                        </div>
                        <div class="col-md-4">
                        <h3>Collection</h3>
                          <ul>
                            
                            
                            <li><a href="personalcare.aspx">Personal Care</a></li>
                            <li><a href="http://www.shopwillslifestyle.com/" target=_blank>Shop Online</a></li>
                          </ul>
                        </div>
                        <div class="col-md-4">
                        <h3>Customer Service</h3>
                          <ul>
                            
                            <li><a href="contactus.aspx">Contact Us</a></li>
                            <li><a href="http://feedback.willslifestyle.com/default.aspx?bid=1" target=_blank>Feedback</a></li>
                            <li><a href="return-policy.aspx">Return Policy</a></li>
                            <li><a href="terms-use.aspx">Terms of Use</a></li>
                            <li><a href="privacy-policy.aspx">Privacy Policy</a></li>
                            <li><a href="faq.aspx">FAQs</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 padding-0">
          <div class="about-right"> <img src="img/sitemap.jpg"></div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--#########################Body End Here#########################-->
<script src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
});
$(window).trigger('resize');

</script>
    </form>
</body>
</html>

