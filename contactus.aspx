

<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India�s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />

<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css">













<!--Animation work start here-->
<script src="js/jsValidation.js" type="text/javascript"></script>

<!--<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-min.js"></script>
<script type="text/javascript" src="lightbox/facebox.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>-->
<script type="text/javascript" src="js/abslider.js"></script>
<!--<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>-->
<script type="text/javascript" src="js/master.js"></script>

    <script src="backup/js/abslider_lookbook.js" type="text/javascript"></script>

    
    <script src="js/ajaxfileupload.js" type="text/javascript"></script>
<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>

</head>
    <body>
    <form name="form1" method="post" action="contactus.aspx" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTcyMzg2NjY0NWRkqIdRuGPrzWzwSBUeMSF3OjM8Aqs=" />
</div>

<div>

	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWBwLUg6uqDwKbufQdArLNxoMNAqW6yJUHAu6A6s0KAuuX0uAIAtypj7sC8QORyw+i+HY0Q7SQzVXvdlhruJo=" />
</div>

<div class="fakeloader"></div>
<div class="commonnav">
<!-- Paste this code after body tag -->
<!--	<div class="se-pre-con"></div> -->
	<!-- Ends -->
<div class="left-nav" id="leftnav">
  <div class="wills_logo"><a href="index.aspx"><img src="img/wills_lifestyle.jpg" class="icon-big"> <img src="img/wills_logo.gif" class="icon-small"></a></div>
  <nav class="hideonmobile">
    <ul>

      <li><a href="lookbook.aspx" data-toggle="tooltip" title="Earth Collection" data-placement="right"><span>Earth Collection</span></a></li>
      <li><a href="http://www.shopwillslifestyle.com" target="_blank" data-toggle="tooltip" title="Shop Online" data-placement="right"><span>Shop Online</span></a></li>
      <li><a href="store.aspx" data-toggle="tooltip" title="Store Locator" data-placement="right"><span><img src="img/store-locator-Nw-icon.png" /></span></a></li>
      <li><a href="gifting.aspx" data-toggle="tooltip" title="Store Gifting" data-placement="right"><span>Gifting</span></a></li>
      <li><a href="club-itc.aspx" data-toggle="tooltip" title="Club ITC" data-placement="right"><span>Club ITC</span></a></li>
      <li><a href="video-gallery.aspx" data-toggle="tooltip" title="Video Gallery" data-placement="right"> <span>Video Gallery</span></a></li>
      <li><a href="contactus.aspx" data-toggle="tooltip" title="Contact Us" data-placement="right"><span>Contact Us</span></a></li>
      
    </ul>
  </nav>
  <div class="socialdiv">
    <ul>
      <li><a href="https://www.facebook.com/willslifestyleonline" target="_blank"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>
      <li><a href="https://twitter.com/willslifestyle" target="_blank"><i class="fa fa-twitter"></i> <span>Twitter</span></a></li>
      <li><a href="http://www.pinterest.com/wlsonline/" target="_blank"><i class="fa fa-pinterest"></i> <span>Pinterest</span></a></li>
      <li><a href="http://www.youtube.com/willslifestyleonline" target="_blank"><i class="fa fa-youtube"></i> <span>Youtube</span></a></li>
      <li><a href="http://www.instagram.com/willslifestyle" target="_blank"><i class="fa fa-instagram"></i> <span>Instagram</span></a></li>
    </ul>
  </div>
  <div class="itclogo"><a href="http://www.itcportal.com/" target="_blank"><img src="img/ITCLogo.gif"></a></div>
</div>
<!-- end left nav -->


<!-- Navigation --> 
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
  <ul class="sidebar-nav">
    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
    <li> Wills Lifestyle</li>
    <li><a href="profile.aspx">About us</a></li>
    <li><a href="store.aspx">Store Locator</a></li>
    
    <li><a href="club-itc.aspx">Club ITC</a></li>
    <li><a href="gifting.aspx">Gifting</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li> Collection</li>
    
    <li><a href="personalcare.aspx"> Personal Care</a></li>
    <li><a href="lookbook.aspx"> Earth Collection</a></li>
    <li><a href="http://www.shopwillslifestyle.com" target="_blank"> Shop Online</a></li>
    <li><a href="video-gallery.aspx"> Video Gallery</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li>Customer Service</li>
    <li><a href="contactus.aspx">Contact Us</a></li>
    <li><a href="http://feedback.willslifestyle.com/default.aspx?bid=1" target="_blank">Feedback</a></li>
    <li><a href="return-policy.aspx">Return Policy</a></li>
    <li><a href="terms-use.aspx">Terms of use</a></li>
    <li><a href="http://www.itcportal.com/about-itc/policies/privacy-policy.aspx" target="_blank">Privacy Policy</a></li>
    <li><a href="FAQ.aspx">FAQs</a></li>
    <li><a href="sitemap.aspx">Sitemap</a></li>
  </ul>
</nav>

<img src="img/copyright.png" class="copyright">

</div>
<!--#########################Body Start Here#########################-->
  <div class="pageWidth"><section id="wrapper">
     
     <div class="container-fluid padding-0">
     <div class="row">
     	<div class="col-md-8 padding-0">
        <div class="about-left">
        	<div class="leftinner">
            <h1 class="black35">Contact us</h1>
           <nav class="famTreb pbprc"> 
                    	<a href="index.html">Home</a> | Contact us
                    </nav>
            <div class="row pb2prc" id="divsuccess" style="display:none">
                        	<div class="col-md-12 col-sm-12 ">
                            <span class="black25">Thank You </span><br>
                                <p class="black16 pb10">for your feedback.</p>  
                                                   </div>
                        </div>
           
            <div class="scroll-content" id="divcontent">
            
            	<div class="contactform">	
                    
                    <div class="row">
						<div class="col-md-12 col-sm-12 ">
                       <p>If you have any experience that you would like to share with us, do let us know. We will be delighted to hear from you or for more queries you can get in touch with Customer Care at customercare.executive@itc.in or contact us at 0124-4101010 from Monday to Friday 9:00AM to 5:00PM</p>

                    </div>
                     <div class="col-md-12">
                      <section class="pb2prc">
                        	<span class="erRed size12" style="display:none;color:red;"></span>
                        </section>
                        </div>
                    <div class="col-md-12 col-sm-12">
                        	<div class="row">
                            	<div class="fname">
                            	<div class="col-md-6 col-sm-6">
                            	
                                <div class="name">Name* <br>
                               
                                    <input name="Name" type="text" id="Name" maxlength="50" class="form-control" />
                                </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                <div class="name">Email-Id*<br>
                               
                                    <input name="email" type="text" id="email" maxlength="50" class="form-control" />
                                
                                </div>
                                </div>
                                
                            </div>
                            
                            <div class="col-md-6 col-sm-6">
                                <div class="name">Mobile* <br>
                               
                                    <input name="mobile" type="text" id="mobile" onkeypress="return isNumberKey(event)" maxlength="10" class="form-control" />
                                </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                <div class="dropdown">Nature of feedback*<br>
                         
								  <select name="feedbacktype" id="feedbacktype" class="form-control">
	<option value="0">Select</option>
	<option value="1">General Feedback</option>
	<option value="2">Query</option>
	<option value="3">Grievance</option>
</select>
							</div>
                                
                               
                                
                                </div>
                                
                           
                            <div class="col-md-12 col-sm-12">
                         	
                            	<div class="address">
                                    <label for="comment">Address*</label>
                                  
                                    <textarea name="address" id="address" rows="3" class="form-control" onkeyup="postlimitAddress(this)" onkeydown="postlimitAddress(this)"></textarea>
                        
                                     </div>
                                
                            
                         </div>
                         <div class="col-md-12 col-sm-12">
                         	
                            	<div class="form-group">
                                    <label for="comment">Your Feedback* (2000 characters max)</label>
                                 
                                    <textarea name="feedback" id="feedback" rows="3" class="form-control" onkeyup="postlimitFeedback(this)" onkeydown="postlimitFeedback(this)"></textarea>
                                     </div>
                                
                            
                         </div>
                         <div class="col-md-12 col-sm-12">
                         	<div class="row">
                            	<div class="captcha">
                            	<div class="col-md-5 col-sm-5">

                                	
                                    	<img  src="Jquerypages/splashCaptcha.aspx" id="captchImg"  alt="captcha"/>
						
                                   <img class="refresh_captcha" src="img/captcha_refresh.gif" style="border:0px;width:39px;vertical-align:center;cursor:pointer;">

                                </div></div>
                                <div class="col-md-7 col-sm-7">
                                	<div class="name">
                               
                                         <input name="" type="text" class="form-control" style="width:89%;margin:10px 0;" id="contactCaptcha" value="Enter the number displayed on left to proceed" maxlength="6" onFocus="if(this.value=='Enter the number displayed on left to proceed')this.value=''" onblur="if(this.value=='')this.value='Enter the number displayed on left to proceed'" />
						
                                </div>
                                </div>
                            </div>
                         </div>
                         
                         <div class="col-md-12 col-sm-12">
                         	<div class="submitbtn">
                            	
                                 <input name="" type="button" value="Submit &rsaquo;" class="redBtn" onclick="return check();" >
                            </div>
                         </div>
                    	          
                        </div>
                        
                </div>
                    </div>
                    	    
            	
           </div>
           </div>
            </div>
        </div>
        </div>
        
          <div class="col-md-4 padding-0">
        <div class="about-right">
        <img src="img/profile.jpg"></div></div>
        </div>
        
      
     </div>
         
     </div>
  </section></div>
<!--#########################Body End Here#########################-->  

<script src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<!-- custom scrollbar plugin -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript">
$(function(){

    $('.refresh_captcha').click(function () {
        debugger;
    $('#captchImg').attr('src', 'Jquerypages/splashCaptcha.aspx?cache=' + new Date().getTime());
});
});
</script>
<script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
});
$(window).trigger('resize');

</script>

	<script>
	
	
	$(document).ready(function() {
  function setScroll() {
	  if ($(window).width() > 992) {
		  
    windowHeight = $(window).innerHeight() - 160;
    	$('.about-content').css('max-height', windowHeight);
		$('.scroll-content').css('height', windowHeight);
		$(".scroll-content").mCustomScrollbar({
					theme:"minimal"
		});	
	  }
	  else{
		  $('#wrapper').height($(window).height());
		  $('.about-content').css('max-height','none');
		$('.scroll-content').css('height', 'auto');
		$(".scroll-content").mCustomScrollbar("disable",true);
		
	  }
  };
  setScroll();
  
  $(window).resize(function() {
    setScroll();
  });
});
		
		



	</script>

    </form>
           
</body>
</html>

