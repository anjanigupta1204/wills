








<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India�s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css">
</head>
<body>
<form name="form1" method="post" action="store.aspx" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJLTczOTI2NTcyZGRpndRxnwKjHx5R30ct2yTNC6LchA==" />
</div>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0F2C5E87" />
</div>
  
<div class="fakeloader"></div>
<div class="commonnav">
<!-- Paste this code after body tag -->
<!--	<div class="se-pre-con"></div> -->
	<!-- Ends -->
<div class="left-nav" id="leftnav">
  <div class="wills_logo"><a href="index.aspx"><img src="img/wills_lifestyle.jpg" class="icon-big"> <img src="img/wills_logo.gif" class="icon-small"></a></div>
  <nav class="hideonmobile">
    <ul>
      <li><a href="classic.aspx" data-toggle="tooltip" title="Collection" data-placement="right"><span>Collection</span></a></li>
      <li><a href="lookbook.aspx" data-toggle="tooltip" title="Earth Collection" data-placement="right"><span>Earth Collection</span></a></li>
      <li><a href="http://www.shopwillslifestyle.com" target="_blank" data-toggle="tooltip" title="Shop Online" data-placement="right"><span>Shop Online</span></a></li>
      <li><a href="store.aspx" data-toggle="tooltip" title="Store Locator" data-placement="right"><span><img src="img/store-locator-Nw-icon.png" /></span></a></li>
      <li><a href="gifting.aspx" data-toggle="tooltip" title="Store Gifting" data-placement="right"><span>Gifting</span></a></li>
      <li><a href="club-itc.aspx" data-toggle="tooltip" title="Club ITC" data-placement="right"><span>Club ITC</span></a></li>
      <li><a href="video-gallery.aspx" data-toggle="tooltip" title="Video Gallery" data-placement="right"> <span>Video Gallery</span></a></li>
      <li><a href="contactus.aspx" data-toggle="tooltip" title="Contact Us" data-placement="right"><span>Contact Us</span></a></li>
      
    </ul>
  </nav>
  <div class="socialdiv">
    <ul>
      <li><a href="https://www.facebook.com/willslifestyleonline" target="_blank"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>
      <li><a href="https://twitter.com/willslifestyle" target="_blank"><i class="fa fa-twitter"></i> <span>Twitter</span></a></li>
      <li><a href="http://www.pinterest.com/wlsonline/" target="_blank"><i class="fa fa-pinterest"></i> <span>Pinterest</span></a></li>
      <li><a href="http://www.youtube.com/willslifestyleonline" target="_blank"><i class="fa fa-youtube"></i> <span>Youtube</span></a></li>
      <li><a href="http://www.instagram.com/willslifestyle" target="_blank"><i class="fa fa-instagram"></i> <span>Instagram</span></a></li>
    </ul>
  </div>
  <div class="itclogo"><a href="http://www.itcportal.com/" target="_blank"><img src="img/ITCLogo.gif"></a></div>
</div>
<!-- end left nav -->


<!-- Navigation --> 
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
  <ul class="sidebar-nav">
    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
    <li> Wills Lifestyle</li>
    <li><a href="profile.aspx">About us</a></li>
    <li><a href="store.aspx">Store Locator</a></li>
    
    <li><a href="club-itc.aspx">Club ITC</a></li>
    <li><a href="gifting.aspx">Gifting</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li> Collection</li>
   
    <li><a href="personalcare.aspx"> Personal Care</a></li>
    <li><a href="lookbook.aspx"> Earth Collection</a></li>
    <li><a href="http://www.shopwillslifestyle.com" target="_blank"> Shop Online</a></li>
    <li><a href="video-gallery.aspx"> Video Gallery</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li>Customer Service</li>
    <li><a href="contactus.aspx">Contact Us</a></li>
    <li><a href="http://feedback.willslifestyle.com/default.aspx?bid=1" target="_blank">Feedback</a></li>
    <li><a href="return-policy.aspx">Return Policy</a></li>
    <li><a href="terms-use.aspx">Terms of use</a></li>
    <li><a href="http://www.itcportal.com/about-itc/policies/privacy-policy.aspx" target="_blank">Privacy Policy</a></li>
    <li><a href="FAQ.aspx">FAQs</a></li>
    <li><a href="sitemap.aspx">Sitemap</a></li>
  </ul>
</nav>

<img src="img/copyright.png" class="copyright">

</div>
  
  <!--#########################Body Start Here#########################-->
  <div class="pageWidth">
    <section id="wrapper" class="bg-white">
      <div class="container-fluid padding-0">
        <div class="row">
          <div class="col-md-6 padding-0">
            <div class="about-left">
              <div class="leftinner">
                <h1 class="black35">Store</h1>
                <nav class="famTreb pbprc"> <a href="index.aspx">Home</a> | Store</nav>
                <div class="css-slideshow">
                  <div class="item"> <img src="img/image1.jpg" /> </div>
                  <div class="item"> <img src="img/image3.jpg" /> </div>
                  <div class="item"><img src="img/image4.jpg" /> </div>
                  <div class="item"> <img src="img/image5.jpg" /> </div>
                  <div class="item"> <img src="img/image6.jpg" /> </div>
                </div>
                <div class="storedd-mob">
                  <select class="form-control">
                    <option>Find your nearest store</option>
                    <option value="agartala">Agartala </option>
                    <option value="agra">Agra</option>
                    <option value="ahmedabad">Ahmedabad </option>
                    <option value="Aizawl">Aizawl </option>
                    <!--<option value="bareilly">Bareilly </option>-->
                    <option value="baroda">Baroda </option>
                    <option value="bengaluru">Bengaluru </option>
                    <option value="bhopal">Bhopal </option>
                    <option value="bhubaneswar">Bhubaneswar </option>
                    <option value="bilaspur">Bilaspur </option>
                    <!--<option value="burdwan">Burdwan</option>
                    <option value="chandanagar">Chandanagar</option>-->
                    <option value="chandigarh">Chandigarh </option>
                    <option value="chennai">Chennai </option>
                    <option value="cochin">Cochin </option>
                    <option value="dehradun">Dehradun </option>
                    <option value="delhi">Delhi - NCR</option>
                    <option value="gangtok">Gangtok </option>
                    <option value="goa">Goa </option>
                    <option value="guwahati">Guwahati </option>
                    <option value="gwalior">Gwalior </option>
                    <option value="haldwani">Haldwani </option>
                    <option value="hyderabad">Hyderabad </option>
                    <option value="imphal">Imphal </option>
                    <option value="indore">Indore </option>
                    <option value="jaipur">Jaipur</option>
                    <option value="jalandhar ">Jalandhar </option>
                    <option value="jammu">Jammu</option>
                    <option value="jodhpur">Jodhpur</option>
                    <option value="jorhat">Jorhat</option>
                    <option value="kolkata">Kolkata </option>
                    <option value="lucknow">Lucknow</option>
                    <option value="ludhiana">Ludhiana</option>
                    <!--<option value="meerut">Meerut </option>-->
                    <option value="mumbai">Mumbai </option>
                    <option value="nasik">Nasik </option>
                    <option value="patiala">Patiala </option>
                    <option value="patna">Patna </option>
                    <option value="pune">Pune </option>
                    <option value="raipur">Raipur </option>
                    <option value="siliguri">Siliguri </option>
                    <option value="srinagar">Srinagar</option>
                    <option value="surat">Surat </option>
                    <option value="udaipur">Udaipur </option>
                  </select>
                </div>
                <div class="scroll-content" style="height: 300px;">
                  <div class="storecontainer-mob">
                    <div class="address-dv jorhat">
                      <h3>Jorhat </h3>
                      <p>Wills Lifestyle Store, Shree Krishna Venture, Near Nandlal Brothers, K. B. Road, Jorhat � 785001 <br>
                        Phone: 9435052828 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@26.7544946,94.2126181,17z/data=!3m1!4b1!4m5!3m4!1s0x3746c2be51c45133:0x2c0d113bf22bec8f!8m2!3d26.7544898!4d94.2148068?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                     <div class="address-dv chandanagar">
                      <h3>Chandanagar </h3>
                      <p>Wills Lifestyle Store, Padripara, G.T. Road, Opp. Raymonds Showroom, Chandan Nagar, Hoogly - 712136 <br>
                        Phone: 8013245643 <span class="mappin"><a href="https://www.google.co.in/maps/search/Padripara,+G.T.+Road,+Opp.+Raymonds+Showroom,+Chandan+Nagar,+Hoogly/@22.8596901,88.3607198,17z/data=!3m1!4b1?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                   <div class="address-dv Aizawl">
                      <h3>Aizawl</h3>
                      <p>ABIGAIL The Home Store, A-13, Electric Veng, Aizawl, Mizoram- 796001 <br>
                        Phone: 9774019660 <span class="mappin"><a href="https://www.google.co.in/maps/The Home Store,+A-13,+Electric Veng+Aizawl+Mizoram target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                         <!--
                    <div class="address-dv burdwan">
                      <h3>Burdwan </h3>
                      <p>Wills Lifestyle Store,Fancy Market, Tinkonia Bus Stand,Opp. Narayan Pump, (Bharat Petrol Pump), Burdwan- 713101 <br>
                        Phone: 7044269553 <span class="mappin"><a href="https://www.google.co.in/maps/place/Fancy+Market,+Grand+Trunk+Rd,+Khosbagan,+Burdwan,+West+Bengal+713101/@23.2452856,87.8670578,17z/data=!3m1!4b1!4m5!3m4!1s0x39f849c3d042e55d:0x8cb19836a8ea999b!8m2!3d23.2452807!4d87.8692465?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div> -->
                    <div class="address-dv srinagar" id="srinagar">
                      <h3>Srinagar </h3>
                      <p>Shop no. 119 to 121,Sahara City Mall, Near Exhibition Road, Srinagar - 190001. <br>
                        Phone: 8803623082 <span class="mappin"><a href="https://maps.google.com/maps?ll=34.216344,74.750976&z=17&t=m&hl=en-US&gl=IN&mapclient=embed&cid=7276243916075775004" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv jammu">
                      <h3>Jammu</h3>
                      
                      <p>313-A, Apsara Road, Gandhi Nagar, Jammu - 180001<br>
                        Phone: 9205080000 <span class="mappin"><a href="https://www.google.co.in/maps/place/Apsara+Rd,+Jammu+180004/@32.6990309,74.8559999,17z/data=!3m1!4b1!4m5!3m4!1s0x391e84b0e1080bff:0xabba560a03723aef!8m2!3d32.6990264!4d74.8581886?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p>WILLS LIFESTYLE, Shop No UGF 16, WAVES Mall, Opposite Bhatindi Morh Narwal, JAMMU - 180004 <span class="mappin"><a href="https://www.google.co.in/maps/search/WAVES+Mall,+Narwal,+JAMMU+-+180004+/@32.7130341,74.8654395,15z/data=!3m1!4b1?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv jalandhar">
                      <h3>Jalandhar</h3>
                      <p>Shop No- 505, R-Model Town, Jalandhar<br>
                        Phone:0181-4647005/06 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@31.3071678,75.577233,17z/data=!4m8!1m2!2m1!1sShop+No-+505,+R-Model+Town,+Jalandhar!3m4!1s0x391a5bab6f8ab5e7:0xa1f939b92a368c5!8m2!3d31.306766!4d75.580795?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv ludhiana">
                      <h3>Ludhiana</h3>
                      <p>Shop No. 85/4A, The Mall Ludhiana 141001, Phone: 0161-4644150 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@30.9102112,75.8386146,17z/data=!3m1!4b1!4m5!3m4!1s0x391a83a410b99833:0x110c5a8e63ba9aa!8m2!3d30.9102066!4d75.8408033?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p>M/S CHERYL, Shop no - 07, 1st Floor, Silver Arch Mall, Ferozpur Road, Ludhiana - 141001<br>
                        Phone: 0161-4640150 <span class="mappin"><a href="https://www.google.com/maps/place/Silver+Arc+Mall/@30.9005476,75.8270343,17z/data=!3m2!4b1!5s0x391a83c7ec381905:0x63c528b9a4403498!4m5!3m4!1s0x391a83c7eea8dd95:0x2fcef382bdff343!8m2!3d30.900543!4d75.829223?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv chandigarh">
                      <h3>Chandigarh</h3>
                      <!--<p>SCO 14, Sector 17E, Phone: 0172 - 6549856 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@30.739254,76.7792507,18z/data=!4m8!1m2!2m1!1sSCO+14,+Sector+17E+chandigarh!3m4!1s0x390fed0a8f3fde29:0x124c22e89404a5f4!8m2!3d30.7402911!4d76.7805913?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>-->
                      <p>Shop No 124,First Floor, Elante Mall<br>
                        Phone: 0172-4666814/815 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@30.7056705,76.7989966,17z/data=!3m2!4b1!5s0x390fece98a30442b:0x3e305f6d2fadd440!4m5!3m4!1s0x390fece98dbfe195:0x584ee8858d9e9c9c!8m2!3d30.7056659!4d76.8011853?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    
                    <div class="address-dv patiala">
                      <h3>Patiala </h3>
                      <p>Next to Sigma Laboratory, Opposite OBC Bank, Bhupindera road<br>
                        Phone: 0175 -5011777 <span class="mappin"><a href="https://www.google.com/maps/place/Migz+The+Trend+Setter(Wills+Lifestyle)/@30.3436875,76.3765539,17z/data=!3m1!4b1!4m5!3m4!1s0x391028b971aa86b1:0x707d5a39dc892d!8m2!3d30.3436829!4d76.3787426?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    
                    
                    <div class="address-dv dehradun">
                      <h3>Dehradun </h3>
                      <p>WILLS LIFESTYLE STORE, Unit No- LG-06, <br>
                        Pacific Mall, Mauja Jakhan, <br>
                        Rajpur Road, Dehradun- 248001<br>
                        Phone: 0135-2733344 <span class="mappin"><a href="https://www.google.co.in/maps/search/Unit+No-+LG-06,+Pacific+Mall,+Mauja+Jakhan,+Rajpur+Road,+Dehradun/@30.3653983,78.0682726,17z/data=!3m1!4b1?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv haldwani">
                      <h3>Haldwani </h3>
                      <p>The Icon, Planet Fashion, Nanital Road, Haldwani<br>
                        Phone:059-46224555 <span class="mappin"><a href="https://www.google.com/maps/place/Planet+Fashion/@29.2182691,79.510788,17z/data=!3m1!4b1!4m5!3m4!1s0x39a09ae0359598f7:0xaaf41ee144cd94eb!8m2!3d29.2182644!4d79.5129767?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv meerut">
                      <h3>Meerut </h3>
                      <p>The Wills Lifestyle Store, 174 A B Bombay Bazar, SADAR BAZAR MEERUT 250001<br>
                        Phone: 0121-4055245 <span class="mappin"><a href="https://www.google.com/maps/place/Bombay+Bazar,+Sadar+Bazaar,+Meerut,+Uttar+Pradesh+250001,+India/@28.998174,77.6896206,15z/data=!3m1!4b1!4m5!3m4!1s0x390c65a833509727:0x5e5e376c7e566de2!8m2!3d28.998156!4d77.6983754?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv delhi">
                      <h3>Delhi</h3>
                      <p>ITC Maurya, Shopping Arcade 2, Diplomatic Enclave<br>
                        Phone: 011- 46215365 <span class="mappin"><a href="https://www.google.com/maps/place/ITC+Maurya/@28.9985027,77.4181919,10z/data=!4m8!1m2!2m1!1swills+lifestyle+ITC+Maurya,+Shopping+Arcade+2,+Diplomatic+Enclave+delhi!3m4!1s0x390d1d402faf44c9:0x89c489e861057115!8m2!3d28.5970948!4d77.1736739?hl=en-US" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p>Shop No. F-41, South Extention, Part -1<br>
                        Phone: 011- 41648523/ 524 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.5691879,77.2169702,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce25ec0000001:0x69f44ca0c4c98882!8m2!3d28.5691832!4d77.2191589?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p>Shop No. 227, DLF Promenade Mall, Vasant Kunj<br>
                        Phone: 011 -41551455/41551355 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.5426968,77.1207332,13z/data=!3m1!5s0x390d1dcec97e8203:0xcee4a177bc306f10!4m8!1m2!2m1!1swills+lifestyle+Vasant+Kunj+delhi!3m4!1s0x390d1dceb6975a13:0xb6bded8afe327754!8m2!3d28.5427238!4d77.156342?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p>Select City Walk, F - 70, District Centre, Press Enclave Rd, Saket New Delhi 110017<br>
                        Phone:011 - 42658267 / 268 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.529119,77.2175029,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce18afd2a6669:0xd8d0ae68877ead49!8m2!3d28.5291143!4d77.2196916?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p>Shop No. E-2, Inner Circle, Connaught Place<br>
                        Phone: 011-64717773 / 011-64717774 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle,CP/@28.6313145,77.2175942,17z/data=!3m1!4b1!4m5!3m4!1s0x390cfd365ac28265:0x467a6b13c09a404c!8m2!3d28.6313098!4d77.2197829?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <!--<p>Plot No 1B3, City Centre Mall, Sector 10, Twin District Centre 1, Opp Rajiv Cancer Institure, Rohini<br>
                        Phone: 011 - 64640766/ 767 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.7243887,77.1104019,17z/data=!3m1!4b1!4m5!3m4!1s0x390d01488d6a1857:0x87142fbfb97c2fe3!8m2!3d28.724384!4d77.1125906?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>-->
                      <p>G-8 to G-12 Manish Megha Plaza, plot no-13, Sector-5 Dwarka, New Delhi-110075<br>
                        Phone: 011-47016307 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Life+Style/@28.5958906,77.0497142,17z/data=!4m8!1m2!2m1!1swills+lifestyle+Bhagwaati+Plaza,+Plot+No.12,+Sector-5,+Dwarka+delhi!3m4!1s0x390d1ad9c92029e5:0x31ff37069f483ee8!8m2!3d28.594956!4d77.053038?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <h3>Gurgaon</h3>
                      <p>Shop No F-125, First Floor Ambience Mall, Ambience Island, NH-8Gurgaon 122001<br>
                        Phone: 0124-6460667/ 68 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.5039758,77.0947933,17z/data=!3m2!4b1!5s0x390d194e9e7836b9:0x2ee084f3a2a6a2d5!4m5!3m4!1s0x390d19227d12c8d5:0x62a309f3e8ddb49a!8m2!3d28.5039711!4d77.096982?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p>Shop No.17, 18, 19 & 20, Ground Floor, Metropolitan Mall, M G Road<br>
                        Phone: 0124 - 4104444/ 446 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.4811218,77.0778234,17z/data=!3m2!4b1!5s0x390d18d8753db56f:0x25757141f00593d2!4m5!3m4!1s0x390d19227d6215f5:0xefbbcfd241968201!8m2!3d28.4811171!4d77.0800121?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <h3>Noida</h3>
                      <p>Shop No- B-210 & 211 A, Ist Floor, DLF Mall Of India , Sector - 18, Noida 201301<br>
                        Phone: 0120-2479847 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.5703217,77.3196313,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce44f4d1b2ae1:0x371390cfe97a9ad0!8m2!3d28.570317!4d77.32182?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <!--<p>Shop No. F 14, Sector 18<br>
                        Phone: 0120- 6491802/ 03 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle+Store/@28.5708849,77.3219018,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce44ed694f223:0x55b704b986760163!8m2!3d28.5708802!4d77.3240905?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>-->
                      <h3>Ghaziabad</h3>
                      
                      <p>Shop No 23 & 46, Ground Floor, Level 2, Shipra Mall, Indirapura, Ghaziabad - 201014<br>
                       Phone: 0120-4106885 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@28.6339577,77.3678253,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce553b393d58b:0xd16656604558a612!8m2!3d28.633953!4d77.370014?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <h3>Faridabad</h3>
                      <p>Shop No-G-36 D, Gf, Crown Interiorz Mall, Delhi Mathura Road,Sec-35, Faridabad- 121003<br>
                        Phone: 0129-4118132 <span class="mappin"><a href="https://www.google.com/maps/place/Peter+England/@28.4105917,77.3096353,17z/data=!3m1!4b1!4m5!3m4!1s0x390cddb65a66ab65:0xc86786550e4c1e54!8m2!3d28.410587!4d77.311824?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv bareilly">
                      <h3>Bareilly</h3>
                      <p> Shop No. - G-30, Ground Floor, Phoenix United Mall, Pilibhit Bypass Road, Bareilly - 243006<br>
                        Phone: 0581-2583242 <span class="mappin"><a href="https://www.google.com/maps/place/Phoenix+United/@28.3946904,79.4544702,17z/data=!4m8!1m2!2m1!1swills+lifestyle+Phoenix+United+Mall,+Pilibhit+Bypass+Road,+Bareilly!3m4!1s0x39a0068f06651d8f:0x9270349580ba106e!8m2!3d28.3947713!4d79.4565317?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv jaipur">
                      <h3>Jaipur</h3>
                      <p> Shop NO- 1, Gulab Niwas, M. I Road Jaipur 302001<br>
                        Phone: 0141 - 2363422 / 4017422 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@26.9165798,75.8108528,17z/data=!3m1!4b1!4m5!3m4!1s0x396db6ac769a43f9:0xac0f52b011e36a8d!8m2!3d26.916575!4d75.8130415?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No. G23,G24,G25 , Triton The Mega Mall, Jhotwara Road , Near Chomu Pulia Jaipur 302012<br>
                        Phone: 0141 - 5156731/6540221 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Life+Style/@26.9166217,75.7955319,14z/data=!4m8!1m2!2m1!1swills+lifestyle+Jhotwara+Road+,+Near+Chomu+Pulia+Jaipur!3m4!1s0x396db3a2c44133db:0xd6683c21ee995e64!8m2!3d26.9412942!4d75.7718211?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> UGF-12/13(Block B), JLN Marg, Malviya Nagar, World Trade Park, Jaipur-302017<br>
                        Phone: 0141-2728678 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@26.8487488,75.8032453,17z/data=!3m1!4b1!4m5!3m4!1s0x396db5e0475e992d:0xfa81e0290d0e660f!8m2!3d26.848744!4d75.805434?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Ground Floor No.1,Sdc Monarch Plot No 236/237,Amarpali Marg,Vaishali Nagar<br>
                        Phone: 0141 - 4020312 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@26.9119423,75.7455554,17z/data=!3m1!4b1!4m5!3m4!1s0x396db49d3bb7ff0f:0xf1f1fdf2d4db2cdd!8m2!3d26.9119375!4d75.7477441?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv jodhpur">
                      <h3>Jodhpur </h3>
                      <p> Shop no.1, 4th C Road, opposite Sardarpura Thana, Sardarpura, Jodhpur - 342001<br>
                        9829775543 <span class="mappin"><a href="https://www.google.com/maps/place/WILLS+Lifestyle+(divy+%26+prashant+gangwani)+(JODHPUR)/@26.2764059,73.0068493,17z/data=!3m1!4b1!4m5!3m4!1s0x39418c30e586cc65:0x3810d402bd4c29c7!8m2!3d26.2764011!4d73.009038?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv udaipur">
                      <h3>Udaipur </h3>
                      <p> Unit No. : F- 01-08 First Floor The Celebration Mall, N. H. 8 Opposite Devendra Dham Bhuwana, Udaipur-313004<br>
                        Phone:0294-5158810 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@24.6127576,73.7004819,17z/data=!3m2!4b1!5s0x3967e5a991e4e5e3:0xcb31bb7dc3bcde15!4m5!3m4!1s0x3967e5a98d1ce019:0x25fc35b0ca24f90c!8m2!3d24.6127527!4d73.7026706?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv agra">
                      <h3>Agra </h3>
                      <p> Shop No 1 & 2, Shopping Arcade, Hotel ITC, Mughal, Fatehabad Road, Taj Gang, AGRA - 282001<br>
                        Phone: 0562 - 4063933 <span class="mappin"><a href="https://www.google.com/maps/place/ITC+Mughal/@27.1284588,78.0365644,14z/data=!4m8!1m2!2m1!1swills+lifestyle+ITC+Mughal+Agra!3m4!1s0x390cfcdfdaaf2f89:0x44a5530eee2ed87c!8m2!3d27.160929!4d78.044074?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <!--<p> 2/216A, Ground Floor, Swadeshi Bima Nagar, M.G. Road<br>
                        Phone: 0562 - 2520552 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@27.1658135,77.9942658,13z/data=!4m8!1m2!2m1!1swills+lifestyle+M.G.+Road+Agra!3m4!1s0x397477443a639907:0x87ef7444b1009b74!8m2!3d27.2030707!4d78.0044964?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>-->
                    </div>
                    <div class="address-dv lucknow">
                      <h3>Lucknow </h3>
                     
                      <p> Shop No - B1 First Floor Fun Republic Mall Opp RBI Gomti Nagar Lucknow - 226010<br>
                        Phone: 0522-4029871 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@26.8581596,80.9739192,17z/data=!3m2!4b1!5s0x399bfd377ef09a01:0xaf9f05b314e61645!4m5!3m4!1s0x399bfd377ec3e985:0xbcbc92546d067a76!8m2!3d26.8581548!4d80.9761079?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv patna">
                      <h3>Patna </h3>
                      <p> Shop no.G06&07,P&M Mall Patna,Patliputra Kurji Road, Patliputra Industrial Area<br>
                        Phone: 0612-2270710 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@25.6339697,85.103728,17z/data=!3m1!4b1!4m5!3m4!1s0x39ed577e05d03b67:0x9f94ee0ad04e8bf8!8m2!3d25.6339649!4d85.1059167?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      
                    </div>
                    <div class="address-dv gangtok">
                      <h3>Gangtok </h3>
                      <p> Planet Square, Opp. Hotel Migtin,Tibet Road, Gangtok - 737101, Sikkim<br>
                        Phone:03592-200099 <span class="mappin"><a href="https://www.google.com/maps/place/Tibet+Rd,+Gangtok,+Sikkim+737101,+India/@27.3303347,88.6122435,17z/data=!3m1!4b1!4m5!3m4!1s0x39e6a56ad2868991:0xa15c4db3b1cc2ef0!8m2!3d27.33033!4d88.6144322?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv siliguri">
                      <h3>Siliguri </h3>
                      <p> Shop No 20 & 21, Cosmos Mall, Lower Ground Floor, 2Nd Mile Sevoke Road<br>
                        Phone: 0353 - 2545255, 2545254 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@26.7317955,88.4060629,15z/data=!3m1!5s0x39e441024c00a895:0xf508db744ec0e57d!4m8!1m2!2m1!1sWills+lifestyle+Gangtok,+Sikkim,+India!3m4!1s0x0:0xe2f8503ad092c127!8m2!3d26.7387262!4d88.4343061?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Unit No. E0007, City Centre, Siliguri,<br>
                        Ambuja Realty Development Ltd, Uttorayon Township, Nh 31<br>
                        Phone: 0353-2576114/2576115 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@26.7317955,88.4060629,15z/data=!3m1!5s0x39e446cee084ee4d:0xc743fe2ebe48e095!4m8!1m2!2m1!1sWills+lifestyle+Gangtok,+Sikkim,+India!3m4!1s0x39e446ceec6d927b:0x268d1ebb063d3c02!8m2!3d26.7248274!4d88.3953285?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    <p>ITC LIMITED- LRBD, Unit 3 & 4, Vega Circle Mall,<br>
                         3rd Mile, Sevoke Road, Siliguri - 734001 (West Bengal)<br>
                        Phone: 8443029135<span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@26.7317955,88.4060629,15z/data=!3m1!5s0x39e446cee084ee4d:0xc743fe2ebe48e095!4m8!1m2!2m1!1sWills+lifestyle+Gangtok,+Sikkim,+India!3m4!1s0x39e446ceec6d927b:0x268d1ebb063d3c02!8m2!3d26.7248274!4d88.3953285?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
</div>
                    <div class="address-dv guwahati">
                      <h3>Guwahati </h3>
                      <p>Shop No. A,B,C,E & P, Adams Plaza, G S Road, Christian Basti<br>
                        Phone: 0361-2349922 <span class="mappin"><a href="https://www.google.com/maps/place/Adam+Plaza/@26.1562538,91.7755469,17z/data=!3m1!4b1!4m5!3m4!1s0x375a5913dcfcbc79:0xd9052eec1a4d4b!8m2!3d26.156249!4d91.7777356?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv imphal">
                      <h3>Imphal</h3>
                      <p>M/s Jupiter Enterprise, Thangmeiband Meisnam Leikai, Opposite Thau Ground, Manipur-795004<br>
                        Phone:0385-2416313 <span class="mappin"><a href="https://www.google.com/maps/place/Thangmeiband+Hijam+Leikai/@24.8171292,93.9364729,17z/data=!3m1!4b1!4m5!3m4!1s0x374927a562e6cbc9:0x73dc09623e20f0d8!8m2!3d24.8171243!4d93.9386616?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv agartala">
                      <h3>Agartala</h3>
                      <p>Gulmohar House, M . L Plaza (Basement), Mantri Bari Road, Tripura West, Agartala- 799001.<br>
                        Phone: 0381-2328983 <span class="mappin"><a href="https://www.google.com/maps/place/ML+Plaza/@23.8284883,91.276824,17z/data=!3m1!4b1!4m5!3m4!1s0x3753f46d5afeff49:0x85779d784cfe2930!8m2!3d23.8284834!4d91.2790127?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv gwalior">
                      <h3>Gwalior</h3>
                      <p>Shop no. F5 , Dindayal City Mall , MLB Road , Gwalior - 475110<br>
                        Phone: 0751-4019551 <span class="mappin"><a href="https://www.google.com/maps/place/Dindayal+Mall/@26.2096047,78.1641879,17z/data=!3m2!4b1!5s0x3976c42842f59eaf:0xd6a1f030eaad1930!4m5!3m4!1s0x3976c42868348417:0xed839ed3252040!8m2!3d26.2095999!4d78.1663766?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv ahmedabad">
                      <h3>Ahmedabad</h3>
                      <p>Shop No. 3 & 6, Mansi Infrastructure Pvt Ltd, Ground Floor,Time Square Building, C.G.Road, Navrangpura<br>
                        Phone: 079 - 40062339/303 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@23.0334766,72.5229481,15z/data=!4m8!1m2!2m1!1swills+lifestyle+near+Navrangpura,+Ahmedabad,+Gujarat,+India!3m4!1s0x395e848b3af6491d:0xdf7d4414332d78a3!8m2!3d23.0280808!4d72.5580363?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No. 231 & 232, Iscon Mega Mall, Near Iscon Temple, Sarkhej National Highway<br>
                        Phone: 079 - 40026305 / 307 <span class="mappin"><a href="https://www.google.com/maps/place/wills+lifestyle/@23.0334766,72.5229481,15z/data=!4m8!1m2!2m1!1swills+lifestyle+near+Navrangpura,+Ahmedabad,+Gujarat,+India!3m4!1s0x395e84cc67d736a7:0xd79f9d95a3541dff!8m2!3d23.0270554!4d72.5241584?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> G-29, Alpha One,Near Vastrapur Lake,Vastrapur<br>
                        Phone: 079 - 40062323/40072323 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@23.0334766,72.5229481,15z/data=!3m1!5s0x395e84b744192cc3:0xc05f6327e17fa72c!4m8!1m2!2m1!1swills+lifestyle+near+Navrangpura,+Ahmedabad,+Gujarat,+India!3m4!1s0x395e84b73658b725:0x26cf133333873c84!8m2!3d23.0398587!4d72.531849?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv baroda">
                      <h3>Baroda </h3>
                      <p> Flagship-2, Ground Floor, Kshitij's Baroda Mall, Sarabhai Circle, BARODA - 390007<br>
                        Phone: 0265-3914740 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle+Store/@22.3181221,73.167179,17z/data=!3m2!4b1!5s0x395fc8b830ee8209:0x8375b984ddcd4ec!4m5!3m4!1s0x395fc8b83166b13b:0x74a1866a4e003fda!8m2!3d22.3181172!4d73.1693677?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> G-18, Inorbit Mall, Opp. Alembic School, Subhanpura, Vadodara - 390023<br>
                        Phone: 0265 - 2291011/033 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@22.3181467,73.1606129,15z/data=!3m1!5s0x395fc8c71610a547:0x81ad8f56cae15ea0!4m8!1m2!2m1!1swills+lifestyle+Vadodara,+Baroda!3m4!1s0x395fc8c7230cce87:0xa721aa9396e81319!8m2!3d22.3225508!4d73.1656602?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv indore">
                      <h3>Indore </h3>
                      
                      <p> Shop No.12, Ground Floor , Treasure Island, 11, Tukoganj, Main Road, M. G. Road, Madhya Pradesh City- Indore Pincode- 452001 Phone-0731-4073723 <span class="mappin"><a href="https://www.google.co.in/maps/place/Treasure+Island+Mall/@22.7213186,75.8763919,17z/data=!3m1!4b1!4m5!3m4!1s0x3962fd1640000001:0xe20580f2dd7ce8f3!8m2!3d22.7213137!4d75.8785806?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv bhopal">
                      <h3>Bhopal </h3>
                      <p> Shop No. Gf-41, Db City, Opp Hotel Surendra Villas, M P Nagar<br>
                        Phone: 0755 - 6644244 / 6644245 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@23.2329049,77.4277895,17z/data=!3m2!4b1!5s0x397c4267483078c3:0xd5e54ffe1575b512!4m5!3m4!1s0x397c42674ce7a653:0x3075e0f5eada0316!8m2!3d23.2329!4d77.4299782?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  bilaspur">
                      <h3> Bilaspur </h3>
                      <p>Shop No: G-22, Rama Magneto Mall, Dindayal Upadhaya Chowk,
                        Sreekant Verma Road, Bilaspur-Chattisgarh-474004<br>
                        Phone: 9977002880 <span class="mappin"><a href="https://www.google.com/maps/place/Rama+Magneto+The+Mall/@22.0717044,82.1471382,17z/data=!4m8!1m2!2m1!1swills+lifestyle+Rama+magnto+mall,+Bilaspur!3m4!1s0x3a280b4076594541:0x7c7fc2ae353a23dd!8m2!3d22.0716343!4d82.1495276?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  kolkata">
                      <h3> Kolkata </h3>
                      <p> Shop No. 19-B, Ground Floor, Shakespeare Sarani<br>
                        Phone: 033 - 22826102 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Life+Style/@22.5453651,88.3502316,17z/data=!3m1!4b1!4m5!3m4!1s0x3a0277174ff15cc7:0x84bbb34bf7d808f!8m2!3d22.5453602!4d88.3524203?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> C 008 & C 010, Block DC, City Centre Mall, Salt Lake City, Sector - 1<br>
                        Phone: 033 - 23589152 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle+(CC1)/@22.5883053,88.4064909,17z/data=!3m2!4b1!5s0x3a0275dd89a41613:0x22f27effafe89935!4m5!3m4!1s0x3a0275e77e757845:0xb4c52a70894c18ac!8m2!3d22.5883004!4d88.4086796?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Quest Mall, 1st Floor Shop no: 126-129, 33 Syed Amir ali Avenue Kolkata 700017<br>
                        Phone:033-22870828/29 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle+Quest+Mall/@22.5391429,88.3634575,17z/data=!3m1!5s0x3a0276de3a0370ad:0xbd3e6abe25c6ab80!4m8!1m2!2m1!1swills+lifestyle+Syed+Amir+ali+Avenue+Kolkata!3m4!1s0x3a0276de31dc8ceb:0x26bdbfbeea1fdc2a!8m2!3d22.539312!4d88.365606?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  surat">
                      <h3> Surat </h3>
                      <p>Virtuous Mall FP29, Rundh Village Dumas Road, Magdalla<br>
                        Phone: 0261 679 5058/9. <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@21.1534577,72.7612695,16z/data=!3m1!5s0x3be04dfa41ddebe3:0xeca38940f9714975!4m8!1m2!2m1!1swills+lifestyle+Magdalla,+Surat!3m4!1s0x3be0527fe30b1703:0xe92339c628d2d451!8m2!3d21.1452003!4d72.7572728?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  nasik">
                      <h3> Nasik </h3>
                      <p>Shop No. Ug-7, City Centre Mall, Lawate Nagar, Untwadi Road, Near Mico Circle<br>
                        Phone: 0253 - 2232172 / 73 <span class="mappin"><a href="https://www.google.com/maps/place/WILLS+LIFESTYLE/@19.9906919,73.7596363,17z/data=!3m1!4b1!4m5!3m4!1s0x3bddeb70e54f39af:0xa315566691d7b441!8m2!3d19.9906869!4d73.761825?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  mumbai">
                      <h3> Mumbai </h3>
                      <p> ITC Grand Central, 287, Dr Babasaheb Ambedkar Road, Parel<br>
                        Phone: 022 - 24101010 / 04 <span class="mappin"><a href="https://www.google.com/maps/place/ITC+Grand+Central/@18.9984426,72.836461,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7c84440013f13:0x5c5fb613a8b199d8!8m2!3d18.9984375!4d72.8386497?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Store NO- F-15, 1st Floor, R-City Mall, South Wing, LBS Marg, Ghatkopar (W), Mumbai- 400086<br>
                        Phone:022-69000550 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@19.0994772,72.9136638,17z/data=!3m2!4b1!5s0x3be7c7cf4d57f8d7:0xee817aba7e27f090!4m5!3m4!1s0x3be7c7cf4fe6af7d:0xcd9bff1c292e038d!8m2!3d19.0994721!4d72.9158525?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Unit No 4 & 5, Skyzone Level 1/ Block 2, Phoenix Mills Compound, 462 Senapati Bapat Marg, Lower Parel<br>
                        Phone: 022 - 40040603 / 04 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@18.9946521,72.8230967,17z/data=!3m2!4b1!5s0x3be7ce8c621dca7f:0x3fd13807ce5c4420!4m5!3m4!1s0x3be7ce8cf5bf3487:0x2961c0650310dc7c!8m2!3d18.994647!4d72.8252854?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <!--<p> Unit No.F 34, International Business Park, Oberoi Garden City, Off Western Express Highway, Goregaon (East)<br>
                        Phone: 022 - 28432127/ 28 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@19.1739064,72.8587633,17z/data=!3m2!4b1!5s0x3be7b7a9b549eb6b:0x766aee4d5d0c81c0!4m5!3m4!1s0x3be7b7a84aa18083:0xf8163a95e93a7bb3!8m2!3d19.1739013!4d72.860952?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>-->
                      <p> F8 & F9, 1St Floor , Inorbit Malls Pvt Ltd , Mindspace , Malad Link Road , Malad (West)<br>
                        Phone: 022 - 28712224 / 40032086 <span class="mappin"><a href="https://www.google.com/maps/place/Wills+Lifestyle/@19.1790978,72.8309763,16z/data=!3m1!5s0x3be7b6589f477e19:0xa189ab3cb5fd26e0!4m8!1m2!2m1!1swills+lifestyle++Malad+west+Mumbai!3m4!1s0x3be7b6589e0db2d1:0xd707865dade855e0!8m2!3d19.1730683!4d72.8359009?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No.G-6, Market City Mall, Security Gate No.3, Sunder Baug Lane, Kamani Junction, Near Naaz Hotel, Kurla (West)<br>
                        Phone: 022 - 61801415 / 16 <span class="mappin"><a href="https://www.google.com/maps/place/Phoenix+Marketcity/@19.0866587,72.886725,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7c887efb78b9f:0x9f9dc99c3119470a!8m2!3d19.0866536!4d72.8889137?hl=en-IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No. G 24, Inorbit Mall, Plot No. 39/1, Sector 30A, Vashi<br>
                        Phone: 022 - 27810133 / 65251162 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@19.0655355,72.998888,17z/data=!3m2!4b1!5s0x3be7c14e8be58155:0xbb952d1a963f2e85!4m5!3m4!1s0x3be7c136b2c080cb:0x490ce2319d8d3eaa!8m2!3d19.0655304!4d73.0010767?hl=IN" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Wills Lifestyle Unit No.FF42,First  Floor, Pokhran Road No.2 . Subhash Nagar, Next to Jupiter Hospital , Thane (West) 400610<br>
                        Phone:  9819310362 <span class="mappin"><a href="https://www.google.co.in/maps/place/Globus/@19.2309581,72.9495393,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7bbd5e3179d7b:0x562e11bd3e0c2b63!8m2!3d19.230953!4d72.951728?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                        <p> Wills Lifestyle Unit Nos. F-51/52-SH, First Floor, L&T Seawoods Grand Central, plot R-1, Sector 40, Seawoods Railway Station, Nerul Node, Navi Mumbai, Mumbai - 400706<br>
                        Phone:  9819310362 <span class="mappin"><a href="https://www.google.co.in/maps/place/Globus/@19.2309581,72.9495393,17z/data=!3m1!4b1!4m5!3m4!1s0x3be7bbd5e3179d7b:0x562e11bd3e0c2b63!8m2!3d19.230953!4d72.951728?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No-1, ITC Maratha,Sahar, International Airport Road, Andheri East, Mumbai-400099<br>
                        Phone: 022-40342089 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle+Shop/@19.1040028,72.8674412,17z/data=!3m1!4b1!4m5!3m4!1s0x3a027719edb0a0e7:0xa97b552306df6b67!8m2!3d19.1039977!4d72.8696299?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv pune">
                      <h3> Pune </h3>
                      <p> Amanora Town Centre, Amanora Park Town,Shop No.13, Ground Floor, West Block Building, Opposite Magarpatta, Hadapsar Kharadi Bypass, Hadapsar<br>
                        Phone: 020 - 67267846 / 67267847 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@18.5293139,73.8733328,14z/data=!3m1!5s0x3bc2c18a19904107:0x9cc40d05454d8a25!4m8!1m2!2m1!1swills+lifestyle+Hadapsar+mumbai!3m4!1s0x3bc2c18a22deeb25:0x36b61010e052c4d4!8m2!3d18.518837!4d73.9342046?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <!--<p> Shop No.1204/22, Ground Floor, Shivaji Nagar, Junglee Maharaj Road<br>
                        Phone: 020 - 66019401 / 02 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@18.5223194,73.8452915,17z/data=!3m1!4b1!4m5!3m4!1s0x3bc2c0795966459b:0xcf8374d1c4bacc8!8m2!3d18.5223143!4d73.8474802?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>-->
                      <p> Phonix Market City Mall, Unit No- 55, 1st Floor, Sr. No- 207, Behind Baker Gauges, Viman Nagar, Nagar Road, Pune-411014<br>
                        Phone: 020-66890565/64 <span class="mappin"><a href="https://www.google.co.in/maps/place/Phoenix+Market+City/@18.5619579,73.8994168,14z/data=!4m8!1m2!2m1!1swills+lifestyle+Phonix+Market+City+Mall+pune!3m4!1s0x3bc2c147b8b3a3bf:0x6f7fdcc8e4d6c77e!8m2!3d18.5619579!4d73.9169263?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  hyderabad">
                      <h3> Hyderabad </h3>
                      <p>Shop No. 4 & 5, Ground Floor, G S Chambers, Nagarjuna Circle, Banjara Hills<br>
                        Phone: 040 - 66369200/66364700 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@17.4191501,78.4464213,17z/data=!3m1!4b1!4m5!3m4!1s0x3bcb973699c5e78d:0x384551fd548d5f6b!8m2!3d17.419145!4d78.44861?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No.1&2, H.No.3-6-108/2, Kuchkulla House, Himayat Nagar<br>
                        Phone: 040 - 66255160 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@17.4119603,78.4557882,15z/data=!4m8!1m2!2m1!1swills+lifestyle+himayat+nagar+hyderabad!3m4!1s0x3bcb99e0a94d5083:0x8f34cae6364efac8!8m2!3d17.404735!4d78.480476?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Unit no.G1-12/ Ground 1, In orbit Malls (India) Pvt. Ltd, API I C Software Layout, Opposite Durgam Cheruvu,Madhapur, Hyderabad-500081<br>
                        Phone:040-40136384/40136392 <span class="mappin"><a href="https://www.google.co.in/maps/place/Arrow+-+Arvind+Lifestyle+Brands+Limited+-+MG+TRENDS/@17.434759,78.3844155,17z/data=!4m8!1m2!2m1!1swills+lifestyle+orbit+Malls+(India)+Pvt.+Ltd,+API+I+C+Software+Layout+hyderabad!3m4!1s0x3bcb915831e4691f:0xb2211f0014185e2b!8m2!3d17.435581!4d78.386864?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  raipur">
                      <h3> Raipur </h3>
                      
                      <p>GF-11, Magneto Mall, Raipur, Chattisgarh-492001 <span class="mappin"><a href="https://www.google.co.in/maps/place/WILLS+LIFESTYLE/@21.2362062,81.6681322,14z/data=!3m1!5s0x3a28dd9d430095ef:0x78213ab9043f80ec!4m8!1m2!2m1!1sWills+Lifestyle,+Raipur,+Chhattisgarh!3m4!1s0x3a28dd9d467ce30d:0xea0e100987a26a43!8m2!3d21.2549397!4d81.6466455?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv bhubaneswar">
                      <h3> Bhubaneswar </h3>
                      <p> Plot No.1, Sriya Square, Janpath, B751001.<br>
                        Phone: 0674 - 2380831 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@20.2751959,85.8411703,17z/data=!3m1!4b1!4m5!3m4!1s0x3a19a75157c04b75:0xa59d6e60d42ebc09!8m2!3d20.2751909!4d85.843359?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
             
                    <div class="address-dv goa">
                      <h3> Goa </h3>
                      <p> Below Goa Lodge, MG Road, Opposite Old High Court, Panjim<br>
                        Phone: 0832 - 6641222 / 6642616 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@15.5005772,73.8263593,17z/data=!3m1!4b1!4m5!3m4!1s0x3bbfc088de48b5ab:0x7facaaa87ae0b680!8m2!3d15.500572!4d73.828548?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Wills Lifestyle, Shop no.10, GF, Mall De Goa, Alto-Porvorim,Bardez, Goa-403521<br>
                        Phone: 0832-2414015/16 <span class="mappin"><a href="https://www.google.co.in/maps/place/Mall+De+Goa/@15.5256769,73.8249278,17z/data=!3m1!4b1!4m5!3m4!1s0x3bbfc06ce1f55fcb:0xafe5a7776f5bf4e4!8m2!3d15.5256717!4d73.8271165?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv bengaluru">
                      <h3> Bengaluru </h3>
                      <p> Itc Royal Gardenia Hotel, 1  Residency Road Bengaluru 560025<br>
                        Phone: 080 - 43455301 / 302 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@12.9743905,77.6056433,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae167dfe7700f7:0xf0bf4d5bc00781c0!8m2!3d12.9743853!4d77.607832?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Municipal No 664, Binnamangala Ist Stage, 100 Feet Road, Indiranagar<br>
                        Phone: 080 - 41715665 / 415172165 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@12.9772957,77.6389444,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae16a59a20358d:0x9ced37b113c6a3ec!8m2!3d12.9772905!4d77.6411331?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No.03&04, Orion Mall,Brigade Gateway, Dr.Rajkumar Road, Behind Metro, Malleshwaram <br>
                        Phone: 080 - 22682021 / 22 <span class="mappin"><a href="https://www.google.co.in/maps/place/Orion+Mall/@13.0117437,77.553309,17z/data=!4m8!1m2!2m1!1swills+lifestyle+Rajkumar+Road,+Behind+Metro,+Malleshwaram,+bengaluru!3m4!1s0x3bae1629af0c075f:0x3c984ebd22cd5d54!8m2!3d13.010924!4d77.5549633?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <!--<p> Shop No.222. 2<sup>Nd</sup> Floor, Garuda Mall, Magarath Road Bangalore 560025<br>
                        Phone: 080 - 40937784 / 40937791 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@12.9703802,77.6070843,17z/data=!3m2!4b1!5s0x3bae168044f3b1e7:0x4d6cadcaf1dc4aa5!4m5!3m4!1s0x3bae16805c779911:0x81b6dfbae8bd45e5!8m2!3d12.970375!4d77.609273?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p> -->
                     
                      <p> Sri Arcade, No. 16 (Old No. 17), 11Th Main, 3Rd Block, Jayangar East<br>
                        Phone: 080 - 41211435 / 41210073 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@12.9309157,77.5840024,17z/data=!3m1!4b1!4m5!3m4!1s0x3bae15a293d13bb7:0x8a78859cb2f337db!8m2!3d12.9309105!4d77.5861911?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  chennai">
                      <h3> Chennai </h3>
                      <p>ITC Grand Chola, New No. 63, Old No.37,Mount Road, Guindy,<br>
                        Phone: 044 -22301206 / 207 <span class="mappin"><a href="https://www.google.co.in/maps/place/Ottimo+Cucina+Italiana/@13.0097318,80.2114043,17z/data=!3m1!4b1!4m5!3m4!1s0x3a52676d4886723b:0x49155fb3f03eb0cb!8m2!3d13.0097266!4d80.213593?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No. 19, Ground Floor, Quaiser Tower, Khader Nawaz Khan Road, Nungambakkam<br>
                        Phone: 044 - 28332513/ 514 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@13.0604583,80.2464935,17z/data=!3m1!4b1!4m5!3m4!1s0x3a52666a39d75a03:0xb37d20eff439f213!8m2!3d13.0604531!4d80.2486822?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No. S 114, Express Avenue Mall, No: 49,50L, Whites Road, Roypettah, Chennai- 600014 <br>
                        Phone: 044-28464235 / 36 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Lifestyle/@13.0661812,80.2341816,15z/data=!4m8!1m2!2m1!1sWills+lifestyle,++Express+Avenue+Mall,+Whites+Road,+Chennai!3m4!1s0x3a52663d5c4f09e9:0xbb30ee3f7a5de97!8m2!3d13.0589454!4d80.2647104?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <!--<p> Shop No. 6, Chennai Citi Centre Mall, Dr.Radha Krishnan Salai, Mylapore <span class="mappin"><a href="https://www.google.co.in/maps/place/Chennai+Citicentre/@13.0516236,80.2569793,16z/data=!4m8!1m2!2m1!1sWills+lifestyle,++Chennai+Citi+Centre+Mall,+Mylapore,+Chennai!3m4!1s0x3a52662982a8f3d3:0xef3a40997a0631cd!8m2!3d13.0428652!4d80.2738073?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>-->
                      <p> Shop No.F 23,Phoenix Market City Mall, Plot No 142, Velachery Main Road<br>
                        Phone: 044 - 30083322, 044 - 30083323 <span class="mappin"><a href="https://www.google.co.in/maps/place/LIFE+STYLE/@12.9914082,80.2143053,17z/data=!3m2!4b1!5s0x3a52676235f014b5:0x14c7f6f665145ab5!4m5!3m4!1s0x3a526762372b73ed:0xba845088856484eb!8m2!3d12.991403!4d80.216494?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> WILLS LIFESTYLE STORE, Door No 68(Old No 89), MSR Hall, Sir Thyagaraya Road, T Nagar, Chennai 600017 <br>
                        Phone: 044-43322214/15 <span class="mappin"><a href="https://www.google.co.in/maps/place/Sir+Thyagaraya+Rd,+T+Nagar,+Chennai,+Tamil+Nadu/@13.0403269,80.2383054,17z/data=!3m1!4b1!4m5!3m4!1s0x3a526653cfcaf5f3:0x45964e7b90c6c15d!8m2!3d13.0403217!4d80.2404941?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                      <p> Shop No. S 109, Express Avenue Mall, Whites Road, No 49,50L Royahpetta 600014<br>
                        <span class="mappin"><a href="https://www.google.co.in/maps/search/Express+Avenue+Mall,+Whites+Road,+No+49,50L+Royahpetta+600014/@13.0585918,80.2617807,17z/data=!3m1!4b1?hl=en" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                    <div class="address-dv  cochin">
                      <h3> Cochin </h3>
                     
                      <p> Shop no. FA2b, Lulu International Shopping Mall Pvt. Ltd, 50/2392 N.H 17, Edappally<br>
                        Phone: 0484-4038883 , 4038880 <span class="mappin"><a href="https://www.google.co.in/maps/place/Wills+Life+Style/@10.0047688,76.2781896,14z/data=!4m8!1m2!2m1!1swills+lifestyle+Edappally!3m4!1s0x0:0x2a62bb000f8ee593!8m2!3d10.0284158!4d76.3083637?hl=in" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></span> </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 padding-0 hideonmobile">
            <div class="storecontainer-mob">
              <div class="mapcontainer">
                <div class="address-dv" style="display: block">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d15309087.43697231!2d73.75555141229212!3d20.482839859656377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sindia+wills+lifestyle!5e0!3m2!1sen!2sin!4v1467187804409" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv srinagar">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3299.2612009595614!2d74.74878731560399!3d34.21634841651063!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38e19d1be15ef9f1%3A0x64fa694a5ccb341c!2sAllen+Solly+Store!5e0!3m2!1sen!2sin!4v1467099062045" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv jammu">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3357.5115917016783!2d74.85599991557802!3d32.699030895264045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391e84b0e1080bff%3A0xabba560a03723aef!2sApsara+Rd%2C+Jammu+180004!5e0!3m2!1sen!2sin!4v1467184397848" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv delhi">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d112092.68624198603!2d77.11974872719053!3d28.60288327276237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle%2C++delhi+ncr!5e0!3m2!1sen!2sin!4v1467186062387" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv jalandhar">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6817.725135851766!2d75.5756763273334!3d31.30754339239249!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391a5bab6f8ab5e7%3A0xa1f939b92a368c5!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467175769136" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv ludhiana">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d13694.395898409599!2d75.80579423945079!3d30.897877667824357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle+ludhiana!5e0!3m2!1sen!2sin!4v1467184450819" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                
                <div class="address-dv chandigarh">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d27438.686962057876!2d76.77337871804664!3d30.723014217289727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle+chandigarh!5e0!3m2!1sen!2sin!4v1467185171683" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv patiala">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3443.183549699176!2d76.37543631554031!3d30.34573301123062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391028ba36c7add7%3A0x386bb4b0b8218198!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467176080090" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                
                
                <div class="address-dv meerut">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13958.541494449255!2d77.68962063898977!3d28.998174034098607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390c65a833509727%3A0x5e5e376c7e566de2!2sBombay+Bazar%2C+Sadar+Bazaar%2C+Meerut%2C+Uttar+Pradesh+250001!5e0!3m2!1sen!2sin!4v1467176360920" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv dehradun">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3442.4915021210923!2d78.06827261554066!3d30.36539831029301!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sUnit+No-+LG-06%2C+Pacific+Mall%2C+Mauja+Jakhan%2C+Rajpur+Road%2C+Dehradun!5e0!3m2!1sen!2sin!4v1470395007673" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv haldwani">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3482.17967632582!2d79.51078801552339!3d29.21826906407875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39a09ae0359598f7%3A0xaaf41ee144cd94eb!2sPlanet+Fashion!5e0!3m2!1sen!2sin!4v1467176502009" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv bareilly">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3509.8109973654073!2d79.45434301551153!3d28.394776001551705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39a0068f06651d8f%3A0x9270349580ba106e!2sPhoenix+United!5e0!3m2!1sen!2sin!4v1467176669582" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv jaipur">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d28458.191286910678!2d75.76288321449866!3d26.926529632849785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle+jaipur!5e0!3m2!1sen!2sin!4v1467178747600" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv jodhpur">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3577.58453205675!2d73.00683681548281!3d26.275144293569298!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39418c30e586cc65%3A0x3810d402bd4c29c7!2sWILLS+Lifestyle+(divy+%26+prashant+gangwani)+(JODHPUR)!5e0!3m2!1sen!2sin!4v1467178865969" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv udaipur">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3627.316980471232!2d73.7004819154621!3d24.612757561201143!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3967e5a98d1ce019%3A0x25fc35b0ca24f90c!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467178907559" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv agra">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d56795.156383394664!2d77.99426577930755!3d27.165813466525723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle%2C+agra!5e0!3m2!1sen!2sin!4v1467178954997" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv lucknow">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56962.99310934105!2d80.9123892781438!3d26.834001728621722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2b6894de31863656!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467179000601" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv patna">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d14390.042326543518!2d85.11321913825431!3d25.621174487079358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle%2C+patna!5e0!3m2!1sen!2sin!4v1467179057890" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv siliguri">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d14253.576594656903!2d88.40606288848419!3d26.731795495677776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle%2C+siliguri!5e0!3m2!1sen!2sin!4v1467179086136" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv imphal">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3563.176894715916!2d88.43211811548886!3d26.73873057399418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e4410248deb317%3A0xe2f8503ad092c127!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467179129785" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv agartala">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3649.72184261705!2d91.27682401545295!3d23.828488291704584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3753f46d5afeff49%3A0x85779d784cfe2930!2sML+Plaza!5e0!3m2!1sen!2sin!4v1467179181475" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv guwahati">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3581.2417914160715!2d91.77554691548129!3d26.156253798539407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x375a5913dcfcbc79%3A0xd9052eec1a4d4b!2sAdam+Plaza!5e0!3m2!1sen!2sin!4v1467177193920" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv gwalior">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3579.602539851532!2d78.1641879154819!3d26.209604696311768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3976c42868348417%3A0xed839ed3252040!2sDindayal+Mall!5e0!3m2!1sen!2sin!4v1467177340075" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv bilaspur">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7394.6066945228185!2d82.14125142684644!3d22.076229234027576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a280b4076594541%3A0x7c7fc2ae353a23dd!2sRama+Magneto+The+Mall!5e0!3m2!1sen!2sin!4v1467177454169" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv raipur">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3718.4147492744355!2d81.64433166542547!3d21.255044185389494!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a28dd9d467ce30d%3A0xea0e100987a26a43!2sWILLS+LIFESTYLE!5e0!3m2!1sen!2sin!4v1467177508569" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv kolkata">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d58958.6045274654!2d88.34553051464836!3d22.544939682935762!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1swills+lifestyle+kolkata!5e0!3m2!1sen!2sin!4v1467183931598" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv ahmedabad">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.882509833783!2d72.55584761544404!3d23.028085721899583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848b3af6491d%3A0xdf7d4414332d78a3!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467177615033" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv baroda">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3690.9342634138134!2d73.16720071543632!3d22.318325747879104!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395fc8b83166b13b%3A0x74a1866a4e003fda!2sWills+Lifestyle+Store!5e0!3m2!1sen!2sin!4v1467177663896" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv indore">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3679.5555647474307!2d75.8920084653623!3d22.744754747081906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3962fd53ffecafa3%3A0xe6f894bdec3ccb1b!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467177788624" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv bhopal">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3666.279752311923!2d77.42778951544626!3d23.23290491426311!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x397c42674ce7a653%3A0x3075e0f5eada0316!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467177822937" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv surat">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.187389020406!2d72.75493716542447!3d21.144939639176656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04d919beaaa81%3A0x95f899ed82b61961!2sVR+Surat!5e0!3m2!1sen!2sin!4v1467177878784" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv nasik">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3749.4244722901535!2d73.75963631541353!3d19.990691927776524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bddeb70e54f39af%3A0xa315566691d7b441!2sWILLS+LIFESTYLE!5e0!3m2!1sen!2sin!4v1467177904551" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv bhubaneswar">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3742.6053455390393!2d85.84117031541611!3d20.275195918449572!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a19a75157c04b75%3A0xa59d6e60d42ebc09!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467178020185" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                
                </div>
                <div class="address-dv mumbai">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3772.4826426349014!2d72.83646101540484!3d18.998442559341477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c84440013f13%3A0x5c5fb613a8b199d8!2sITC+Grand+Central!5e0!3m2!1sen!2sin!4v1467178128745" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv pune">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30263.927098506454!2d73.87333280863342!3d18.52931393480866!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2c18a22deeb25%3A0x36b61010e052c4d4!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467178289368" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv hyderabad">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.846553226933!2d78.44642131539221!3d17.419150106471953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb973699c5e78d%3A0x384551fd548d5f6b!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467178337226" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv goa">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3844.699695558765!2d73.82635931537887!3d15.500577158541462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bbfc088de48b5ab%3A0x7facaaa87ae0b680!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467178366089" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv bengaluru">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.0758757001736!2d77.59364531536502!3d12.966996518486484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae15d807b107e5%3A0xf52ffb5f35cc75fb!2sITC+Gardenia%2C+A+Luxury+Collection+Hotel%2C+Bengaluru!5e0!3m2!1sen!2sin!4v1467178397114" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv chennai">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.3951916003707!2d80.21505486536522!3d13.010487267542684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a52676d5bb54be1%3A0xf2c9c9c91db16643!2sITC+Grand+Chola!5e0!3m2!1sen!2sin!4v1467178485264" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv cochin">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.4350512139313!2d76.2806739153538!3d9.980875476100463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080d4e2bdb7e2d%3A0x2dcd0a5983752a40!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1467178533465" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <!--<div class="address-dv burdwan">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3665.939580273992!2d87.86705781544642!3d23.245285613799652!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39f849c3d042e55d%3A0x8cb19836a8ea999b!2sFancy+Market%2C+Grand+Trunk+Rd%2C+Khosbagan%2C+Burdwan%2C+West+Bengal+713101!5e0!3m2!1sen!2sin!4v1470400783584" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="address-dv chandanagar">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3676.4537675269094!2d88.36071976544218!3d22.859690128131334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sPadripara%2C+G.T.+Road%2C+Opp.+Raymonds+Showroom%2C+Chandan+Nagar%2C+Hoogly!5e0!3m2!1sen!2sin!4v1470400838946" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>-->
                             <div class="address-dv jorhat">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3562.6828645611417!2d94.21261811548906!3d26.75449457332325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3746c2be51c45133%3A0x2c0d113bf22bec8f!2sWills+Lifestyle!5e0!3m2!1sen!2sin!4v1470398168792" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  
  <!--#########################Body End Here#########################--> 
  
  <!-- custom scrollbar stylesheet -->
  <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
  <style type="text/css">
.storecontainer-mob .address-dv p{ position: relative; padding-right: 20px}
.mappin{ position: absolute; right: 15px; bottom: 5px; font-size: 20px}
</style>
  <!-- custom scrollbar plugin --> 
  <script src="js/jquery-2.2.4.min.js"></script> 
  <script type="text/javascript" src="js/bootstrap.min.js"></script> 
  <script type="text/javascript" src="js/loader.js"></script> 
  <script type="text/javascript" src="js/customjs.js"></script> 
  <script src="js/jquery.mCustomScrollbar.concat.min.js"></script> 
  <script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
	$('.address-dv iframe').height($(window).height());
	
});
$(window).trigger('resize');
</script> 
  
  <!-- Owl Carousel Assets -->
  <link href="owl-carousel/owl.carousel.css" rel="stylesheet">
  <link href="owl-carousel/owl.theme.css" rel="stylesheet">
  <script src="owl-carousel/owl.carousel.js"></script> 
  <script>
	
	
	$(document).ready(function() {
  function setScroll() {
	  var sliderheight = $('.css-slideshow img').height();
	  //alert(sliderheight);
	  if ($(window).width() > 992) {
		  
    windowHeight = $(window).innerHeight() - sliderheight - 200;
    	//$('.about-content').css('max-height', windowHeight);
		$('.scroll-content').css('height', windowHeight);
		$(".scroll-content").mCustomScrollbar({
					theme:"minimal"
		});	
	  }
	  else{
		$('#wrapper').height($(window).height());
		//$('.about-content').css('max-height','none');
		$('.scroll-content').css('height', 'auto');
		$(".scroll-content").mCustomScrollbar("disable",true);
		
	  }
  };
  setScroll();
  $(window).resize(function() {
    setScroll();
  });
 
});

$(function() {
  $('.storedd-mob select').change(function(){
    $(".address-dv").hide();
    $('.' + $(this).val()).show();
  });
  
  $('.css-slideshow').owlCarousel({
    loop:true,
	nav: true,
	pagination: false,
    items:1,
	autoPlay:true,
	animateOut: 'slideOutDown',
    animateIn: 'flipInX',
	autoplayTimeout:1000,
	responsive:true,
    responsive:{
        0:{
            items:1
            
        },
        600:{
            items:1
            
        },
        1000:{
            items:1
            
        }
    }
});
  
});
</script>
</form>
</body>
</html>
