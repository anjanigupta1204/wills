﻿<%@ page language="C#" autoeventwireup="true" inherits="clublife, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>

<html lang="en">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head id="Head1">
<title>Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/slider.css" rel="stylesheet" type="text/css" />

<link href="lightbox/facebox.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/fakeLoader.css">
</head>
<body>
    <form id="form1" runat="server">
  <uc:navigation id="navigation1" runat="server" />

<!-- inner tab menu -->

<div class="cLeft" align="center"> 
<div id="Clogo" class="text-center">
  	<h3 class="heading">Explore AW'16 Collection </h3>
<ul id="alogo">
  <li><a href="classic.aspx" class="classic">&nbsp;</a></li>
  <li><a href="sport.aspx" class="sport">&nbsp;</a></li>
	<li><a href="signature.aspx" class="signature">&nbsp;</a></li>
  <li><a href="personalcare.aspx" class="personalcare">&nbsp;</a></li>
</ul>
  </div>
<img src="collection/clublife/clublife.gif" class="collection-logo">
  <p class="txt-collection">
    Add adventure to your evenings.<br>
Luxurious and impeccably designed evening wear that is designed to get you the attention you deserve.
  </p>
  
</div>

<!-- end inner tab menu -->

<div class="cRight">
  <div class="abmain">
    <ul id="cslider">
      <li><a href="collection/clublife/clublifebig1.jpg"  id="A1" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#1" ><img src="img/spacer.png" ></a><img src="collection/clublife/clublifebig1.jpg">
        <div class="cContent cbband"><strong class="size16">Glamorous Evenings</strong><br>
Man : Rule the party circuit by donning this velvet blazer paired with printed shirt.<br> Woman: Prevail at the ebony night & bedazzle everyone with your vogue in the embellished scuba dress.<br>
Crafted to perfection with cap sleeves and a circular knit in slim fit, it provides unceasing comfort with added style.
                    
                    </div>
      
      </li>
    </ul>
    <div class="thumbnails">
      <ul>
        <li>
          <div><img src="collection/clublife/Thumbnail/clublifethumb1.jpg" /></div>
        </li>
       
      </ul>
    </div>
    <div class="shopnwBtn"><a href="http://www.shopwillslifestyle.com/custom/wills-classic.html" target="_blank">Shop Now<span></span></a></div>
  </div>
  
  <!-- mobile slider -->
  
  <!--
  <div class="container padding-0 showonmobile">
    <ul class="sliderwrapper">
      <li><img src="collection/clublife/clublifebig1.jpg">
        <p class="content"><strong class="size16">Designed to turn heads</strong><br>
Man : Rule the party circuit by donning this velvet blazer paired with printed shirt.<br> Woman: Prevail at the ebony night & bedazzle everyone with your vogue in the embellished scuba dress.
<br>Crafted to perfection with cap sleeves and a circular knit in slim fit, it provides unceasing comfort with added style. </p>
      </li>
      
     
    </ul>
    <div class="shopnwBtn"><a href="http://www.shopwillslifestyle.com/custom/wills-classic.html" target="_blank">Shop Now<span></span></a></div>
  </div> -->
  
  <!-- end mobile slider --> 
  
</div>
<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script src="js/loader.js"></script>
<script type="text/javascript" src="js/abslider.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<script type="text/javascript" src="lightbox/facebox.js"></script> 


<!--Zoom start here--> 
<script type="text/javascript" src="Gallery/magiczoomplus.js"></script>
<link rel="stylesheet" href="Gallery/magiczoomplus.css" type="text/css" />

<script type="text/javascript" src="js/master.js"></script>
<script type="text/javascript">
$(function() {
	$('#cslider').cslider();
// end of main function
});
</script>
    </form>
</body>
</html>
