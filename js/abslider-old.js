/*
Copyright © Wills Lifestyle
Desigend & HTML5/CSS3/jQuery By Abhay Mandal (Sr. Creative Designer - Sirez Infosystems)
Powered by Sirez Infosystems
*/

function slideractivate(){
	$('#alogo').css({opacity:'0'});
	//$('#alogo').clone().appendTo('#actLogo').removeAttr('id')
	$("#actLogo ul").css({'opacity' : ''});
	//$("#actLogo li, #allcollection>div").css({'display' : 'none'});

$("#xyz").delay(1000);
	//collection logo hover	
	$("#Clogo").hover(
  		function () {
				$('#alogo').stop(true, true).animate({top : '58px',opacity:'1'}, 500).css({'display' :'block'});
  		},
  		function () {
				$('#alogo').stop(true, true).animate({top : '0',opacity:'0'}, 500).css({'display' :'none'});
 		 }
		);
		
	//click function to change collection		
	$('#alogo li').click(function() {
 
		var logonum= $(this).index();	
			$("#alogo li").show().removeClass("actLogo");
			$(this).css({'display' :'none'}).addClass("actLogo")
			$('#alogo').stop(true, true).animate({right : '0',opacity:'0'}, 500).css({'display' :'none'})
		
			$("#actLogo").children('ul').children('li').removeClass("actLogo").fadeOut(500);
			$("#actLogo").children('ul').children('li').eq(logonum).addClass("actLogo").fadeIn(500)
			
			function reSetCollection(){
				$("#allcollection").children("div").children(".viewC").css({"display":""})
				$("#allcollection").children("div").children("ul").children("li").removeClass("act")
				$("#allcollection").children("div").children("ul").children("li:first-child").addClass("act")
				}
			
			$("#allcollection").children('div').removeClass("actLogo").fadeOut(500, reSetCollection());
			$("#allcollection").children('div').eq(logonum).addClass("actLogo").fadeIn(500);
			window.location.hash = $("#actLogo").children('ul').children('li').eq(logonum).children().attr('class')			
		
	});
		
}
 


//Collection Slider
jQuery.fn.abslider = function () {
	
	
	var activeClass ='act'
	var speed=500
	var maId=$(this);
	var myparent=$(maId).parent();

	 $("<div class='prevBtn'><img alt='Previous' src='img/prev-button.png'></div>").insertBefore($(maId))
	 $("<div class='nextBtn'><img alt='Next' src='img/next-button-G.png'></div>").insertAfter($(maId))//.append("")
	 $(maId).children("li").hide();
	 $(maId).children("li:first-child").show().addClass(activeClass)//.css({"background-color":"#000"});
	 //$(maId).clone().appendTo($(maId).closest(myparent).children('.thumbnails')).removeAttr('id')//.addClass("moveme")
	 var lastli= $(this).closest(myparent).children(maId).children('li:last-child').index();
	 $(maId).closest(myparent).children(".thumbnails").children("ul").css({width:180*(lastli+1)})
	 $(maId).closest(myparent).children(".thumbnails").children("ul").children('li:first-child').addClass(activeClass)

	 $(".thumbnails li").css({'display' : ''});
	// alert( $(maId).parent().children(".thumbnails").children("ul").children('li').children("div").children("img:first").attr("id"));
	 $(maId).closest(myparent).children(".icons").attr({id: $(maId).parent().children(".thumbnails").children("ul").children('li').children("div").children("img:first").attr("id")});
	//$(maId).closest(myparent).children(".icons").attr({id: $(maId).children("li:first-child").children().children("img").attr("id")});
	
	 ////Added by Anurag//////
	 $(maId).closest(myparent).children(".icons").find(".likeme").children("p").next(".likecount").remove();
		$("<div class=\"likecount\" style=\"width:130px;\"> Be the first to like</div>").insertAfter($(maId).closest(myparent).children(".icons").find(".likeme").children("p"));

	 function iconReset(){
		$(maId).closest(myparent).children(".icons").children('div').children(".likeimg, .comment, .download, .share").stop().animate({left:'0px'}, 200);
		$(maId).closest(myparent).children(".icons").children(".likeme, .comBox, .dwnBox, .shareBox").stop().animate({right:'0', opacity:0}, 200,'easeInOutExpo').css({"display":"none"}); 
	 ///added by anurag
	 $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("input[type=text],textarea").removeClass("bdrRed");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtComment']").next(".row , erRed").remove();
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtComment']").val("Enter your comment");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtName']").val("Name");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtEmail']").val("Email");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtMobile']").val("Mobile");
            
	 }
	 
	 function nextImg(){
		  
		 $(maId).closest(myparent).children(".viewC").fadeOut(speed);
		 if($(maId).closest(myparent).children(maId).children('li.act').is(":last-child")){
			$(maId).closest(myparent).children(maId).children('li:last-child').removeClass(activeClass).fadeOut(speed);
			$(maId).closest(myparent).children(maId).children('li:first-child').addClass(activeClass).fadeIn(speed);
			
			$(maId).closest(myparent).children(".thumbnails").children("ul").children('li:last-child').removeClass(activeClass)
			$(maId).closest(myparent).children(".thumbnails").children("ul").children("li:first-child").addClass(activeClass)
                
			$(maId).closest(myparent).children(".icons").attr({id: $(maId).parent().children(".thumbnails").children("ul").children("li.act").children().children("img").attr("id")});
			}
		else{
			
			$(maId).closest(myparent).children(maId).children('li.act').removeClass(activeClass).fadeOut(speed)
			.next().addClass(activeClass).fadeIn(speed);
				
			$(maId).closest(myparent).children(".thumbnails").children("ul").children("li.act").removeClass(activeClass)
			.next().addClass(activeClass)

			$(maId).closest(myparent).children(".icons").attr({id: $(maId).parent().children(".thumbnails").children("ul").children("li.act").children().children("img").attr("id")});
			
			}
		iconReset();
		imgMove();
		 }
		 
		function prevImg(){ 
			
			$(maId).closest(myparent).children(".viewC").fadeOut(speed);
			if($(maId).closest(myparent).children(maId).children('li.act').is(':first-child'))
			{
				$(maId).closest(myparent).children(maId).children('li:first-child').removeClass(activeClass).fadeOut(speed);
				$(maId).closest(myparent).children(maId).children('li:last-child').addClass(activeClass).fadeIn(speed);
				
				$(maId).closest(myparent).children(".thumbnails").children("ul").children('li:first-child').removeClass(activeClass)
				$(maId).closest(myparent).children(".thumbnails").children("ul").children("li:last-child").addClass(activeClass)
 
				$(maId).closest(myparent).children(".icons").attr({id: $(maId).parent().children(".thumbnails").children("ul").children("li.act").children().children("img").attr("id")});
			}
			else{
				$(maId).closest(myparent).children(maId).children('li.act').removeClass(activeClass).fadeOut(speed)
				.prev().addClass(activeClass).fadeIn(speed);
				
				$(maId).closest(myparent).children(".thumbnails").children("ul").children("li.act").removeClass(activeClass)
				.prev().addClass(activeClass)
				
				$(maId).closest(myparent).children(".icons").attr({id: $(maId).parent().children(".thumbnails").children("ul").children("li.act").children().children("img").attr("id")});				
				}
		iconReset();
		imgMove();
		}
	
	// click function for next button
	$('.nextBtn').click(function() {
			nextImg();
	});
	
	// click function for previous button
	$('.prevBtn').click(function() {
			prevImg()
			
	});
	
	// KEYBOARD CONTROLS
		$(document).keydown(function(e){
		
			var keyCode = e.keyCode || e.which,
		    	arrow = {sleft:37, sright:39};

		  	switch (keyCode) {
		    	case arrow.sleft:
		      		prevImg();
		    	break;
			    case arrow.sright:
					nextImg();
			    break;
		  	};
	
		});
	
	
	//click function for view collection image
	$(maId).click(function() {
		$(maId).closest(myparent).children(".galleryswitch").children(".gallery").stop().animate({left:'1px'}, 200);
		$(maId).closest(myparent).children(".thumbnails").stop().animate({height:'0px'}, 1000,'easeInOutExpo').css({"overflow":"hidden"});
		$(maId).closest(myparent).children(".icons").stop().animate({bottom:'72px'}, 1000,'easeInOutExpo');
		$(maId).closest(myparent).children(".galleryswitch").stop().animate({bottom:'42px'}, 1000,'easeInOutExpo');
		
		nextImg();
		})
	
	//click function for thumbnail hide on click outside
	$(maId).closest(myparent).children('.viewC').children().children(".redBtn").click(function() {
			$(this).closest(myparent).children(".viewC").fadeOut(speed);
			imgMove();
			
		})
	
	//click function for thumbnail
	$('.thumbnails li').click(function(e) { 
			$(this).closest(myparent).children(".viewC").fadeOut(speed);
			if($(this).hasClass('act')){e.preventDefault();}
			$(this).closest(myparent).children(".thumbnails").children("ul").children('li').removeClass(activeClass);
			$(this).closest(myparent).children(maId).children("li").removeClass(activeClass).fadeOut(speed);;
			$(this).addClass(activeClass);
			var abnum= $(this).index();
			$(this).closest(myparent).children(maId).children("li").eq(abnum).addClass(activeClass).fadeIn(speed);
			//$(this).children().children().attr("id")
			
			$(this).closest(myparent).children(".icons").attr({id: $(this).children().children("img").attr("id")});
			imgMove();
			
			iconReset();
		})
		

		
	//thumnails move function	
	$('.thumbnails').mousemove(function(e){
				if($(this).width() < $('.thumbnails ul').width()) {
				var distance = e.pageX - $(this).offset().left;
				var percentage = distance / $(this).width();
				var targetX = -Math.round(($('.thumbnails ul').width() - $(this).width()) * percentage);
			
				if(e.pageX < 157){
					
					$(this).closest(myparent).children(".thumbnails").children("ul").stop().animate({left:'0px'}, 5000, 'easeOutCirc');
				} else {
					
					$(this).closest(myparent).children(".thumbnails").children("ul").stop().animate({left:targetX+'px'}, 5000, 'easeOutCirc');
				};
			};
		
		});
		
	
		//full image top bottom
	function imgMove(){
		
		$(maId).closest(myparent).mousemove(function(e){
				if($(this).height() < $(maId).children("li.act").children("div").children(".zoom").height()) {
				var distance = e.pageY - $(this).offset().top;
				var percentage = distance / $(this).height();
				var targetY = -Math.round(($(maId).children("li.act").children("div").children(".zoom").height() - $(this).height()) * percentage);
				$(maId).children("li.act").children("div").children(".zoom").stop().animate({top:'0px'}, 5000, 'easeOutCirc');
				if(e.pageY < 300){
					
					$(maId).children("li.act").children("div").children(".zoom").stop().animate({top:'0px'}, 5000, 'easeOutCirc');
				} else {
					
					$(maId.children("li.act").children("div").children(".zoom")).stop().animate({top:targetY+'px'}, 5000, 'easeOutCirc');
				};
			};
		
		});
		}
		
		
	/*******************************************************************
	* Gallery Button									               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	$('.gallery').toggle(function(even, odd){
		$(this).closest(myparent).children(".galleryswitch").children(".gallery").stop().animate({left:'-31px'}, 200);
		$(this).closest(myparent).children(".thumbnails").stop().animate({height:'138px'}, 1000,'easeInOutExpo');
		$(this).closest(myparent).children(".icons").stop().animate({bottom:'213px'}, 1000,'easeInOutExpo');
		$(this).closest(myparent).children(".galleryswitch").stop().animate({bottom:'183px'}, 1000,'easeInOutExpo');	
		iconReset();	
	}, function(){
		$(this).closest(myparent).children(".galleryswitch").children(".gallery").stop().animate({left:'1px'}, 200);
		$(this).closest(myparent).children(".thumbnails").stop().animate({height:'0px'}, 1000,'easeInOutExpo').css({"overflow":"hidden"});
		$(this).closest(myparent).children(".icons").stop().animate({bottom:'72px'}, 1000,'easeInOutExpo');
		$(this).closest(myparent).children(".galleryswitch").stop().animate({bottom:'42px'}, 1000,'easeInOutExpo');
		iconReset();
	});	
	
	
	/*******************************************************************
	* download button									               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	$('.download').click(function(){
		$(this).closest(myparent).children('.icons').children('div').children('.download').animate({left:'-32px'}, 200);
		$(this).parent().next().stop().animate({right:'31px', opacity:1}, 200,'easeInOutExpo').css({"display":"block"});
		$(this).parent().parent().children('div').children(".likeimg, .comment, .share").stop().animate({left:'0px'}, 200);
		$(this).parent().parent().children(".likeme, .comBox, .shareBox").stop().animate({right:'0', opacity:0}, 200,'easeInOutExpo').css({"display":"none"});
	
	 ///added by anurag
	 $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("input[type=text],textarea").removeClass("bdrRed");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtComment']").next(".row , erRed").remove();
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtComment']").val("Please enter your comment");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtName']").val("Please enter your name");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtEmail']").val("Please enter your e-mail");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtMobile']").val("Please enter you mobile no.");
	
	
	});

	
	
	
	/*******************************************************************
	* like button										               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	$('.likeimg').click(function(){
		$(this).closest(myparent).children('.icons').children('div').children('.likeimg').stop().animate({left:'-32px'}, 200);
		$(this).parent().next().stop().animate({right:'31px', opacity:1}, 200,'easeInOutExpo').css({"display":"block"});;
		$(this).parent().parent().children('div').children(".download, .comment, .share").stop().animate({left:'0px'}, 200);
		$(this).parent().parent().children(".dwnBox, .comBox, .shareBox").stop().animate({right:'0', opacity:0}, 200,'easeInOutExpo').css({"display":"none"});

  ///added by anurag
	 InputBoxReset();
	});
	
	//like functinality
	$('.likeFun').click(function(){
		$(this).closest(myparent).children('.icons').children('.likeme').children('.likeFun').children('.like').fadeOut()
		$(this).closest(myparent).children('.icons').children('.likeme').children('.likeFun').children('.ty').fadeIn()

  ///added by anurag
	 InputBoxReset();

	});
	/*******************************************************************
	* comment button									               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	$('.comment').click(function(){
		$(this).closest(myparent).children('.icons').children('div').children('.comment').stop().animate({left:'-32px'}, 200);
		$(this).parent().next().stop().animate({right:'31px', opacity:1}, 200,'easeInOutExpo').css({"display":"block"});;
		$(this).parent().parent().children('div').children(".likeimg, .download, .share").stop().animate({left:'0px'}, 200);
		$(this).parent().parent().children(".likeme, .dwnBox, .shareBox").stop().animate({right:'0', opacity:0}, 200,'easeInOutExpo').css({"display":"none"});
 
 
  ///added by anurag
	 InputBoxReset();
	});
	
	
	/*******************************************************************
	* share button										               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	$('.share').click(function(){
		$(this).closest(myparent).children('.icons').children('div').children('.share').stop().animate({left:'-32px'}, 200);
		$(this).parent().next().stop().animate({right:'31px', opacity:1}, 200,'easeInOutExpo').css({"display":"block"});;
		$(this).parent().parent().children('div').children(".likeimg, .comment, .download").stop().animate({left:'0px'}, 200);
		$(this).parent().parent().children(".likeme, .comBox, .dwnBox").stop().animate({right:'0', opacity:0}, 200,'easeInOutExpo').css({"display":"none"});
	
	
	 ///added by anurag
	 InputBoxReset();
	 
	});
	
	/////added by anurag
	InputBoxReset=function(){
	
	$(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("input[type=text],textarea").removeClass("bdrRed");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtComment']").next(".row , erRed").remove();
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtComment']").val("Please enter your comment");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtName']").val("Please enter your name");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtEmail']").val("Please enter your e-mail");
	    $(maId).closest(myparent).children(".icons").children(".comBox, .dwnBox").find("$input[name='txtMobile']").val("Please enter you mobile no.");
	
	}
	/*******************************************************************
	* close button for share, like, comment and download				*
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	$('.close').click(function(){
		$(this).parent().prev().children().stop().animate({left:'0px'}, 200);
		$(this).parent().animate({right:'0', opacity:0}, 200,'easeInOutExpo').css({"display":"none"});
	});
	
	/*******************************************************************
	* Next previous button hover						               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	//Added By Anurag
	
	////////////////// Post Comment,Like and Share Event Start //////////////////////////////
$(this).parent().children(".icons").find(".postbtn").click(function(){
	  var $imageId=$(this).parent().parent().parent().attr("id");
	   if($imageId==undefined)
	   $imageId=$(this).parent().parent().parent().parent().attr("id");
	  var $comment=$(this).parent().parent().find("$input[name='txtComment']");
	  var $name=$(this).parent().parent().find("$input[name='txtName']");
	  var $email=$(this).parent().parent().find("$input[name='txtEmail']");
	  var $mobile=$(this).parent().parent().find("$input[name='txtMobile']");
	  
	  if($comment.val()=="" || $comment.val()=="Please enter your comment")
	    {
	      //alert("Enter Your Comment");
	      $comment.next(".row , erRed").remove();
	      $comment.after("<section class=\"row erRed\" >Please enter your comment</section>");
	      $comment.addClass("bdrRed");
	      $comment.focus();
	      return false;
	    }
	    else{
	    $comment.next(".row , erRed").remove();
	    $comment.removeClass("bdrRed");
	    }
	    
	  if($name.val()=="" || $name.val()=="Please enter your name")
	    {
	     // alert("Enter Your Name");
	       $comment.next(".row , erRed").remove();
	       $comment.after("<section class=\"row erRed\" >Please enter your name</section>");
	       $name.addClass("bdrRed");
	       $name.focus();
	       return false;
	    }
	    else {
	            $name.removeClass("bdrRed");
	            $comment.next(".row , erRed").remove();
	          }
	    
	     if($email.val()=="" || $email.val()=="Please enter your e-mail")
	    {
	      //alert("Enter Your Email");
	       $comment.next(".row , erRed").remove();
	       $comment.after("<section class=\"row erRed\" >Please enter your e-mail</section>");
	       $email.addClass("bdrRed");
	       $email.focus();
	        return false;
	    }
	    else{
	         $email.removeClass("bdrRed");
	         $comment.next(".row , erRed").remove();
	    }
	    
	    if (validateEmail($email.val())==false) {
	            //alert('Invalid Email Address');
	            $comment.next(".row , erRed").remove();
	            $comment.after("<section class=\"row erRed\" >Please enter a valid e-mail address</section>");
	            $email.addClass("bdrRed");
	            $email.focus();
	        return false;
	        }
	        else{
	        
	              $email.removeClass("bdrRed");
	              $comment.next(".row , erRed").remove();
	          }
	      
	  if($mobile.val()=="" || $mobile.val()=="Please enter you mobile no.")
	    {
	         //alert("Enter Your Mobile Number");
	            $comment.next(".row , erRed").remove();
	            $comment.after("<section class=\"row erRed\" >Please enter you mobile no.</section>");
	            $mobile.addClass("bdrRed");
	            $mobile.focus();
	        return false;
	    }
	    else{
	    
	    $mobile.removeClass("bdrRed");
	     $comment.next(".row , erRed").remove();
	    }
	    
	    var regEx = new RegExp("/[0-9]/");
          if ($mobile.val().length != 10 && !$mobile.val().match(regEx)) {
          
           //alert(" Invalid Mobile Number");
               $comment.next(".row , erRed").remove();
	            $comment.after("<section class=\"row erRed\" >Please enter a valid mobile no. (10 digits)</section>");
                $mobile.addClass("bdrRed");
	           $mobile.focus();
	        return false;
          }
          else
          {
           $mobile.removeClass("bdrRed");
           $comment.next(".row , erRed").remove();
          var $paId=$(this);
          
            $.get("postcomment.aspx", { categoryAndMasterId: $imageId,user_email:$email.val(),user_mobile:$mobile.val(),user_name:$name.val(),user_comment:$comment.val()},function(data){
           if (data != "") {
              if(data=="success"){
               $comment.next(".row , erRed").remove();
	            $comment.after("<section class=\"row erRed\" >Thank You for your comment</section>");
              $mobile.val('Mobile');$email.val('Email');$name.val('Name'); $comment.val('Enter your comment');
               //alert("thanks for your comment");
         
               if($paId.parent().parent().parent().attr("class")=="icons"){
                setTimeout(function() {
               $paId.parent().parent().parent().children(".comBox").stop().animate({right:'0', opacity:0},200,'easeInOutExpo').css("display","none");
               } , 1000);
               }
                else{ 
                setTimeout(function() {
                 $paId.parent().parent().parent(".comBox").stop().animate({right:'0', opacity:0}, 200,'easeInOutExpo').css("display","none");
                $comment.next(".row , erRed").delay(1000).remove();
                 } , 1000);
                 }
              }
           }
           
           });
          
          }
	    
	});


	//$(this).parent().children(".icons").find(".postbtn").click(function(){var a=$(this).parent().parent().parent().attr("id");if(a==undefined)a=$(this).parent().parent().parent().parent().attr("id");var b=$(this).parent().parent().find("$input[name='txtComment']");var c=$(this).parent().parent().find("$input[name='txtName']");var d=$(this).parent().parent().find("$input[name='txtEmail']");var e=$(this).parent().parent().find("$input[name='txtMobile']");if(b.val()==""||b.val()=="Enter your comment"){b.next(".row , erRed").remove();b.after('<section class="row erRed" >Enter your comment.</section>');b.addClass("bdrRed");b.focus();return false}else{b.next(".row , erRed").remove();b.removeClass("bdrRed")}if(c.val()==""||c.val()=="Name"){b.next(".row , erRed").remove();b.after('<section class="row erRed" >Enter your Name.</section>');c.addClass("bdrRed");c.focus();return false}else{c.removeClass("bdrRed");b.next(".row , erRed").remove()}if(d.val()==""||d.val()=="Email"){b.next(".row , erRed").remove();b.after('<section class="row erRed" >Enter your Email.</section>');d.addClass("bdrRed");d.focus();return false}else{d.removeClass("bdrRed");b.next(".row , erRed").remove()}if(validateEmail(d.val())==false){b.next(".row , erRed").remove();b.after('<section class="row erRed" >Enter Valid Email.</section>');d.addClass("bdrRed");d.focus();return false}else{d.removeClass("bdrRed");b.next(".row , erRed").remove()}if(e.val()==""||e.val()=="Mobile"){b.next(".row , erRed").remove();b.after('<section class="row erRed" >Enter Your Mobile.</section>');e.addClass("bdrRed");e.focus();return false}else{e.removeClass("bdrRed");b.next(".row , erRed").remove()}var f=new RegExp("/[0-9]/");if(e.val().length!=10&&!e.val().match(f)){b.next(".row , erRed").remove();b.after('<section class="row erRed" >Invalid Mobile No.</section>');e.addClass("bdrRed");e.focus();return false}else{e.removeClass("bdrRed");b.next(".row , erRed").remove();var g=$(this);$.get("postcomment.aspx",{categoryAndMasterId:a,user_email:d.val(),user_mobile:e.val(),user_name:c.val(),user_comment:b.val()},function(a){if(a!=""){if(a=="success"){alert("thanks for your comment");if(g.parent().parent().parent().attr("class")=="icons")g.parent().parent().parent().children(".comBox").stop().animate({right:"0",opacity:0},200,"easeInOutExpo").css({display:"none"});else g.parent().parent().parent(".comBox").stop().animate({right:"0",opacity:0},200,"easeInOutExpo").css({display:"none"})}}})}})
			
 //  $(this).parent().children(".icons").find(".likeme").find(".like").click(function(e){alert("liked");var a=$(this).parent().parent().attr("id"); if(a==undefined)a=$(this).parent().parent().parent().attr("id"); var b=$(this);$.get("like.aspx",{categoryAndMasterId:a},function(a){if(a!=""){if(a=="success"){alert("Thanks for your like");if(b.parent().parent().attr("class")=="likeme")b.parent().parent().children(".likeme").stop().animate({right:"0",opacity:0},200,"easeInOutExpo").css({display:"none"});else b.parent().parent().children(".likeme").stop().animate({right:"0",opacity:0},200,"easeInOutExpo").css({display:"none"})}}})})

////////////
$(this).parent().children(".icons").find(".likeme").find(".like").click(function(e)
    {
   
    var a=$(this).parent().parent().parent().attr("id");
     if(a==undefined)a=$(this).parent().parent().parent().parent().attr("id");
      var b=$(this); 
         
      $.get("like.aspx",{categoryAndMasterId:a},function(data)
      {
      if(data!="")
      {
      if(data=="success")
      {
            
           $.get("GetJqueryData.aspx", { ImageId: a},function(data){
           if (data != "") {
           
                var cnt=parseInt(data);
              
              if(b.parent().parent().attr("class")=="likeme"){
                 var $this=b.parent().parent(".likeme");
             
                 if(cnt==0)
                  $this.find(".likecount").text("Be the first to like");
                 else
                 $this.find(".likecount").text(data+" people like this");
                 $this.find(".likecount").css({"display":"","width":"130px","height":"15px","padding-left":"20px","cursor":"default"});
                }
                else{
                  var $this=b.parent().parent().children(".likeme");
              
                  if(cnt==0)
                  $this.find(".likecount").text("Be the first to like");
                 else
                    $this.find(".likecount").text(data+" people like this");
                    $this.find(".likecount").css({"display":"","width":"130px","height":"15px","padding-left":"20px","cursor":"default"})
                }
             
           }
           
           }); 
            // b.parent().after("i like this");
          
      }
    }
    
    });
    
    });


////////

$(this).parent().children(".icons").find(".likeimg").click(function(a){var b=$(this).parent().parent().attr("id");if(b==undefined)b=$(this).parent().parent().parent().attr("id");var c=$(this);$(this).parent().parent().children().children(".likeFun").children("img:first").css("display","block");$(this).parent().parent().children().children(".likeFun").children("img:last").css("display","none");$.get("GetJqueryData.aspx",{ImageId:b},function(a){if(a!=""){var b=parseInt(a);if(c.parent().parent().attr("class")=="likeme"){var d=c.parent().parent().children(".likeme");if(b==0)d.find(".likecount").text("Be the first to like.");else d.find(".likecount").text(a+" people like this");d.find(".likecount").css({display:"",width:"130px",height:"15px","padding-left":"20px",cursor:"default"})}else{var d=c.parent().parent().children(".likeme");if(b==0)d.find(".likecount").text("Be the first to like");else d.find(".likecount").text(a+" people like this");d.find(".likecount").css({display:"",width:"130px",height:"15px","padding-left":"20px",cursor:"default"})}}})})
		
	////////////share event///////////////
	
	$(this).parent().children(".icons").find(".share").click(function(){ 
	var collectionid=$(this).parent().attr("id");if(collectionid==undefined)collectionid=$(this).parent().parent().attr("id");
	var $htag=$(this).parent().parent().children(".shareBox").children("a:first");
		if($htag==undefined)$htag=$(this).parent().parent().parent().children(".shareBox").children("a:first");
	$.get('GetJqueryData.aspx',{fbCollectionId:collectionid}, function(results){ 
	            if(results!="" && results!="#"){$htag.attr("target","_blank");$htag.attr("href",results); return false;}
      }).error(function() {
      });
	  });////outer tag
	  
	//$(this).parent().children(".icons").find(".shareBox a:eq(0)").click(function(){var a=$(this).parent().parent().attr("id");if(a==undefined)a=$(this).parent().parent().parent().attr("id");$.get("GetJqueryData.aspx",{fbCollectionId:a},function(a){if(a!=""&&a!="#"){ $(this).target = "_blank";$(this).attr("href",a);window.open(a); return false;  }else{$(this).removeAttr("traget");}}).error(function(){})})
	/////////////////share event end//////////////
	
	$(this).parent().children(".icons").find(".shareBox a:eq(2)").click(function(){
	var url=$(this).attr("href");
	var ImageId=$(this).parent().parent().attr("id");if(ImageId==undefined)ImageId=$(this).parent().parent().parent().attr("id");
	var ImId=ImageId.split("|")[1];
	var thumImage=$(this).parent().parent().parent().children(".thumbnails").find("[id$="+ImId+"]").attr("src");
	thumImage=thumImage.substring(thumImage.indexOf("Thumbnail") + 10, thumImage.length);
	url = url.replace(url.substring(url.indexOf("Thumbnail") + 10, url.indexOf("&description")), thumImage);
	$(this).attr("href",url);
	
	})
	////////////////// Post Comment,Like and Share Event End //////////////////////////////
	
	///////////////Form validation section////////////////////////////////////////////
	validateEmail=function(email){
	 var filter =/^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(email))
     {
        return true;
     }
    else 
          {
             return false;
          }
	}
	isNumberKey=function(evt) { var charCode = (evt.which) ? evt.which : evt.keyCode; if ((charCode > 47 && charCode < 58) || charCode == 8 || charCode == 9) return true;  else  return false;
}
	//////////////Form validation section end//////////////////////////////////////////
	
	
	
	
	
	
	
	$(".prevBtn img").hover(
		  function () {
			$(this).stop().animate({left : '-30px'}, 100);
  		  },
		  function () {
			$(this).stop().animate({left : '0px'}, 100);
  		}
	);
	
	$(".nextBtn img").hover(
		  function () {
			$(this).stop().animate({left : '0'}, 100);
  		  },
		  function () {
			$(this).stop().animate({left : '-30px'}, 100);
  		}
	);
	
	/*******************************************************************
	* All tooltips 										               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/
	$(maId).closest(myparent).children(".icons").children("div").children(".download").hover(
  		function () {
				$(maId).closest(myparent).children(".icons").children(".dt").stop(true, false).fadeIn()
  		},
  		function () {
				$(maId).closest(myparent).children(".icons").children(".dt").stop(true, false).fadeOut()
 		 }
		);
		
	$(maId).closest(myparent).children(".icons").children("div").children(".likeimg").hover(
  		function () {
				$(maId).closest(myparent).children(".icons").children(".lt").stop(true, false).fadeIn()
  		},
  		function () {
				$(maId).closest(myparent).children(".icons").children(".lt").stop(true, false).fadeOut()
 		 }
		);
	$(maId).closest(myparent).children(".icons").children("div").children(".comment").hover(
  		function () {
				$(maId).closest(myparent).children(".icons").children(".ct").stop(true, false).fadeIn()
  		},
  		function () {
				$(maId).closest(myparent).children(".icons").children(".ct").stop(true, false).fadeOut()
 		 }
		);
		
	$(maId).closest(myparent).children(".icons").children("div").children(".share").hover(
  		function () {
				$(maId).closest(myparent).children(".icons").children(".st").stop(true, false).fadeIn()
  		},
  		function () {
				$(maId).closest(myparent).children(".icons").children(".st").stop(true, false).fadeOut()
 		 }
		);
		
	$(maId).closest(myparent).children(".galleryswitch").children(".gallery").hover(
  		function () {
				$(maId).closest(myparent).children(".icons").children(".gt").stop(true, false).fadeIn()
  		},
  		function () {
				$(maId).closest(myparent).children(".icons").children(".gt").stop(true, false).fadeOut()
 		 }
		);
	return false;	
}

/*******************************************************************
* Index autoslider									               *
*                                                                  *
*                                                                  *
*******************************************************************/
jQuery.fn.abauto = function () {
	var activeClass ='act'
	var speed=500
	var maId=$(this);
	$(maId).wrap("<div class='fullcontrol'></div>")

	var myparent=$(maId).parent();
	$("<div class='sliderNav'></div>").insertBefore($(maId))
	$(maId).clone().appendTo($(maId).closest(myparent).children('.sliderNav')).removeAttr('id')//.addClass("moveme")
	$(".sliderNav ul li").empty().html("nav").children("li:first-child").addClass(activeClass)
	$(maId).closest(myparent).children(".sliderNav").children("ul").children("li:first-child").addClass(activeClass)
	
	$(maId).children("li").hide()
	$(maId).children("li:first-child").show().addClass(activeClass)
	
	function runme(){
			if($(maId).closest(myparent).children(maId).children('li.act').is(':last-child'))
			{
				$(maId).closest(myparent).children(maId).children('li:last-child').removeClass(activeClass).fadeOut(speed);
				$(maId).closest(myparent).children(maId).children('li:first-child').addClass(activeClass).fadeIn(speed);
				
				$(maId).closest(myparent).children(".sliderNav").children("ul").children('li:last-child').removeClass(activeClass)
				$(maId).closest(myparent).children(".sliderNav").children("ul").children("li:first-child").addClass(activeClass)
			}
			else{
				$(maId).closest(myparent).children(maId).children('li.act').removeClass(activeClass).fadeOut(speed)
				.next().addClass(activeClass).fadeIn(speed);
				
				$(maId).closest(myparent).children(".sliderNav").children("ul").children("li.act").removeClass(activeClass)
				.next().addClass(activeClass)
				}
		
		}
		
	//calling function for continues changes of slides
	setInterval(function() {
	   runme();
	  }, 5000);
	
	//click function for dots
	$('.sliderNav li').click(function() {
			
			if($(this).hasClass('act')){e.preventDefault();}
			$(this).closest(myparent).children(".sliderNav").children("ul").children('li').removeClass(activeClass);
			$(this).closest(myparent).children(maId).children("li").removeClass(activeClass).fadeOut(speed);;
			$(this).addClass(activeClass);
			var abnum= $(this).index();
			$(this).closest(myparent).children(maId).children("li").eq(abnum).addClass(activeClass).fadeIn(speed);
		})
	

		
	}


/*******************************************************************
* Tab plugIn										               *
*                                                                  *
*                                                                  *
*******************************************************************/

jQuery.fn.abTab = function (e) {
	var activeClass ='acTab'
	var noClick ='noClick'
	var speed=500
	var tId=$(this);
	$(tId).children().children(".TabC, .itC").hide()
	$(tId).children().children(".TabC, .itC").eq(0).show()
	$(tId).children().children("li").eq(0).addClass(activeClass)
	//$(maId).wrap("<div class='fullcontrol'></div>")
	
	$(tId).children().children("li").click(function() {
		if($(this).hasClass(activeClass)){e.preventDefault();}
		if($(this).hasClass(noClick)){e.preventDefault();}
		$(this).closest(tId).children(".Trow").children().removeClass(activeClass)
		$(this).addClass(activeClass)
		var mynum = $(this).index()
		//$("div").hide(speed);
		$(".TabContent").children().hide(speed)
  		$(".TabContent").children().eq(mynum).show(speed)
	});
}



/*******************************************************************
* Runway Gallery									               *
*                                                                  *
*                                                                  *
*******************************************************************/
//Collection Slider
//Collection Slider
jQuery.fn.runway = function () {
	
	var activeClass ='act'
	var speed=500
	var maId=$(this);
	var myparent=$(maId).parent();
	 $(maId).children("li:first-child").show().addClass(activeClass)
	 var lastli= $(this).children('li:last-child').index();
	 $(maId).closest(myparent).css({"overflow":"hidden", "position":"relative", "height":166})
	 $(maId).css({width:210*(lastli+1)})
	 
	 
	 //thumnails move function	
	$(maId).closest(myparent).mousemove(function(e){
				if($(this).width() < $(maId).width()) {
				var distance = e.pageX - $(this).offset().left;
				var percentage = distance / $(this).width();
				var targetX = -Math.round(($(maId).width() - $(this).width()) * percentage);
			
				if(e.pageX < 157){
					
					$(maId).stop().animate({left:'0px'}, 10000, 'easeOutCirc');
				} else {
					
					$(maId).stop().animate({left:targetX+'px'}, 10000, 'easeOutCirc');
				};
			};
		
		});


}
////////////////////////////////////////////subscribe event////////////////////////////////////////////
function SubscribeMe(divId){

var a=$("#"+divId).children("#insubscribecont").children("#txtEmail");
  var b=$("#"+divId).children("#insubscribecont").children("#txtMobile");
 var imagetext=$("#"+divId).children("#inCpatchaDiv").find("#txtCaptcha");
    if(a.val()=="" || a.val()=="Enter your e-mail")
    {
       $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
       $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
       $("#"+divId).children().children(".erRed").css("display","");
       $("#"+divId).children().children(".erRed").text("Please enter your e-mail address");
        a.addClass("bdrRed");
        a.focus();
        return false;
    }  
    else{
     
	              a.removeClass("bdrRed");
	              $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
    }
      if (validateEmail(a.val())==false) {
	              $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
	              $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
	              $("#"+divId).children().children(".erRed").css("display","");
                  $("#"+divId).children().children(".erRed").text("Please enter a valid e-mail address");
	            a.addClass("bdrRed");
	            a.focus();
	        return false;
	        }
	        else{
     
	              a.removeClass("bdrRed");
	               $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
	             
    }
     if(b.val()=="" || b.val()=="Enter your mobile no.")
    {
                  $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
                  $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
                  $("#"+divId).children().children(".erRed").css("display","");
                  $("#"+divId).children().children(".erRed").text("Please enter your mobile no.");
                  b.addClass("bdrRed");
	             b.focus();
     return false;
    }
    else{
	    
	     b.removeClass("bdrRed");
	     $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove()
	   }
	    
          if ((b.val().length != 10) ) {
          
           
             $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
             $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
             $("#"+divId).children().children(".erRed").css("display","");
             $("#"+divId).children().children(".erRed").text("Please enter a valid mobile no. (10 digits)");
             b.addClass("bdrRed");
	           b.focus();
	        return false;
          }
          else{
	    
	     b.removeClass("bdrRed");
	     $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
	     $("#"+divId).children("#inCpatchaDiv").css("display","block");
	      $("#"+divId).children("#insubscribecont").css("display","none");
	   }
                 if(imagetext.val()=="" || imagetext.val()=="Enter the text to proceed")
    {
      
      if($("#wifaCptch").length!=0)
      {
      $("#"+divId).find("#wifaCptch").next(".row , .erRed").remove();
       $("#"+divId).find("#wifaCptch").after("<span class=\"erRed\" style=\"display:none\"></span>")
       $("#"+divId).find(".erRed").css("display","");
       $("#"+divId).find(".erRed").html("<br />Please enter captcha text");
      }
      else{
       $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
       $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
       $("#"+divId).find(".erRed").css("display","");
       $("#"+divId).find(".erRed").html("<br />Please enter captcha text");
      
       }
        imagetext.addClass("bdrRed");
       
        imagetext.focus();
        return false;
    } 
          else
          {
          $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").next(".row , .erRed").remove()
            $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").hide();
            $MId=$("#"+divId).children("#inCpatchaDiv").children(".redBtnInp");
          $.ajax({
             type: "GET",
             url: "Jquerypages/exclusive.aspx",
             data: "semail=" + $("#"+divId).children("#insubscribecont").children("#txtEmail").val() + "&smobile=" + $("#"+divId).children("#insubscribecont").children("#txtMobile").val()+"&txtCaptcha="+imagetext.val(),
             cache: false,
             success: function(msg) {
                 if (msg == "ok") {
                
                      b.removeClass("bdrRed");
                     $MId.parent().parent().parent("#border2").removeAttr("id");
                     if(divId=="subscribe"){ 
                      $("#"+divId).children("#insubscribehid").css("display","block")
                    $("#"+divId).children("#insubscribecont").css("display","none")
                    $("#"+divId).children("#inCpatchaDiv").css("display","none")
                      }
                      else{
                     $("#"+divId).children("#subscribehid").css("display","block")
                    $("#"+divId).children("#subscribecont").css("display","none")
                    $("#"+divId).children("#inCpatchaDiv").css("display","none")
                    
                     
                     }
                     b.prev(".row , erRed").remove();
                 }
                 else {
                     
               
               $("#"+divId).find("#wifaCptch").next(".row , .erRed").remove();
              $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").next(".row , .erRed").remove();
             $("#"+divId).find("#wifaCptch").after("<span class=\"erRed\" style=\"display:none\"></span>")
             $("#"+divId).find(".erRed").css("display","");
             $("#"+divId).find(".erRed").html("<br />"+msg);
              $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").show();
            // $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
            // $("#"+divId).children("#inCpatchaDiv").children(".erRed").css("display","");
             // $("#"+divId).children("#inCpatchaDiv").children(".erRed").html("<br />"+msg);
            
            
                 }
             }
         });
         
           }

}

////////////////////////////////////js code for collection data///////////////

////////////////////////////////////////////////subscrbie me for index page///////////////////////


function SubscribeMe1(divId){

var a=$("#"+divId).children("#insubscribecont").children("#txtEmail");
  var b=$("#"+divId).children("#insubscribecont").children("#txtMobile");
 var imagetext=$("#"+divId).children("#inCpatchaDiv").find("#txtCaptcha");
    if(a.val()=="" || a.val()=="Enter your e-mail")
    {
       //$("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
      // $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
       //$("#"+divId).children().children(".erRed").css("display","");
       //$("#"+divId).children().children(".erRed").text("Please enter your e-mail address");
        //a.addClass("bdrRed");
        appriseServer("Please enter your e-mail address");
        a.focus();
        return false;
    }  
    else{
     
	              a.removeClass("bdrRed");
	              $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
    }
      if (validateEmail(a.val())==false) {
	              //$("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
	             // $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
	             // $("#"+divId).children().children(".erRed").css("display","");
                 // $("#"+divId).children().children(".erRed").text("Please enter a valid e-mail address");
	           
	           // a.addClass("bdrRed");
	           
	             appriseServer("Please enter a valid e-mail address");
	            a.focus();
	        return false;
	        }
	        else{
     
	              a.removeClass("bdrRed");
	               $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
	             
    }
     if(b.val()=="" || b.val()=="Enter your mobile no.")
    {
                 // $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
                 // $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
                 // $("#"+divId).children().children(".erRed").css("display","");
                  //$("#"+divId).children().children(".erRed").text("Please enter your mobile no.");
                 // b.addClass("bdrRed");
                 appriseServer("Please enter your mobile no.");
	             b.focus();
     return false;
    }
    else{
	    
	     b.removeClass("bdrRed");
	     $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove()
	   }
	    
          if ((b.val().length != 10) ) {
          
           
//             $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
//             $("#"+divId).children().children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
//             $("#"+divId).children().children(".erRed").css("display","");
//             $("#"+divId).children().children(".erRed").text("Please enter a valid mobile no. (10 digits)");
//             b.addClass("bdrRed");
appriseServer("Please enter a valid mobile no. (10 digits)");

	           b.focus();
	        return false;
          }
          else{
	    
	     b.removeClass("bdrRed");
	     $("#"+divId).children().children(".redBtnInp").next(".row , .erRed").remove();
	     $("#"+divId).children("#inCpatchaDiv").css("display","block");
	      $("#"+divId).children("#insubscribecont").css("display","none");
	   }
        if(imagetext.val()=="" || imagetext.val()=="Enter the text to proceed")
         {
//       $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
//       $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
//       $("#"+divId).find(".erRed").css("display","");
//       $("#"+divId).find(".erRed").html("<br />Please enter captcha text");
     
appriseServer("Please enter captcha text");
        imagetext.addClass("bdrRed");
       
        imagetext.focus();
        return false;
    } 
          else
          {
          $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").next(".row , .erRed").remove()
            $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").hide();
            $MId=$("#"+divId).children("#inCpatchaDiv").children(".redBtnInp");
          $.ajax({
             type: "GET",
             url: "Jquerypages/exclusive.aspx",
             data: "semail=" + $("#"+divId).children("#insubscribecont").children("#txtEmail").val() + "&smobile=" + $("#"+divId).children("#insubscribecont").children("#txtMobile").val()+"&txtCaptcha="+imagetext.val(),
             cache: false,
             success: function(msg) {
                 if (msg == "ok") {
                
                      b.removeClass("bdrRed");
                     $MId.parent().parent().parent("#border2").removeAttr("id");
                     if(divId=="subscribe"){ 
                      $("#"+divId).children("#insubscribehid").css("display","block")
                    $("#"+divId).children("#insubscribecont").css("display","none")
                    $("#"+divId).children("#inCpatchaDiv").css("display","none")
                      }
                      else{
                     $("#"+divId).children("#subscribehid").css("display","block")
                    $("#"+divId).children("#subscribecont").css("display","none")
                    $("#"+divId).children("#inCpatchaDiv").css("display","none")
                    // $MId.parent().children("div.hidden-box").css("display","block").stop().animate({left: 0}, 200);
                     
                     }
                     b.prev(".row , erRed").remove();
                 }
                 else {
                     //alert("somthing went wrong.");
                        $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").next(".row , .erRed").remove();
             $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
             $("#"+divId).children("#inCpatchaDiv").children(".erRed").css("display","");
             $("#"+divId).children("#inCpatchaDiv").children(".erRed").html("<br />"+msg);
              $("#"+divId).children("#inCpatchaDiv").children(".redBtnInp").show();
                 }
             }
         });
         
           }

}



////////////////////////////////////////////subcribe me end for index/////////////////////////////////////////////////////
function collectiondata(collectionname){
   
  $.ajax({
             type: "GET",
             url: "Jquerypages/CollectionData.aspx",
             data: "collectionName=" + collectionname ,
             cache: false,
             success: function(data) {
                 if (data != "") {
             
                  $("#abslider").html("");
                 $("#abslider").html(data);
                  
                 }
                 
             }
         });
         
         $.ajax({
             type: "GET",
             url: "Jquerypages/CollectionThumbData.aspx",
             data: "collectionName=" + collectionname ,
             cache: false,
             success: function(data) {
                 if (data != "") {
                            
                  $(".thumbnails ul").html("");
                    $(".thumbnails ul").html(data);
                 }
                 
             }
         });
}

//////////////////////////////validation for customer care form//////////////////////////////////

function check() {
            $(".pb2prc").children(".erRed").css("display", "block");
        if (Trim(document.getElementById("Name").value) == "") {
            $(".pb2prc").children(".erRed").text("Please enter your name");
            $("#Name").addClass("bdrRed");
            document.getElementById("Name").focus();
            
            return false;
        }
        else {
            $("#Name").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
        }
        if (Trim(document.getElementById("email").value) == "") {
            $(".pb2prc").children(".erRed").text("please enter your e-mail address");
            $("#email").addClass("bdrRed");
            document.getElementById("email").focus();
            return false;
        }
        else {
            $("#email").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
        }
        if (!echeck(document.getElementById("email").value)) {
            $(".pb2prc").children(".erRed").text("Please enter a valid e-mail address");
            $("#email").addClass("bdrRed");
            return false;
        }
        else {
            $("#email").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
        }
        if (Trim(document.getElementById("mobile").value) == "") {
            $(".pb2prc").children(".erRed").text("Please enter your mobile no.");
            $("#mobile").addClass("bdrRed");
            document.getElementById("mobile").focus();
            return false;
        }
        else {

            $("#mobile").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
        
            var phone = document.getElementById("mobile").value;
            if (phone.length < 10) {
                $(".pb2prc").children(".erRed").text("Please enter a valid mobile no.  (10 digits)");
                $("#mobile").addClass("bdrRed");
                return false;
            }
            else {
                $("#mobile").removeClass("bdrRed");
                $(".pb2prc").children(".erRed").text("");
            }
           
        }
        if (document.getElementById("feedbacktype").value == "0") {
            $(".pb2prc").children(".erRed").text("Please select the nature of feedback");
            $("#feedbacktype").addClass("bdrRed");
            document.getElementById("feedbacktype").focus();
            return false;
        }
        else {
            $("#feedbacktype").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
        }
        if (Trim(document.getElementById("address").value) == "") {
            $(".pb2prc").children(".erRed").text("Please enter your address");
            $("#address").addClass("bdrRed");
            document.getElementById("address").focus();
            return false;
        }
        else {
            $("#address").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
        }
        if (Trim(document.getElementById("feedback").value) == "") {
            $(".pb2prc").children(".erRed").text("Please write your feedback within 2000 characters'");
            $("#Enter your feedback'").addClass("bdrRed");
            document.getElementById("feedback").focus();
            return false;
        }
        else{
        $("#feedback").removeClass("bdrRed");
        $(".pb2prc").children(".erRed").text("");
        }
        if (Trim(document.getElementById("contactCaptcha").value) == "" || Trim(document.getElementById("contactCaptcha").value) == "Enter the number displayed on left to proceed") {
            $(".pb2prc").children(".erRed").text("Please enter captcha text");
            $("#contactCaptcha").addClass("bdrRed");
            $("#contactCaptcha").focus();
            return false;
        }
        else
        {
            $("#contactCaptcha").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
         var name=Trim(document.getElementById("Name").value);
        var  email=Trim(document.getElementById("email").value);
        var  mobile=Trim(document.getElementById("mobile").value);
         var feedbacktype=Trim(document.getElementById("feedbacktype").value);
         var feedback=Trim(document.getElementById("feedback").value);
         var address=Trim(document.getElementById("address").value);
         var cpatchvalue=Trim(document.getElementById("contactCaptcha").value);
          $.ajax({
             type: "GET",
             url: "Jquerypages/postcaredata.aspx",
             data: "name=" + name+"&email="+email+"&feedbacktype="+feedbacktype+"&feedback="+feedback+"&address="+address+"&mobile=" +mobile+"&sCaptchText="+cpatchvalue,
             cache: false,
             success: function(data) {
                 if (data != "" && data=="Thankyou for your feedback.") {
                  $("#divsuccess").css("display","block");  
                   $("#divcontent").css("display","none");  
                    $(".pb2prc").children(".erRed").text("");       
                 // alert(data);
                 // window.location.href="index.aspx";
                 }
                 else
                 {
                  $(".pb2prc").children(".erRed").text(data);
                  $("#contactCaptcha").addClass("bdrRed");
                 }
                 
             },error:function(error){$(".pb2prc").children(".erRed").text(error);$("#contactCaptcha").removeClass("bdrRed"); }
         });
        
        }
      
    }
    
    /////////////////////////////////////////////////////////////////////////////js code to submit trade enquiries//////////////////////////////
    
    
    //////////////////////////////validation for customer care form//////////////////////////////////

function tradesubmit() {
    if (Trim(document.getElementById("feedbackNature").value) == "0") {
        $(".pb2prc").children(".erRed").text("Please select the nature of your enquiry").css("display", "block");
        //alert('Select nature of feedback.');
        document.getElementById("feedbackNature").focus();
        $("#feedbackNature").addClass("bdrRed");
        return false;
    }
    else {
        $("#feedbackNature").removeClass("bdrRed");
        $(".pb2prc").children(".erRed").text("");
        }

       if (Trim(document.getElementById("txtName").value) == "") {
           $(".pb2prc").children(".erRed").text("Please enter your name");
           $("#txtName").addClass("bdrRed");
            document.getElementById("txtName").focus();
            return false;
        }
        else {
            $("#txtName").removeClass("bdrRed");
            $(".pb2prc").children(".erRed").text("");
        }
        
        if (Trim(document.getElementById("txtContact").value) == "") {
            $(".pb2prc").children(".erRed").text("Please enter your mobile no.");
            $("#txtContact").addClass("bdrRed");
            document.getElementById("txtContact").focus();
            return false;
        }
        else {
            var phone = document.getElementById("txtContact").value;
            if (phone.length < 8) {
                $(".pb2prc").children(".erRed").text("Please enter a valid mobile no. (10 digits)");
                $("#txtContact").addClass("bdrRed");
                //alert('Invalid contact no.')
                return false;
            }
            else {
                $("#txtContact").removeClass("bdrRed");
                $(".pb2prc").children(".erRed").text("");
            }
        }
    

        if (Trim(document.getElementById("txtEmail").value) == "") {
            $(".pb2prc").children(".erRed").text("Please enter your e-mail address");
            $("#txtEmail").addClass("bdrRed");
            document.getElementById("txtEmail").focus();
            return false;
        }
        else {
        $("#txtEmail").removeClass("bdrRed");
        $(".pb2prc").children(".erRed").text("");
        }
    if (!echeck(document.getElementById("txtEmail").value)) {

        $(".pb2prc").children(".erRed").text("Please enter a valid e-mail address");
        $("#txtEmail").addClass("bdrRed");
            return false;
        } 
        else{
        $("#txtEmail").removeClass("bdrRed");
        $(".pb2prc").children(".erRed").text("");
        }
        if (Trim(document.getElementById("contactCaptcha").value) == "" || Trim(document.getElementById("contactCaptcha").value) == "Enter the number displayed on left to proceed") {
            $(".pb2prc").children(".erRed").text("Please enter captcha text");
            $("#contactCaptcha").addClass("bdrRed");
            $("#contactCaptcha").focus();
            return false;
        }               
        else
        {
          $("#contactCaptcha").removeClass("bdrRed");
        $(".pb2prc").children(".erRed").text("");
         var name=Trim(document.getElementById("txtName").value);
        var  email=Trim(document.getElementById("txtEmail").value);
        var  phone=Trim(document.getElementById("txtContact").value);
         var feedbacktype=Trim(document.getElementById("feedbackNature").value);
         var comments=Trim(document.getElementById("txtComments").value);
         var file=Trim(document.getElementById("fp").value);
         var captchavalue=Trim(document.getElementById("contactCaptcha").value);
         var qString={name:name,email:email,feedbacktype:feedbacktype,comments:comments,phone:phone,sCaptchText:captchavalue};
          
          $.ajaxFileUpload
            ({
                 url: 'Jquerypages/FileUploader.ashx',
                 secureuri: false,
                 fileElementId: 'fp',
                dataType: 'json',
                data:{name:name,email:email,feedbacktype:feedbacktype,comments:comments,phone:phone,sCaptchText:captchavalue},
               success: function (data, status) {
               
               if (typeof (data.error) != 'undefined') {
               
                if (data.msg=="Ok") {
                
                   $("#tradedivsuccess").css("display","block");  
                   $("#tradedivcont").css("display","none");  
                 } 
                 else {
                       $(".pb2prc").children(".erRed").css("display","block");
                       $(".pb2prc").children(".erRed").text(data.msg);
                        $("#contactCaptcha").addClass("bdrRed");
                       $("#contactCaptcha").focus();        
                      }
                  }
              },
                 error: function (data, status, e) {
                //alert(e);
                
                  $("#tradedivsuccess").css("display","block");  
                   $("#tradedivcont").css("display","none");  
           }
         });

        }
        }
      

    function fnloadmap(val) {
        document.getElementById('storeLeft').innerHTML = "<iframe src='" + val + "' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' style='height:400px; width:290px;border:0px;'></iframe>";
    }
    
   ////////////////////////////splash event start here/////////////////////////
   
   
   function SplashMe(divId){
   
var a=$("#"+divId).find("#stxtEmail");
  var b=$("#"+divId).find("#stxtMobile");
 var imagetext=$("#"+divId).find("#sCaptchText");
    if(a.val()=="" || a.val()=="Enter your e-mail")
    {
       $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
       $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
       $("#"+divId).find(".erRed").css("display","");
       $("#"+divId).find(".erRed").html("<br /><br />Please enter your e-mail address");
        a.addClass("bdrRed");
        a.focus();
        return false;
    }  
    else{
     
	              a.removeClass("bdrRed");
	              $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
    }
      if (validateEmail(a.val())==false) {
	              $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
	              $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
	              $("#"+divId).find(".erRed").css("display","");
                  $("#"+divId).find(".erRed").html("<br /><br />Please enter a valid e-mail address");
	            a.addClass("bdrRed");
	            a.focus();
	        return false;
	        }
	        else{
     
	              a.removeClass("bdrRed");
	               $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
	             
    }
     if(b.val()=="" || b.val()=="Enter your mobile no.")
    {
                  $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
                  $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
                  $("#"+divId).find(".erRed").css("display","");
                  $("#"+divId).find(".erRed").html("<br /><br />Please enter your mobile no.");
                  b.addClass("bdrRed");
	             b.focus();
     return false;
    }
    else{
	    
	     b.removeClass("bdrRed");
	     $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove()
	   }
	    
          if ((b.val().length != 10) ) {
          
           
             $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
             $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
             $("#"+divId).find(".erRed").css("display","");
             $("#"+divId).find(".erRed").html("<br /><br />Please enter a valid mobile no. (10 digits)");
             b.addClass("bdrRed");
	           b.focus();
	        return false;
          }
           if(imagetext.val()=="" || imagetext.val()=="Enter the text to proceed")
    {
       $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
       $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
       $("#"+divId).find(".erRed").css("display","");
       $("#"+divId).find(".erRed").html("<br /><br />Please enter captcha text");
        imagetext.addClass("bdrRed");
        imagetext.focus();
        return false;
    }  
          else
          {
          $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove()
            $("#"+divId).find(".redBtnInp").hide();
            $MId=$("#"+divId).find(".redBtnInp");
          $.ajax({
             type: "GET",
             url: "Jquerypages/SplashPopup.aspx",
             data: "semail=" + $("#stxtEmail").val() + "&smobile=" + $("#stxtMobile").val()+"&sCaptchText="+$("#sCaptchText").val(),
             cache: false,
             success: function(msg) {
                 if (msg == "ok") {
                
                      b.removeClass("bdrRed");
                 //    $MId.parent().parent().parent("#border2").removeAttr("id");
                    // $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
                   // $("#"+divId).find(".erRed").css("display","");
                  //  $("#"+divId).find(".erRed").html("<br /><br />Thank You for subscribing to our exclusive offers.");
                 $("#"+divId).children("#splashdivsuccess").css("display","block");
                 $("#"+divId).children("#splashdiv").css("display","none");
                     b.prev(".row , erRed").remove();
                    
                  window.setTimeout(function(){$("#sc").fadeOut(200);
	          	$("#navDarkbg").fadeOut(1000)},2000);   
                 }
                 else {
                  $("#"+divId).find(".redBtnInp").show();
                 //$("#"+divId).find(".erRed").html("<br /><br />somthing went wrong.");
              $("#"+divId).find(".redBtnInp").next(".row , .erRed").remove();
             $("#"+divId).find(".redBtnInp").after("<span class=\"erRed\" style=\"display:none\"></span>")
             $("#"+divId).find(".erRed").css("display","");
             $("#"+divId).find(".erRed").html("<br />"+msg);
                    // alert("somthing went wrong.");
                 }
             }
         });
         
           }

}
   
   
   /////////////////////////splash event end here////////////////////////
   
   /*$(document).ready(function(){
    $.ajax({
             type: "GET",
             url: "Jquerypages/twittFeed.aspx",
             cache: false,
             success: function(data) {
             
              $("#tweetBox").html("")  ;   
                 $("#tweetBox").html(data)  ;              
             }
         });
   
   
   });*/