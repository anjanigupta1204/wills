﻿/*
Copyright © Wills Lifestyle
Desigend & HTML5/CSS3/jQuery By Abhay Mandal (Sr. Creative Designer - Sirez Infosystems)
Powered by Sirez Infosystems
*/

/*******************************************************************
* Support for IE									               *
*                                                                  *
*                                                                  *
*******************************************************************/
document.createElement("footer");  
document.createElement("article");  
document.createElement("header");  
document.createElement("aside");
document.createElement("section");
document.createElement("hgroup");  
document.createElement("nav");  


/*******************************************************************
* page load									               		   *
*                                                                  *
*                                                                  *
*******************************************************************/

$(window).load(function() {
	/*$("#navDarkbg").css({'z-index' : '7', 'margin-top' : '0'}).fadeIn(1000);
	$("#sc").delay(400).fadeIn(1000);*/
	
	 $("#load").fadeOut(2000, function(){ $(this).remove();})
	 //$("#content, #f,").fadeIn(2000)
	  $('body').css({"overflow-x":'hidden',"overflow-y":'hidden'})
	//	 $(".wall").css({'margin-top':'-1%'})
	 //reSize();
	//.delay(1000).remove();
	


});



$(document).ready(function() {
	

	
	 //$("<div id='load'></div>").insertAfter($("#f"))
	
	/*******************************************************************
	* lightbox											               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/	
	$('a#wifwV').facebox({
					loadingImage : 'lightbox/loading.gif',
					closeImage   : 'lightbox/closelabel.png'
					
	})
	
	
	
	
	function reSize(){
		
		var winW=$(window).width();
		var winH=$(window).height();

			
				var mainwdth = (winW)-80;
				//alert (mainwdth)
				$('#Hmncntr').width(mainwdth).css({'margin-left':'60px'});
				
				
				
				// Applying height to the columns
				var remainH= (winH);
				var remainHs= (winH -130)*20/100;
				
			// Applying height to the columns
				var remainH= (winH);
				var remainHs= (winH -130)*20/100;
				var remainHNew= Math.round((winH)*87/100);
				var remainHfetr= Math.round((remainHNew)*62/100);
				var remainHLk= Math.round((remainHNew)*38/100)-4;
				var remainHsub= Math.round((winH - remainHNew)-2);
				var remainWGal = Math.round ((winW -80)*63/100);
				var remainWGalHm = Math.round ((winW -80));
				var remainWbnr = Math.round ((winW -80)*37/100);
				var remainWbnrs1 = (remainWbnr -8)*1/2;
				var remainWsubs = (remainWbnr)-4;
				var remainWsubs = (remainWbnr)-2;
				var remaimWshop = Math.round((remainHNew)*38/100);
				//var remainHLk1= Math.round((remainHNew)*62/100-2);
			//	var remainWleftcol = (remainWGal);
				//var remainWrightcol =  (mainwdth - remainWleftcol)-2;
			//	var remainWinnLt =Math.round((mainwdth)*67/100)-20;
			//	var remainWinnRt =Math.round((mainwdth)*33/100);
				
				
					//$("#content").height(winH -95)
				$('.shopOnBnr').height(remainHNew);
				$('.banner1').width(remainWbnrs1).css({'margin-left':'4px'}).height(remainHNew);
				$('.fetrcol').height(remainHfetr);
				$('.lookbkBnr').height(remainHLk).css({'margin-top':'4px'});
				//$('.storeBnr').height(remainHLk1).css({'margin-top':'2px'});
				$('.colsubs').height(remainHsub).width(remainWsubs).css({'margin-left':'4px'});
				$(".colNew").height(remainHNew);
				$(".gallerySec").height(remainH).width(remainWGalHm);
				$(".rightBnr").height(remainH).width(remainWbnr);
				$(".col").height(remainHs);
				$(".colbig").height(remainH);
				$(".col2").height(remainHs);
				$(".col3").height(remainH/2.1);
				$("#allcollection,.signatureMC, .classicMC, .sportMC, .clublifeMC, .accMC, .pcMC, .wall, .pg_container, #map_canvas, #store_rght").height(winH-100);
				$(".store_rght_inner ul").height(remainH*47/100)
				//$("#abslider").height(1290);

				//body height
				$("body").height(winH);
				
				//subscribe box height and width
				$("#border").width($(".col2").width()-2).height($(".col2").height()-4);
				
				
				//gap beetween WIFW video and Designer lookbook
				$("#wifwV").css({'margin-bottom' : remainH-remainH/2.1*2});
				
				var mypad = (winW/100)
				
				//internal pages left column height
				$("#inLeft").height(winH);
				
				$(".cRight,.abmain,#map_canvas,#store_rght").height(remainH);
				
				//internal pages righ column height
				$("#inRight").height(winH);
				
				//internal pages left column height
				$("#gmap").height(winH-100);
				
				//club itc pages left column height
				$(".pannel").height(winH-410);
				
				//FAQ's pages left column height
				$(".faqPan").height(winH*.19);
				
				
				
				//calculating Hight of mid column
				var midH = Math.round(winH- (mypad*0.5 + 110))
				
				//calculating Padding between 6 right boxs
				
				var rightPad = Math.round($("#inRight").width()*0.04)
				
				var finalH = (midH - rightPad)/3
				//calculating and applying height of 6 boxes right
				$(".irLeft, .irRight").animate({height:finalH}, 100)
				
				//subscribe box height and width
				$("#border2").width($(".irRight").width()-2)//.height(($(".irRight").height())-4);
				//$("#border2").animate({height:(($(".irRight").height())-4), width:$(".irRight").width()-2})
				//$("#border2").html($(".irRight").height())
				
				//store page scrolling content Height
				var storeH = $("#inLeft").height();
				$(".TabC").height(storeH*0.52)
				$(".itC").height(storeH*0.60)
				$(".aboutPage").height(storeH*0.80)
				
				//Runway page height
				$(".rLR").height(($("#inLeft").height())/2.4)
				//$(".tlShadow").css({"line-height":($("#inLeft").height())/2.4+'px'})
				$(".abmain2").height(($("#inLeft").height())*0.75)
				

				// checking if the resolution is smaller then 1024x600 amd applying font size
				if(winW>1800){
						$('body').addClass("s1800");
						$('body').removeClass("s1400");
						$('body').removeClass("s1200"); 
						$('body').removeClass("s768");
				}				
				if(winW>1400 && parseInt(winW)<1800){
						$('body').removeClass("s1800");
						$('body').removeClass("s1400");
						$('body').removeClass("s1200"); 
						$('body').removeClass("s768");
				}
				if((winW<1400 && parseInt(winW)>1200) || (parseInt(winH)<650)){
						$('body').addClass("s1400"); 
						$('body').removeClass("s1200"); 
						$('body').removeClass("s1800");
						$('body').removeClass("s768");
				}
				if((winW<1200 && parseInt(winW)>999) || (parseInt(winH)<630)){
						$('body').addClass("s1200"); 
						$('body').removeClass("s1400"); 
						$('body').removeClass("s1800");
						$('body').removeClass("s768");
				}
				if(winW<768){
						$('body').addClass("s768");
						$('body').removeClass("s1800");
						$('body').removeClass("s1400");
						$('body').removeClass("s1200");
						$(".cRight").css('height','auto'); 
						$('body').css('overflow-y','auto');
						
				}
				/*******************************************************************
				* Applying Height to SWF							               *
				*                                                                  *
				*                                                                  *
				*******************************************************************/
				var remainSWFh= $("#inLeft").height() -65
				$("#FlashID, .lookbook").attr({height:remainSWFh});
				$("#FlashID2").attr({height:remainSWFh});
		}



/*******************************************************************
* page load, splash screen							               *
*                                                                  *
*                                                                  *
*******************************************************************/
$(".closeSQ").click(function() {
	$("#sc").fadeOut(200);
  	$("#navDarkbg").fadeOut(1000)
});

$(document).keyup(function(e) {
  if (e.keyCode == 27) { 
  		$("#sc").fadeOut(200);
  		$("#navDarkbg").fadeOut(1000)
   }   // esc
});



/*******************************************************************
* red button hover effect							               *
*                                                                  *
*                                                                  *
*******************************************************************/
 $(".redBtnN img").hover(
  function () {
	$(this).stop().animate({left : '-30px'}, 200);
  },
  function () {
	$(this).stop().animate({left : '0'}, 200);
  }
);

 $(".redBtnInp img").hover(
  function () {
	$(this).stop().animate({left : '-30px'}, 200);
  },
  function () {
	$(this).stop().animate({left : '0'}, 200);
  }
);

	
/*******************************************************************
* Navigation Hover function							               *
*                                                                  *
*                                                                  *
*******************************************************************/
 $("#collection").hover(
  function () {
	$(this).addClass("act");  
	$("#f").css({'z-index' : '6'});
    $("#navDarkbg").stop(true, true).fadeIn(100).css({'z-index' : '6', 'margin-top' : '59px'});
	$("#subNav").stop(true, true).fadeIn(100);
  },
  function () {
    $(this).removeClass("act");  
    $("#navDarkbg").stop(true, true).fadeOut(100);
	$("#subNav").stop(true, true).fadeOut(100);	
  }
);

/*******************************************************************
* Contact Hover function							               *
*                                                                  *
*                                                                  *
*******************************************************************/
 $("#cMenu").hover(
  function () {
	$(this).addClass("act");  
	$("#f").css({'z-index' : '6'});
    $("#navDarkbg").stop(true, true).fadeIn(100).css({'z-index' : '6', 'margin-top' : '59px'});
	$("#cSubMenu").stop(true, true).fadeIn(100);
  },
  function () {
    $(this).removeClass("act");  
    $("#navDarkbg").stop(true, true).fadeOut(100);
	$("#cSubMenu").stop(true, true).fadeOut(100);	
  }
);

/*******************************************************************
* Footer Hover function								               *
*                                                                  *
*                                                                  *
*******************************************************************/
 $("#f").hover(
  function () {
	$("#fNav").stop(true, true).animate({height: "180px"}, 700,'easeOutExpo' ).css({'z-index' : '8'}).addClass("dgry");
	$("#f").stop(true, true).animate({height: "180px"}, 700,'easeOutExpo' ).css({'z-index' : '8'});
	$("ul#fNav>li ul").stop(true, true).fadeIn()
	$("#itcLogo").stop(true, true).fadeIn()
    $("#navDarkbg").stop(true, true).fadeIn(400).css({'z-index' : '7', 'margin-top' : '0px'});
  },
  function () {
    $("#fNav").stop(true, true).animate({height: "16px"}, 700,'easeInOutExpo' ).removeClass("dgry");
    $("#f").stop(true, true).animate({height: "16px"}, 700,'easeInOutExpo' );	
	$("ul#fNav>li ul").stop(true, true).fadeOut()
	$("#itcLogo").stop(true, true).fadeOut(400)
    $("#navDarkbg").stop(true, true).fadeOut(400);
  }
);

/*******************************************************************
* Faq page drop down change							               *
*                                                                  *
*                                                                  *
*******************************************************************/
 $("#faqs").change(function()
 {
   $("#" + this.value).show().siblings().hide();
 }); 

$("#faqs").change();

/*******************************************************************
* Calling all function for Ajax Load				               *
*                                                                  *
*                                                                  *
*******************************************************************/

function allFunction(){
	 $("#faqs").change(function()
	 {
	   $("#" + this.value).show().siblings().hide();
	 }); 

	$("#faqs").change();


	
	
//Main Nav Hover function 

$('.maimNAv ul li a span').fadeOut(1);

$('.maimNAv ul li a' ).hover(function(){
$(this).children('span').stop().fadeIn(800);
},function(){
$(this).children('span').stop().fadeOut(200);
})

//Footer Hover function 
$('footer').hover(function(){
	$(this).stop().animate({right:"0px"});
	$('.ftr').stop().fadeOut(200);	
	
},function(){
	
		$(this).stop().animate({right:"-148px"})
		$('.ftr').stop().fadeIn(2500);

	})


 
	//var hash=window.location.hash.substring(1,window.location.hash.length);
		//if(hash=="signature" ||hash=="classic" || hash=="personal" || hash=="personalcare" || hash=="sport" || hash=="clublife"|| hash=="accessories")
		//collectiondata(hash);
	 //added by anurag function to load gallery data from server 
/*		
	window.setTimeout(slideractivate, 2000);	

	window.setTimeout(loadslider, 2000 );
	function loadslider(){

	}
*/	

		//Runway Gallery function reloaded
	//$('.Run_gal').runway();
	//$('.Run_gal2').runway();
		reSize();
	//$('#abslider7').runway();
	$('.itC, .TabC, .myScroll, .pannel, .faqPan').each(
					function()
					{
						
						$(this).jScrollPane(
							{
								showArrows: $(this).is('.arrow'),
								autoReinitialise: true
							}
						);
						var api = $(this).data('jsp');
						var throttleTimeout;
						$(window).bind(
							'resize',
							function()
							{
								if ($.browser.msie) {
									// IE fires multiple resize events while you are dragging the browser window which
									// causes it to crash if you try to update the scrollpane on every one. So we need
									// to throttle it to fire a maximum of once every 50 milliseconds...
									if (!throttleTimeout) {
										throttleTimeout = setTimeout(
											function()
											{
												api.reinitialise();
												throttleTimeout = null;
											},
											50
										);
									}
								} else {
									api.reinitialise();
								}
							}
						);
					}
				)
	

	/*******************************************************************
	* What's new Slider									               *
	*                                                                  *
	*                                                                  *
	*******************************************************************/	
	//$('#whatsnew').abauto()
		

	}

function AjaxLoadFix(){
	
	allFunction();
	
	$('a[rel*=facebox]').facebox({
					loadingImage : 'lightbox/loading.gif',
					closeImage   : 'lightbox/closelabel.png'
				  })

	
	}
	AjaxLoadFix()
//allFunction()
//$('#whatsnew').abauto()



// if the user resizing window
$(window).resize(function () {
	reSize();
	
	
});


/*******************************************************************
* Daynamic Page Load								               *
*                                                                  *
*                                                                  *
*******************************************************************/
$('#alogo li a, .ajAxLoad, .rNavN a, #sitemap a, #navigation li a, #fNav>li>a').click(function(){
		//$('#fix1000').fadeOut('fast');
	})

		/////////////added by anurag stroe change event
 changeevent=function(event){if(event.value=="0"){ alert("select city"); return false; }
   $.ajax({
             type: "GET",
             url: "Jquerypages/storedata.aspx",
             data: "cityname=" + event.value ,
             cache: false,
             success: function(data) {
                 if (data != "") {
                   $(".Trow li").each(function(){$(this).removeClass("acTab")});
                   $(".Trow li:first").addClass("acTab");
                  $(".TabContent").html("");
                 $(".TabContent").html(data);
                    AjaxLoadFix();
                 }
                 
             }
         });
  
}


	//store locator click function

//	$("#staticMap li a").click(function(e){
//	 	e.preventDefault();
//		$('#staticMap li a').removeClass('act')
//		$(this).addClass('act')
//		$('#staticMap li span').css({'display':'none'})
//	 	$(this).parent().children('span').css({'display':'block'})
//	 })
	 
//	 $("#staticMap li span").click(function(e){
//	 	e.preventDefault();
//		$('#staticMap li a').removeClass('act')
//	 	$(this).css({'display':'none'})
//	 })


// end of main function
});

///////////////////////////

