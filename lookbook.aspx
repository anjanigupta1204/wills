


<!DOCTYPE html>

<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head id="Head1">
<title>Wills Lifestyle</title>
<meta name="Description" content="India�s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/slider.css" rel="stylesheet" type="text/css" />

<link href="lightbox/facebox.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/fakeLoader.css">
</head>
    <body class="mobilebg">
    <form name="form1" method="post" action="lookbook.aspx" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJLTczOTI2NTcyZGSwHopCLE54ec/K2M7pNKfEr7ZJsA==" />
</div>


  
<div class="fakeloader"></div>
<div class="commonnav">
<!-- Paste this code after body tag -->
<!--	<div class="se-pre-con"></div> -->
	<!-- Ends -->
<div class="left-nav" id="leftnav">
  <div class="wills_logo"><a href="index.aspx"><img src="img/wills_lifestyle.jpg" class="icon-big"> <img src="img/wills_logo.gif" class="icon-small"></a></div>
  <nav class="hideonmobile">
    <ul>

      <li><a href="lookbook.aspx" data-toggle="tooltip" title="Earth Collection" data-placement="right"><span>Earth Collection</span></a></li>
      <li><a href="http://www.shopwillslifestyle.com" target="_blank" data-toggle="tooltip" title="Shop Online" data-placement="right"><span>Shop Online</span></a></li>
      <li><a href="store.aspx" data-toggle="tooltip" title="Store Locator" data-placement="right"><span><img src="img/store-locator-Nw-icon.png" /></span></a></li>
      <li><a href="gifting.aspx" data-toggle="tooltip" title="Store Gifting" data-placement="right"><span>Gifting</span></a></li>
      <li><a href="club-itc.aspx" data-toggle="tooltip" title="Club ITC" data-placement="right"><span>Club ITC</span></a></li>
      <li><a href="video-gallery.aspx" data-toggle="tooltip" title="Video Gallery" data-placement="right"> <span>Video Gallery</span></a></li>
      <li><a href="contactus.aspx" data-toggle="tooltip" title="Contact Us" data-placement="right"><span>Contact Us</span></a></li>
      
    </ul>
  </nav>
  <div class="socialdiv">
    <ul>
      <li><a href="https://www.facebook.com/willslifestyleonline" target="_blank"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>
      <li><a href="https://twitter.com/willslifestyle" target="_blank"><i class="fa fa-twitter"></i> <span>Twitter</span></a></li>
      <li><a href="http://www.pinterest.com/wlsonline/" target="_blank"><i class="fa fa-pinterest"></i> <span>Pinterest</span></a></li>
      <li><a href="http://www.youtube.com/willslifestyleonline" target="_blank"><i class="fa fa-youtube"></i> <span>Youtube</span></a></li>
      <li><a href="http://www.instagram.com/willslifestyle" target="_blank"><i class="fa fa-instagram"></i> <span>Instagram</span></a></li>
    </ul>
  </div>
  <div class="itclogo"><a href="http://www.itcportal.com/" target="_blank"><img src="img/ITCLogo.gif"></a></div>
</div>
<!-- end left nav -->


<!-- Navigation --> 
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
  <ul class="sidebar-nav">
    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
    <li> Wills Lifestyle</li>
    <li><a href="profile.aspx">About us</a></li>
    <li><a href="store.aspx">Store Locator</a></li>
    
    <li><a href="club-itc.aspx">Club ITC</a></li>
    <li><a href="gifting.aspx">Gifting</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li> Collection</li>
    
    <li><a href="personalcare.aspx"> Personal Care</a></li>
    <li><a href="lookbook.aspx"> Earth Collection</a></li>
    <li><a href="http://www.shopwillslifestyle.com" target="_blank"> Shop Online</a></li>
    <li><a href="video-gallery.aspx"> Video Gallery</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li>Customer Service</li>
    <li><a href="contactus.aspx">Contact Us</a></li>
    <li><a href="http://feedback.willslifestyle.com/default.aspx?bid=1" target="_blank">Feedback</a></li>
    <li><a href="return-policy.aspx">Return Policy</a></li>
    <li><a href="terms-use.aspx">Terms of use</a></li>
    <li><a href="http://www.itcportal.com/about-itc/policies/privacy-policy.aspx" target="_blank">Privacy Policy</a></li>
    <li><a href="FAQ.aspx">FAQs</a></li>
    <li><a href="sitemap.aspx">Sitemap</a></li>
  </ul>
</nav>

<img src="img/copyright.png" class="copyright">

</div>

<!--#########################Body Start Here#########################-->

<div class="cLeft" align="center">
  <div class="psRel">
    <div style="position:absolute; left:42px;" id="rghtThumb">
      <ul class="thumblisting">
        <li style="display: list-item"><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30"> "Living Roofs"</strong> </p>
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p> <strong class="size16 mtop30">"Leafy Glades" </strong></p>
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30">"Sunkissed Meadows
            " </strong></p>
          
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30">"Indigenous Roots
            " </strong></p>
          
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30">"Ancient Tales
            " </strong></p>
          
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30">"Whispering Waltz" </strong></p>
          
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30"> "Nomadic Paths
            " </strong></p>
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30">"Earthy Twines
            "</strong> </p>
          
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30">"Modern Tribes
            " </strong> </p>
          
        </li>
        <li><img src="collection/sport/sport.jpg" >
          <p><strong class="size16 mtop30">"Indigenous Roots
            "</strong> </p>
          
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="cRight">
  <div class="abmain">
    <ul id="cslider" class="sliderlisting">
      <li><a href="collection/lookbook/Large/17.jpg"  id="A1" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#1" ><img src="img/spacer.png" ></a><img src="collection/lookbook/17.jpg" >
        <div class="cContent cband"><strong class="size16">Living Roofs</strong><br>
         Malleable as the elements of the planet, evocative of the ways we mold it,<br> the design is a reflection of the shapes of Earth and the beauty they take on as nature leaves her mark. </div>
      </li>
      <li><a href="collection/lookbook/Large/7.jpg"  id="A2" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#2" ><img src="img/spacer.png" ></a><img src="collection/lookbook/7.jpg" >
        <div class="cContent cband"><strong class="size16">Leafy Glades </strong><br>
          As the seasons change, the falling leaves cover the land in a beautiful hue of rustling shades.  <br>  Invoking feelings of peace and respite, bringing alive memories of silent spaces and shifting days.</div>
      </li>
      <li><a href="collection/lookbook/Large/1.jpg"  id="A3" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#3" ><img src="img/spacer.png" ></a><img src="collection/lookbook/1.jpg" >
        <div class="cContent cband"><strong class="size16">Sunkissed Meadows </strong><br>
          The fields from a distance look like a handmade canvas, where nature with a fine brush has drawn in lines of gold. <br>   The breathable handwoven fabric acts as the perfect land for the meticulously planted fields of gold that adorn it.
                     </div>
      </li>
      <li><a href="collection/lookbook/Large/18.jpg"  id="A5" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#5" ><img src="img/spacer.png" ></a><img src="collection/lookbook/18.jpg" >
        <div class="cContent cband"><strong class="size16">Indigenous Roots </strong><br>
          Drawn since time immemorial, replicated in the design they inspire, the motif captures  the beauty and elegance of dancing birds <br>  At once, a homage and ode to the age-old drawings, capturing the birds as they fly across the relaxed silhouette. </div>
      </li>
      <li><a href="collection/lookbook/Large/5.jpg"  id="A6" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#6" ><img src="img/spacer.png" ></a><img src="collection/lookbook/5.jpg" >
        <div class="cContent cband"><strong class="size16">Ancient Tales </strong><br>
          Ancient Tales is a tribute to age old stories, hidden within the folds of gnarled rock. <br> At once a witness and ode to life, in its most primitive - reminiscent of the natural,  breathing fabric that carries it. 
</div>
      </li>
      <li><a href="collection/lookbook/Large/10.jpg"  id="A7" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#7" ><img src="img/spacer.png" ></a><img src="collection/lookbook/10.jpg" >
        <div class="cContent cband"><strong class="size16">Whispering Waltz</strong><br>
          Ditzy as petals dancing in the wind, this design is a tribute to the graceful waltz  between cloth <br> and wearer, as it capers, rustles and whirls with every step.</div>
      </li>
      <li><a href="collection/lookbook/Large/16.jpg"  id="A8" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#8" ><img src="img/spacer.png" ></a><img src="collection/lookbook/16.jpg" >
        <div class="cContent cband"><strong class="size16">Nomadic Paths </strong><br>
        Soft as the sand slipping through your fingers, following the paths left in the wake of drifting caverns, this shirt is inspired by the desert nomads. <br>Endless, as the routes that guide them on their path, this truly fine Linen is meant to grow. </div>
      </li>
      <li> <a href="collection/lookbook/Large/15.jpg"  id="A9" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#9" ><img src="img/spacer.png" ></a><img src="collection/lookbook/15.jpg" >
        <div class="cContent cband"><strong class="size16">Earthy Twines </strong><br>
          Inspired by the goodness, both outside and within, Earthy Twines captures  the benevolent qualities of nature. <br> The cloth, an ode and promise to the  elements it reflects. </div>
      </li>
      <li><a href="collection/lookbook/Large/4.jpg"  id="A10" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#10" ><img src="img/spacer.png" ></a><img src="collection/lookbook/4.jpg" >
        <div class="cContent cband"><strong class="size16">Modern Tribes </strong><br>
          Since time immemorial, people have come together to create tribes. Like minded.  Collaborative. Human. Creating art, together, to reflect their passion.<br>   A collective, 
at once charged and inherently artistic. Just like the linen that carries their inspiration. </div>
      </li>
      <li><a href="collection/lookbook/Large/11.jpg"  id="A12" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#12" ><img src="img/spacer.png" ></a><img src="collection/lookbook/11.jpg" >
        <div class="cContent cband"><strong class="size16"> Indigenous Roots  </strong><br>
          Drawn since time immemorial, replicated in the design they inspire, the motif captures  the beauty and elegance of dancing birds.<br> At once, a homage and ode to the age-old drawings, capturing the birds as they fly across the relaxed silhouette.
</div>
      </li>
    </ul>
    <div class="thumbnails">
      <ul>
        <li>
          <div><img src="collection/lookbook/Thumbnail/17.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/7.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/1.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/18.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/5.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/10.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/16.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/15.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/4.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/lookbook/Thumbnail/11.jpg" /></div>
        </li>
      </ul>
    </div>
    
    <div class="shopnwBtn"><a href="http://www.shopwillslifestyle.com/look-book.html" target="_blank">Shop Now<span></span></a></div>
  </div>
</div>

<!--#########################Body End Here#########################--> 
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script src="js/loader.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<script type="text/javascript" src="lightbox/facebox.js"></script> 
<script type="text/javascript" src="js/abslider_lookbook.js"></script> 
<!--Zoom start here--> 
<script type="text/javascript" src="Gallery/magiczoomplus.js"></script>
<link rel="stylesheet" href="Gallery/magiczoomplus.css" type="text/css" />
<script src="js/master.js"></script> 
<script type="text/javascript">
$(function() {
	$('#cslider').cslider();
// end of main function
});
</script>

    </form>
</body>
</html>