﻿<%@ page language="C#" autoeventwireup="true" inherits="classic, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head id="Head1">
<title>Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/slider.css" rel="stylesheet" type="text/css" />

<link href="lightbox/facebox.css" rel="stylesheet" type="text/css" />
<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/fakeLoader.css">

</head>
<body>
   <form id="form1" runat="server" style="overflow: hidden">
  <uc:navigation id="navigation1" runat="server" />

<!-- inner tab menu -->


<!-- end inner tab menu -->

<div class="cRight">
  <div class="abmain">
    <ul id="cslider">
      <li><a href="collection/classic/classicbig1.jpg"  id="A1" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#1" ><img src="img/spacer.png" ></a><img src="collection/classic/classicbig1.jpg">
        <div class="cContent cband"><strong class="size16">Zenith Of Craftsmanship</strong><br>
          Immaculately crafted for those with an eye for detail, experience top notch craftsmanship in this classic three piece suit - Dubliner </div>
      </li>
      <li><a href="collection/classic/classicbig2.jpg"  id="A2" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#2" ><img src="img/spacer.png" ></a><img src="collection/classic/classicbig2.jpg">
        <div class="cContent cband"><strong class="size16">Classic Twist</strong><br>
          Man: Mark your arrival in style in this immaculately made travel friendly suit. <br> Carefully crafted by master craftsmen, it consists of multi pockets and a light construction to provide unmatched finesse with pure ease and utility. <br> Made from high quality premium stretch fabric, it offers sheer opulence with its perfect slim fit.<br> Woman : Classic paisley printed dress by Ritu Kumar </div>
      </li>
      <li><a href="collection/classic/classicbig3.jpg"  id="A3" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom" name="#3" ><img src="img/spacer.png" ></a><img src="collection/classic/classicbig3.jpg" >
        <div class="cContent cband"><strong class="size16">Stand Out</strong><br>
          Add a stylish quotient to your wardrobe this season in this flaired trouser paired with intricate lace top,<Br>its premium cotton fabric ensures perfect lustre and color to match up with your look for the day. </div>
      </li>
    </ul>
    <div class="thumbnails">
      <ul>
        <li>
          <div><img src="collection/classic/Thumbnail/classicthumb1.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/classic/Thumbnail/classicthumb2.jpg" /></div>
        </li>
        <li>
          <div><img src="collection/classic/Thumbnail/classicthumb3.jpg" /></div>
        </li>
      </ul>
    </div>
    <div class="shopnwBtn"><a href="http://www.shopwillslifestyle.com/custom/wills-classic.html" target="_blank">Shop Now<span></span></a></div>
  </div>
  
</div>
</form>

<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
<script src="js/loader.js"></script>
<script type="text/javascript" src="js/abslider.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<script type="text/javascript" src="lightbox/facebox.js"></script> 


<!--Zoom start here--> 
<script type="text/javascript" src="Gallery/magiczoomplus.js"></script>
<link rel="stylesheet" href="Gallery/magiczoomplus.css" type="text/css" />

<script type="text/javascript" src="js/master.js"></script>
<script type="text/javascript">
$(function() {
	$('#cslider').cslider();
// end of main function
});
</script>
  
</body>
</html>