﻿<%@ page language="C#" autoeventwireup="true" inherits="AdminPanel_Header, App_Web_unlrms9o" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Admin Panel</title>
    <link href="Styles/CLStyle.css" rel="stylesheet" type="text/css" />
    <link href="Styles/hcMenuStyles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-family: "Arial" , Helvetica, sans-serif;
            font-size: 30px;
            font-weight: bold;
            color: #000000;
        }
    </style>
</head>
<body style="margin-top: 0px; margin-left: 0px">
    <form id="form1" runat="server">
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="4" align="left" height="60" valign="bottom" bgcolor="#FFFFFF" class="ar_20b">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" background="../images/bg_header.gif">
                        <tr>
                            <td align="left" valign="bottom">
                                <div align="left" style="padding-left:25px;" >
                                    <a href="main.aspx" target="_parent">
                                       <%-- <img src="images/headerlogo.png" border="0" />--%></a></div>
                            </td>
                            <td align="right" valign="bottom" style="padding: 0 20px 15px 0;">
                                <div align="right">
                                    Admin Control Panel&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="170" class="bdcom">
                    &nbsp;
                </td>
                <td width="40"  align="left" class="bdcom">
                    <img src="images/icon_acc.gif" height="27" />
                </td>
                <td  align="left" class="bdcom">
                   <span  class="welcome"> Welcome</span> &nbsp;&nbsp;                   
                        <asp:Label ID="lblUserName" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<%--<a
                        bgcolor="#D7D8D8"   href="right.htm" target="right" class="ar_14">Dashboard</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;--%>
                        <a href="logout.aspx" target="_parent" >Logout</a>
                </td>
                <td align="right" class="bdcom">
                (Note:&nbsp;<font color="#ff3333">(*)</font>&nbsp;indicate required fields)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>