﻿<%@ page language="C#" autoeventwireup="true" inherits="AdminPanel_AdminUser_Login, App_Web_unlrms9o" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title>Wills Lifestyle </title>
  <link href="Styles/CLStyle.css" rel="stylesheet" type="text/css" />
     
    <style type="text/css">
      .style1
        {
            height: 15px;            
        }
    </style>
    <script language="javascript">
        function fnreset() {
            document.form1.reset();
            return true;
        }
    </script>
</head>
<body>
    <form id="form1"  runat="server">
       <div id="centered">
<table width="554" border="0"  align="center" bgcolor="white" cellpadding="0" cellspacing="0">
<tr>
    <td width="24"><img src="Images/left_wcorner.jpg" width="24" height="17" /></td>
    <td width="506" align="left" background="Images/bg_top.jpg"><img src="Images/trans.gif" width="1" height="1" /></td>
    <td width="24" align="right"><img src="Images/right_wcorner.jpg" width="24" height="17" /></td>
  </tr>  
  <tr>
    <td colspan="3" align="center" background="Images/bg_white.jpg">
    <table width="536" border="0" cellspacing="0" bgcolor="white" cellpadding="0">       
      <tr>
        <td width="16" align="left"><img src="Images/gray_left.jpg" width="16" height="15" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="1" height="1" /></td>
        <td width="16" align="right"><img src="Images/right_gray.jpg" width="16" height="15" /></td>
      </tr>
      <tr>
        <td colspan="3" bgcolor="#313234"><img src="Images/trans.gif" width="8" height="8" /></td>
        </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="40" align="left" valign="top" bgcolor="#313234" class="ar_12bw">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="65" align="left" valign="bottom" ><%--<img src="Images/Logo.png" height="100" />--%></td>
            <td align="right" valign="bottom" class="ar_20b" style="color:White;">Control Panel Login</td>
          </tr>
        </table>
        </td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      
        <tr>
        <td align="left" bgcolor="#313234">&nbsp;</td>
        <td align="left" bgcolor="#313234"></td>
        <td align="right" bgcolor="#313234">&nbsp;</td>
      </tr>
       <tr>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td align="left" bgcolor="#909193" class="ar_12bw"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
      </tr>
       <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td  align="left" valign="top" bgcolor="#313234" class="ar_12bw"></td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="40" align="left" valign="top" bgcolor="#313234" class="ar_12bw">Enter a valid Username and Password. Then click the &quot;Sign In&quot; button to access the User Control Panel. </td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td align="left" bgcolor="#909193" class="ar_12bw"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td align="center" bgcolor="#313234" class="ar_12bw"><asp:Label ID="lblError" runat="Server" EnableViewState="False" ForeColor="Red" Font-Size="Small"></asp:Label>&nbsp;</td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td align="center" bgcolor="#313234" class="ar_12bw"><table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="78" align="left" class="ar_12bw">Username</td>
            <td width="426" align="left" class="ar_12bw"><asp:TextBox  CssClass="ar_11" 
                    ID="txtUsername" runat="Server" Width="157px"></asp:TextBox>
                                                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="(required)"
                                                                ControlToValidate="txtUsername" Font-Size="Small"></asp:RequiredFieldValidator></td>
          </tr>
          <tr>
            <td colspan="2"><img src="Images/trans.gif" width="1" height="6" /></td>
          </tr>
          <tr>
            <td align="left" class="ar_12bw">Password</td>
            <td align="left" class="ar_12bw">
            <asp:TextBox ID="txtPassword" runat="Server" Width="157px" TextMode="Password"></asp:TextBox>&nbsp;
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="(required)"
                                                                ControlToValidate="txtPassword" Font-Size="Small"></asp:RequiredFieldValidator>
            </td>
          </tr>
          <tr>
            <td colspan="2" align="left" class="ar_12bw"><img src="Images/trans.gif" width="1" height="6" /></td>
            </tr>
          <tr>
            <td align="left" class="ar_12bw">&nbsp;</td>
            <td align="left" class="ar_12bw">
                <asp:ImageButton ID="ImgButtonSignIn" 
                    runat="server" ImageUrl="Images/sign-in.gif" width="54" height="18" onclick="ImgButtonSignIn_Click" 
                    />&nbsp;&nbsp;
                    <%--<a href="#" onclick="document.form1.reset()" >
                        <img border="0" alt="reset" src="Images/reset.gif" width="54" height="18" /> </a>--%>                     
                   
                    <asp:ImageButton ID="ImgBtnReset" runat="server" 
                    ImageUrl="Images/reset.gif" width="54" height="18" 
                    OnClientClick="javascript:return fnreset();"  CausesValidation="False" 
                    onclick="ImgBtnReset_Click" /></td>
          </tr>
        </table></td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td align="left" bgcolor="#313234" class="ar_12bw">&nbsp;</td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td align="left" bgcolor="#909193" class="ar_12bw"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
      </tr>
      
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="40" align="left" valign="bottom" bgcolor="#313234" class="ar_12bw">If you have forgotten your password, click on the &quot;Forgot your Password&quot; link to have a reminder sent to you at the e-mail address you specified during registration. </td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="35" align="left" bgcolor="#313234" class="ar_12bw"> 
            <asp:ImageButton ID="ImgForgot" runat="server"  ImageUrl="Images/forgot.gif" 
                width="133" height="18" 
                CausesValidation="False" onclick="ImgForgot_Click" /></td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      
       <tr>
        <td><img src="Images/dgray_left.jpg" width="16" height="15" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="1" height="1" /></td>
        <td><img src="Images/dright_gray.jpg" width="16" height="15" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="Images/left_d_corner.jpg" width="24" height="17" /></td>
    <td background="Images/bg_gray.jpg">&nbsp;</td>
    <td><img src="Images/right_dcorner.jpg" width="24" height="17" /></td>
  </tr>
</table>
</div>
    </form>
</body>
</html>