﻿<%@ page language="C#" autoeventwireup="true" inherits="AdminPanel_LeftLinks, App_Web_unlrms9o" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title>Wills Lifestyle</title>	
		 <link href="Styles/CLStyle.css" rel="stylesheet" type="text/css" />
		<link href="Styles/hcMenuStyles.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript">
		    TreeParams = {
		        OPEN_MULTIPLE_MENUS: false,
		        OPEN_WHILE_CLOSING: true,
		        TIME_DELAY: 100,
		        OPEN_MENU_ICON: "images/menuopen.gif",
		        CLOSED_MENU_ICON: "images/menuoclose.gif"
		    };

		</script>
		<script   src="Includes/utils.js"  type="text/javascript"  ></script>
		<script   src="Includes/AnimTree.js" type="text/javascript"></script>		
		
	</head>
	<body  >
		<form id="Form1" method="post" runat="server">
		
			<%--<div  id="divcombo"></div>--%>
			<DIV id="nav">
           
				<DIV class="button" id="divVideo" runat="server" visible="false">
					<SPAN class="buttonlabel" onClick="toggleMenu(this);return false">
						<TABLE cellSpacing="0" cellPadding="0" width="175" border="0">
							<TR>
								<TD class="MenuBgDark" vAlign="middle" align="center" width="26" style="height: 26px"><IMG src="images/menuoclose.gif"></TD>
								<TD class="border" width="1" style="height: 26px"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
								<TD class="MenuBgDark" style="height: 26px"><A href="#" target="right"><b>Video</b></A></TD>
							</TR>
							<TR>
								<TD class="border" colSpan="3" height="1"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
							</TR>
						</TABLE>
					</SPAN>
				</DIV>
				<DIV class="menu" id="divVideoMenu" runat="server" visible="false">
				    <DIV class="menuItem" id="divaddworkout" >
						<TABLE cellSpacing="0" cellPadding="0" width="175" border="0">
							<TR class="menuItemBg">
								<TD vAlign="middle" align="right" width="26" height="26"><IMG src="images/arrow_red.gif">&nbsp;&nbsp;</TD>
								<TD class="border" width="1" height="26"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
								<TD height="26"><A href="Video_AddEdit.aspx" target="right">Add Viedio</A>
								</TD>
							</TR>
							<TR>
								<TD class="border" colSpan="3" height="1"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
							</TR>
						</TABLE>
					</DIV>

                    

					<DIV class="menuItem" id="divViewWorkout" >
						<TABLE cellSpacing="0" cellPadding="0" width="175" border="0"> 
							<TR  class="menuItemBg">
								<TD vAlign="middle" align="right" width="26" height="26"><IMG src="images/arrow_red.gif">&nbsp;&nbsp;</TD>
								<TD class="border" width="1" height="26"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
								<TD height="26"><A href="Video_View.aspx" target="right">View Video</A>
								</TD>
							</TR>
							<TR>
								<TD class="border" colSpan="3" height="1"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
							</TR>
						</TABLE>
					</DIV>
					
					
					
				</DIV>
				
				
                   
			        <%--Change Passoword--%>
			        <DIV class="button" id="divChangePassoword" runat="server" visible="false">
					<SPAN class="buttonlabel" onClick="toggleMenu(this);return false">
						<TABLE cellSpacing="0" cellPadding="0" width="175" border="0">
							<TR>
								<TD class="MenuBgDark" vAlign="middle" align="center" width="26" style="height: 26px"><IMG src="images/menuoclose.gif"></TD>
								<TD class="border" width="1" style="height: 26px"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
								<TD class="MenuBgDark" style="height: 26px"><A href="AdminUser_ResetPassword.aspx" target="right"><b>Change Password</b></A></TD>
							</TR>
							<TR>
								<TD class="border" colSpan="3" height="1"><IMG height="1" src="Images/spacer.gif" width="1"></TD>
							</TR>
						</TABLE>
					</SPAN>
				</DIV>
					
					
				
					
					
					
					
					
				</DIV>
		</form>
	</body>
</html>


