﻿<%@ page language="C#" autoeventwireup="true" inherits="AdminPanel_ForgotPassword, App_Web_unlrms9o" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Wills Lifestyle</title>
<script language="javascript">
    // ******   Java Script Start here for Email Checking ***********


    function echeck(str) {
        var at = "@"
        var dot = "."
        var lat = str.indexOf(at)
        var lstr = str.length
        var ldot = str.indexOf(dot)
        if (str.indexOf(at) == -1) {
            alert("Invalid E-mail ID")
            return false
        }

        if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
            alert("Invalid E-mail ID")
            return false
        }

        if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
            alert("Invalid E-mail ID")
            return false
        }

        if (str.indexOf(at, (lat + 1)) != -1) {
            alert("Invalid E-mail ID")
            return false
        }

        if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
            alert("Invalid E-mail ID")
            return false
        }

        if (str.indexOf(dot, (lat + 2)) == -1) {
            alert("Invalid E-mail ID")
            return false
        }

        if (str.indexOf(" ") != -1) {
            alert("Invalid E-mail ID")
            return false
        }
        if ((str.indexOf("..") > -1) || (str.substring(str.length - 1, str.length) == dot)) {
            alert("Invalid E-mail ID")
            return false
        }
        return true
    }
    // ******   Java Script end here for Email Checking ***********
    function validation() {
        if (document.getElementById("txtmail").value == "") {
            alert("Please enter valid Email Id");
            document.getElementById("txtmail").value = "";
            document.Form1.txtmail.focus();
            return false;
        }
        if (!echeck(document.getElementById("txtmail").value)) {
            //alert("Please enter valid Email Id");
            document.getElementById("txtmail").value = "";
            document.Form1.txtmail.focus();
            return false;
        }
        return true;
    }

        </script>
		  <link href="Styles/CLStyle.css" rel="stylesheet" type="text/css" />     
    </HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" defaultfocus="txtmail" method="post" runat="server">			
			<div id="centered">
<table width="554" border="0" align="center" bgcolor="white" cellpadding="0" cellspacing="0">
<tr>
    <td width="24"><img src="Images/left_wcorner.jpg" width="24" height="17" /></td>
    <td width="506" align="left" background="Images/bg_top.jpg"><img src="Images/trans.gif" width="1" height="1" /></td>
    <td width="24" align="right"><img src="Images/right_wcorner.jpg" width="24" height="17" /></td>
  </tr>
  
  <tr>
    <td colspan="3" align="center" background="Images/bg_white.jpg">
    <table width="536" border="0" cellspacing="0" bgcolor="white" cellpadding="0">
     <%-- <tr>
      
        <td align="left" colspan="3" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="65" align="left" valign="bottom" ><img src="Images/Heads Up For Tails-logo.jpg"  /></td>
            <td align="right" valign="bottom" class="ar_20b">Control Panel Login</td>
          </tr>
        </table></td>
        
      </tr>
      <tr>
        <td align="left">&nbsp;</td>
        <td align="left" bgcolor="#FFFFFF"></td>
        <td align="right">&nbsp;</td>
      </tr>
      
      <tr>
        <td width="16" align="left"><img src="Images/gray_left.jpg" width="16" height="15" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="1" height="1" /></td>
        <td width="16" align="right"><img src="Images/right_gray.jpg" width="16" height="15" /></td>
      </tr>
      <tr>
        <td colspan="3" bgcolor="#313234"><img src="Images/trans.gif" width="8" height="8" /></td>
        </tr>--%>
        <tr>
        <td width="16" align="left"><img src="Images/gray_left.jpg" width="16" height="15" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="1" height="1" /></td>
        <td width="16" align="right"><img src="Images/right_gray.jpg" width="16" height="15" /></td>
      </tr>
      <tr>
        <td colspan="3" bgcolor="#313234"><img src="Images/trans.gif" width="8" height="8" /></td>
        </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="40" align="left" valign="top" bgcolor="#313234" class="ar_12bw">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="65" align="left" valign="bottom" ><img src="Images/Logo.png" /></td>
            <td align="right" valign="bottom" class="ar_20b" style="color:White;">Control Panel Login</td>
          </tr>
        </table>
        </td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      
        <tr>
        <td align="left" bgcolor="#313234">&nbsp;</td>
        <td align="left" bgcolor="#313234"></td>
        <td align="right" bgcolor="#313234">&nbsp;</td>
      </tr>
       <tr>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td align="left" bgcolor="#909193" class="ar_12bw"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
      </tr>
       <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td  align="left" valign="top" bgcolor="#313234" class="ar_12bw"></td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="40" align="left" valign="top" bgcolor="#313234" class="ar_12bw">If you've forgotten your password, you don't have to register 
														all over again. Just fill in the fields below and you'll receive an email 
														shortly with that information.  </td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td align="left" bgcolor="#909193" class="ar_12bw"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td align="center" bgcolor="#313234" class="ar_12bw"><asp:Label ID="lblError" runat="Server" EnableViewState="False" ForeColor="Red" Font-Size="Small"></asp:Label>&nbsp;</td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td align="center" bgcolor="#313234" class="ar_12bw"><table width="98%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td  align="left" width="110px" class="ar_12bw">Email Address</td>
            <td  align="left" class="ar_12bw">&nbsp;<asp:textbox id="txtmail" runat="Server" width="175px"></asp:textbox>
             <asp:RequiredFieldValidator ID="rfvEmamil" CssClass="errorMessage" ValidationGroup="Forgot" ForeColor="Red"
                        runat="server" ControlToValidate="txtmail" Display="Dynamic" ErrorMessage="Please enter email."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="Forgot" runat="server" ControlToValidate="txtmail" ForeColor="Red"
                        Display="Dynamic" ErrorMessage="Invalid email" CssClass="errorMessage"></asp:RegularExpressionValidator>
            </td>
          </tr>
          <tr>
            <td colspan="2"><img src="Images/trans.gif" width="1" height="6" /></td>
          </tr>
          <tr>
            <td align="left" class="ar_12bw">&nbsp;</td>
            <td align="left" class="ar_12bw">
               <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click"  ValidationGroup="Forgot"  Text="Submit" TabIndex="1" />
                    &nbsp;   &nbsp; <a href="AdminUser_Login.aspx" style="color:#ffffff;"> Back to Login </a> </td>
          </tr>
          <tr>
            <td colspan="2" align="left" class="ar_12bw"><img src="Images/trans.gif" width="1" height="6" /></td>
            </tr>
          <%--<tr>
            <td align="left" class="ar_12bw">&nbsp;</td>
            <td align="left" class="ar_12bw">
                <asp:ImageButton ID="ImgButtonSignIn" 
                    runat="server" ImageUrl="Images/sign-in.gif" width="54" height="18" onclick="ImgButtonSignIn_Click" 
                    />&nbsp;&nbsp;
                                     
                   
                    <asp:ImageButton ID="ImgBtnReset" runat="server" 
                    ImageUrl="Images/reset.gif" width="54" height="18" OnClientClick="javascript:return fnreset();"  CausesValidation="False" /></td>
          </tr>--%>
        </table></td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td align="left" bgcolor="#313234" class="ar_12bw">&nbsp;</td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td align="left" bgcolor="#909193" class="ar_12bw"><img src="Images/trans.gif" width="8" height="1" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="8" height="1" /></td>
      </tr>
      
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="40" align="left" valign="bottom" bgcolor="#313234" class="ar_12bw"> </td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#313234">&nbsp;</td>
        <td height="35" align="left" bgcolor="#313234" class="ar_12bw"> 
            <%--<asp:ImageButton ID="ImgForgot" runat="server"  ImageUrl="Images/forgot.gif" 
                width="133" height="18" 
                CausesValidation="False" onclick="ImgForgot_Click" />--%></td>
        <td bgcolor="#313234">&nbsp;</td>
      </tr>
      
       <tr>
        <td><img src="Images/dgray_left.jpg" width="16" height="15" /></td>
        <td bgcolor="#313234"><img src="Images/trans.gif" width="1" height="1" /></td>
        <td><img src="Images/dright_gray.jpg" width="16" height="15" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><img src="Images/left_d_corner.jpg" width="24" height="17" /></td>
    <td background="Images/bg_gray.jpg">&nbsp;</td>
    <td><img src="Images/right_dcorner.jpg" width="24" height="17" /></td>
  </tr>
</table>
</div>
		</form>
	</body>
</html>
