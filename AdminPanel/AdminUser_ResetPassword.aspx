﻿<%@ page language="C#" autoeventwireup="true" inherits="AdminPanel_AdminUser_ResetPassword, App_Web_unlrms9o" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <title>:: Control panel :: Reset  User Password ::</title>  
    <link href="Styles/CLStyle.css" type="text/css" rel="stylesheet">
    <script src="Includes/PValidation.js" type="text/javascript"></script>
    <script language="javascript">

        function Trim(str) {
            return str.replace(/^\s*|\s*$/g, "");
        }

        function Check() {
            if (Trim(document.Form1.txtNewPassword.value) == "") {
                alert('Please enter new Password');
                document.Form1.txtNewPassword.value = "";
                document.Form1.txtNewPassword.focus();
                return false;
            }
            if (Trim(document.Form1.txtNewPassword.value).length < 5) {
                alert("New password should be minimum 5 digits.");
                document.Form1.txtNewPassword.focus();
                return false;
            }

            if (Trim(document.Form1.txtRetypePassword.value) == "") {
                alert('Please Retype new Password');
                document.Form1.txtRetypePassword.value = "";
                document.Form1.txtRetypePassword.focus();
                return false;
            }

            if (Trim(document.Form1.txtRetypePassword.value) != Trim(document.Form1.txtNewPassword.value)) {
                alert('Passwords are different\nPlease Retype Password correctly');
                document.Form1.txtNewPassword.value = "";
                document.Form1.txtRetypePassword.value = "";
                document.Form1.txtNewPassword.focus();
                return false;
            }


            return true;
        }
    </script>

</head>
<body >
    <form id="Form1" method="post" runat="server">
    <table style="width: 100%; border-right: #808080 2px solid; border-top: #808080 2px solid;
        border-left: #808080 2px solid; border-bottom: #808080 2px solid">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td >
                        </td>
                        <td >
                        </td>
                    </tr>
                    <tr>
                        <td class="GridAltItem" align="right" width="40%" >
                            Enter New Password&nbsp;&nbsp;
                        </td>
                        <td  class="GridAltItem" width="60%" >
                            <asp:TextBox ID="txtNewPassword" runat="server" MaxLength="15"  TextMode="Password"></asp:TextBox>&nbsp;(15 Char Max)<br />
                            <asp:RequiredFieldValidator ID="rdpwd" runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Please enter new password !" ValidationGroup="n" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td  height="1px"></td><td ></td>
                    </tr>
                    <tr>
                        <td align="right" class="GridAltItem" width="40%" >
                            Retype Password&nbsp;&nbsp;
                        </td>
                        <td class="GridAltItem"  width="60%" >
                            <asp:TextBox ID="txtRetypePassword" runat="server" MaxLength="15"  TextMode="Password"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="txtRetypePassword" ErrorMessage="Please re-enter new password !" ValidationGroup="n"></asp:RequiredFieldValidator>
                             <asp:CompareValidator ID="CompareValidator1" runat="server"  ForeColor="Red" ControlToCompare="txtNewPassword" 
                                                ControlToValidate="txtRetypePassword" Display="Dynamic" ErrorMessage="Password mismatch."
                                                ValidationGroup="n" EnableClientScript="true"></asp:CompareValidator>
                        </td>
                    </tr>
                     <tr>
                    <td height="5px" ></td><td ></td>
                </tr>
                    <tr>
                        <td >
                        </td>
                        <td >
                            &nbsp;<asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Submit" ValidationGroup="n"/>
                            &nbsp;<asp:Button ID="btnCancel" OnClientClick="javascript:window.close();"  Visible="false"  runat="server" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td >
                        </td>
                        <td >
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
<%--</body>--%>
</html>
