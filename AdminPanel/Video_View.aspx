﻿<%@ page language="C#" autoeventwireup="true" inherits="AdminPanel_Video_View, App_Web_unlrms9o" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Workouts</title>
    <link href="Styles/CLStyle.css" type="text/css" rel="stylesheet">
    <link href="Styles/hcMenuStyles.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script> 
    <script language="javascript">
        function GotoPrev() {
            window.location.href = 'Right.htm';
        }
        function SetPermissionWindow(arg) {

            window.open('AdminUsers_permissions.aspx?AdminID=' + arg + '', '', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=yes,width=425,height=400,left=100, top=50');
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table cellspacing="0" cellpadding="3" width="100%" border="0">
            <tr>
                <td valign="top" width="99%" style="height: 336px">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Bg1" valign="top" width="5">
                                <img height="5" src="images/ctl.gif" width="5">
                            </td>
                            <td class="Bg1" align="center" width="62">
                                <img height="6" src="images/arrow_black.gif" width="9">
                            </td>
                            <td width="27">
                                <img height="22" src="images/slice1.gif" width="27">
                            </td>
                            <td class="header2" nowrap>Video List
                            </td>
                            <td class="header2" nowrap align="right">[ Total :
                            <asp:Label ID="lblTotalRecord" runat="server"></asp:Label>]
                            </td>
                            <td class="header2" valign="top" align="right" width="13"></td>
                        </tr>
                        <tr>
                            <td class="Bg1" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                            <td class="Bg1" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                            <td width="27" height="3">
                                <img height="3" src="images/slice2.gif" width="27">
                            </td>
                            <td bgcolor="#ffffff" colspan="3" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                        </tr>
                        <tr>
                            <td class="Bg1" colspan="6" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                        </tr>
                    </table>
                    <img height="5" src="images/spacer.gif" width="1">
                    <table class="Bg1" align="center" cellspacing="0" cellpadding="0" width="50%" border="0">
                        <tr>
                            <td width="5" height="5">
                                <img height="5" src="images/ctl.gif" width="5">
                            </td>
                            <td height="5">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                            <td width="5" height="5">
                                <img class="SetImageFace" height="5" src="images/ctr.gif" width="5">
                            </td>
                        </tr>
                        <tr>
                            <td width="5" height="40px">&nbsp;
                            </td>
                            <td align="center">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center" width="77%" style="padding-left: 15px">
                                            <asp:RadioButtonList ID="rblActive" runat="server" CssClass="Normal" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="2">All</asp:ListItem>
                                                <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                                <asp:ListItem Value="0">Inactive</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td align="left">
                                            <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="images/search.gif" ImageAlign="AbsBottom"
                                                OnClick="btnSearch_Click"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="5" height="6">&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    <img height="5" src="images/spacer.gif" width="1">
                    <table runat="server" id="tbl_DgrUserList" cellspacing="0" cellpadding="0" width="100%"
                        border="0">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblData" ForeColor="Red" runat="server"></asp:Label>
                              <%--  <asp:Button ID="btnExport" Visible="false" runat="server" Text="Export to Excel"
                                    Width="130px" OnClick="btnExport_Click" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GrdViewWorkout" runat="server" Width="100%" BorderWidth="0px" CellPadding="0"
                                    CssClass="Grid" AutoGenerateColumns="False" GridLines="None" AllowSorting="True"
                                    DataKeyNames="VideoId" HeaderStyle-HorizontalAlign="Left" OnPageIndexChanging="GrdViewWorkout_PageIndexChanging"
                                    EmptyDataText="<b>No Record Found.</b>">
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                    <RowStyle HorizontalAlign="Left" CssClass="GridItem" />
                                    <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                                    <FooterStyle CssClass="GridItem"></FooterStyle>
                                    <Columns>
                                        <asp:BoundField Visible="False" DataField="VideoId" ReadOnly="True" HeaderText="VideoId"></asp:BoundField>
                                        <asp:BoundField DataField="VideoTitle" HeaderText="Video Title">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        </asp:BoundField>
                                     
                                        <asp:BoundField DataField="VideoYoutubeId" HeaderText="Youtube VideoId">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        </asp:BoundField>
                               <asp:TemplateField HeaderText="Image">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hfId" Value='<%#Eval("VideoYoutubeId") %>' runat="server" Visible="false" />
                                                <asp:Image ID="Image1" ImageUrl='<%# Eval("VideoYoutubeId", "http://img.youtube.com/vi/{0}/mqdefault.jpg") %>'
                                                    Height="100" Width="100" name="ImageStory" runat="server" />
                                               
                                                             
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:HyperLinkField HeaderText="Edit" DataNavigateUrlFormatString="Video_AddEdit.aspx?VideoID={0}"
                                            Text="&lt;img src=images/edit.gif border=0&gt;" DataNavigateUrlFields="eVideoId">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:HyperLinkField>
                                        
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--Navigation Start--%>
                                <table id="tblNavigator" cellspacing="0" cellpadding="0" width="100%" border="0"
                                    runat="server" class="Grid1">
                                    <tr>
                                        <td class="GridHeader" width="35%">
                                            <%--<div id="divTotalPages" style="display: block" align="left" runat="server">
                                                                <strong>Results:
                                                                    <asp:Label ID="lblCurrentPage" runat="server"></asp:Label></strong> - <strong>
                                                                        <asp:Label ID="lblTotalPages" runat="server"></asp:Label></strong> of <strong>
                                                                            <asp:Label ID="lblTotNoofrec" runat="server"></asp:Label></strong></div>--%>
                                        </td>
                                        <td class="GridHeader" style="width: 30%">
                                            <div id="divTotalPages1" style="display: block" align="center" runat="server">
                                                Result Per Page:
                                            <asp:DropDownList ID="ddlPerPage" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPerPage_SelectedIndexChanged">
                                                <asp:ListItem Value="10" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="20"></asp:ListItem>
                                                <asp:ListItem Value="30"></asp:ListItem>
                                                <asp:ListItem Value="40"></asp:ListItem>
                                                <asp:ListItem Value="50"></asp:ListItem>
                                                <asp:ListItem Value="100"></asp:ListItem>
                                            </asp:DropDownList>
                                                <span class="textbiggest">Page Number:</span>
                                                <asp:DropDownList ID="ddlpages" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlpages_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <td width="35%" class="GridHeader">
                                            <div id="Navigationlnk" style="display: block" align="right" runat="server">
                                                &nbsp;
                                            <asp:LinkButton ID="btnFirst" runat="server" Text="First" OnClick="btnFirst_Click"></asp:LinkButton>&nbsp;
                                            <asp:LinkButton ID="btnPrev" runat="server" Text="Prev" OnClick="btnPrev_Click"></asp:LinkButton>&nbsp;
                                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>&nbsp;/&nbsp;
                                            <asp:Label ID="lblTotalPage" runat="server"></asp:Label>&nbsp;
                                            <asp:LinkButton ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click"></asp:LinkButton>&nbsp;
                                            <asp:LinkButton ID="btnLast" runat="server" Text="Last" OnClick="btnLast_Click"></asp:LinkButton>&nbsp;
                                            <asp:HiddenField ID="hdfCurrentPageNo" runat="server" Value="0" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <%--Navigation Ends--%>
                            </td>
                        </tr>
                    </table>
                    <table runat="server" id="tbl_RecordNotFound" visible="false" cellspacing="0" cellpadding="0"
                        width="100%" border="0">
                        <tr>
                            <td align="center">NO RECORD FOUND
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input id="tmpDelete" type="hidden" name="tmpDelete" runat="server">
    </form>
</body>
</html>

