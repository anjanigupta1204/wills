function Trim(TRIM_VALUE)
{
	if(TRIM_VALUE.length < 1)
	{
		return"";
	}
	TRIM_VALUE = RTrim(TRIM_VALUE);
	TRIM_VALUE = LTrim(TRIM_VALUE);
	if(TRIM_VALUE=="")
	{
		return "";
	}
	else
	{
		return TRIM_VALUE;
	}
} //End Function

function RTrim(VALUE)
{
	var w_space = String.fromCharCode(32);
	var v_length = VALUE.length;
	var strTemp = "";
	if(v_length < 0)
	{
		return"";
	}
	var iTemp = v_length -1;

	while(iTemp > -1)
	{
		if(VALUE.charAt(iTemp) == w_space)
		{
		}
		else
		{
			strTemp = VALUE.substring(0,iTemp +1);
			break;
		}
		iTemp = iTemp-1;

	} //End While
	return strTemp;

} //End Function

function LTrim(VALUE)
{
	var w_space = String.fromCharCode(32);
	if(v_length < 1)
	{
		return"";
	}
		var v_length = VALUE.length;
		var strTemp = "";

		var iTemp = 0;

	while(iTemp < v_length)
	{
		if(VALUE.charAt(iTemp) == w_space)
		{
		}
		else
		{
			strTemp = VALUE.substring(iTemp,v_length);
			break;
		}
		iTemp = iTemp + 1;
	} //End While
	return strTemp;
} //End Function

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Common Functions to be used in other functions

// chkNumeric function can be used for telephone, zipcode validation etc. or at any place where only numeric data is required.
//    function chkNumeric()
//			{
//				if(event.shiftKey) return false;
//		 		if((event.keyCode<48 || event.keyCode>57) && event.keyCode != 190 && event.keyCode != 8 && event.keyCode != 37 && event.keyCode != 38 && event.keyCode != 39 && event.keyCode != 40 && event.keyCode != 46 && event.keyCode != 13 && event.keyCode != 116 && event.keyCode != 16 && (event.keyCode <96 || event.keyCode > 105) && event.keyCode != 9 && event.keyCode != 110) event.returnValue = false
//			}


function IsValidURL(urlStr) {

   
    if (urlStr.indexOf(" ") != -1) 
    {
        alert("Spaces are not allowed in a URL");
        return false;
    }
    if(urlStr.indexOf(".") == -1) 
    {
        alert("Invalid URL.\nURL should have at least one (.) character");
        return false;
    }
//    var domArr=urlStr.split(".");
//    var len=domArr.length;
//    alert(len);
//    if(len<2)
//    {
//        
//    }
  //  var x= ((urlStr.indexOf("http://") == -1) + (urlStr.indexOf("https://") == -1));
   
   var x= ((urlStr.indexOf("http://")) + (urlStr.indexOf("https://")));
   // alert(x);alert(urlStr.indexOf("http://"));alert(urlStr.indexOf("https://"));
    if(x==-2)
    {
        alert("Invalid URL.\nURL should start with http:// or https://");
        return false;
    }
    return true;
    
}

function isURL(urlStr) {
if (urlStr.indexOf(" ") != -1) {
alert("Spaces are not allowed in a URL");
return false;
}

if (urlStr == "" || urlStr == null) {
return true;
}

urlStr=urlStr.toLowerCase();

var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
var validChars="\[^\\s" + specialChars + "\]";
var atom=validChars + '+';
var urlPat=/^http:\/\/(\w*)\.([\-\+a-z0-9]*)\.(\w*)/;
var matchArray=urlStr.match(urlPat);

if (matchArray==null) {
//alert("The URL seems incorrect \ncheck it begins with http://\n and it has 2 .'s");
return false;
}

var user=matchArray[2];
var domain=matchArray[3];

for (i=0; i<user.length; i++) {
if (user.charCodeAt(i)>127) {
//alert("This domain contains invalid characters.");
return false;
}
}

for (i=0; i<domain.length; i++) {
if (domain.charCodeAt(i) > 127) {
//alert("This domain name contains invalid characters.");
return false;
}
}

var atomPat=new RegExp("^" + atom + "$");
var domArr=domain.split(".");
var len=domArr.length;

for (i=0;i<len;i++) {
if (domArr[i].search(atomPat) == -1) {
//alert("The domain name does not seem to be valid.");
return false;
}
}

return true;
} 



function IsValidChar()
        {
            //does not work in mozilla
            
	        var kk
	        kk=event.keyCode
        	//alert(kk);
	       // if ((kk>=65 && kk<=90) || (kk>=97 && kk<=122) || (kk>=48 && kk<=57) || kk==8 || kk==9  || kk==95 || kk==46 ||kk==190)
	        if ((kk>=65 && kk<=90) || (kk>=97 && kk<=122) ||  kk==32 )
	        {
	         return true;
	        }
	        return false;
        }

    function isValidCharacter(evt)
			{
			// MOzilla , safari, IE
				var charCode = (evt.which) ? evt.which : event.keyCode
		
			     if ((charCode>=65 && charCode<=90) || (charCode>=97 && charCode<=122) ||  charCode==32 ||  charCode==8)
                    {
                     return true;
                    }		
				return false;
			}



	function isNumberKey(evt)
			{
			    
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			}
		function isNumberKeyforStock(evt)
			{
			    
				var charCode = (evt.which) ? evt.which : event.keyCode
				//alert(charCode);
				if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=45)
					return false;
				return true;
			}	
function isFloatKey(evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					    {
					         if(charCode==46)
					            {
					                 return true;
					            }
					            {
					                 return false;
					            }
					       
					    }
				else {	
				       
				       return true;
				        
				     }
			}

function isNumberKey_Phone(evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 31 && (charCode < 48 || charCode > 57)) || charCode ==107 || charCode==109 ) 
					return false;
				return true;
			}

// This function should be used with almost every text type field, It Trims Starting White spaces from any string

function CheckMail(control,message) 
{
    var controlname=document.getElementById(control);
    var str= document.getElementById(control).value;
    if (str.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
    {
        return true;
    }
    else
    {
        alert(message);
        controlname.focus();
        return false;
    }					
}

function EmailFormatCheck(str) {

		var at="@"
		var dot="."
		var lat=str.indexOf(at)
		var lstr=str.length
		var ldot=str.indexOf(dot)
		if (str.indexOf(at)==-1){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		   alert("Invalid E-mail ID")
		   return false
		}

		if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		    alert("Invalid E-mail ID")
		    return false
		}

		 if (str.indexOf(at,(lat+1))!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		    alert("Invalid E-mail ID")
		    return false
		 }

		 if (str.indexOf(dot,(lat+2))==-1){
		    alert("Invalid E-mail ID")
		    return false
		 }
		
		 if (str.indexOf(" ")!=-1){
		    alert("Invalid E-mail ID")
		    return false
		 }

 		 return true					
	}


function isEmail (s)
		{  
			 // there must be >= 1 character before @, so we
			// start looking at character position 1 
			// (i.e. second character)
			var i = 1;
			var sLength = s.length;
			// look for @
			while ((i < sLength) && (s.charAt(i) != "@"))
			{
				i++;
			}

			if ((i >= sLength) || (s.charAt(i) != "@")) return false;
			else i += 2;

			// look for .
			while ((i < sLength) && (s.charAt(i) != "."))
			{ i++;
			}

			// there must be at least one character after the .
			if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
			else return true;
		}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++