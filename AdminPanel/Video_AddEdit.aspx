﻿<%@ page language="C#" autoeventwireup="true" inherits="AdminPanel_Video_AddEdit, App_Web_unlrms9o" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add Edit Workout</title>
    <link href="Styles/CLStyle.css" type="text/css" rel="stylesheet">
    <link href="Styles/hcMenuStyles.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script> 
    <script language="javascript">
        function GotoPrev() {
            window.location.href = 'Right.htm';
        }
        function SetPermissionWindow(arg) {

            window.open('AdminUsers_permissions.aspx?AdminID=' + arg + '', '', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=yes,width=425,height=400,left=100, top=50');
        }

        function custCardion_ClientValidate(source, args) {
            var SelectedValue = $("#ddlCategories option:selected").val();
            var Cardio = $("#ddlCardioSubCategory option:selected").val();
            args.IsValid = true;
            if (SelectedValue.toLowerCase() == 'cardio') {
                if (Cardio == '') {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }
            
        }


      function Check() {
            var SelectedValue = $("#ddlCategories option:selected").val();
            var Cardio = $("#ddlCardioSubCategory option:selected").val();
            //args.IsValid = true;
            //if (SelectedValue.toLowerCase() == 'cardio') {
            //    if (Cardio == '') {
            //        args.IsValid = false;
            //    }
            //    else {
            //        args.IsValid = true;
            //    }
            //}
        }

    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
      
        <table cellspacing="0" cellpadding="3" width="100%" border="0">
            <tr>
                <td valign="top" width="99%" style="height: 336px">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="Bg1" valign="top" width="5">
                                <img height="5" src="images/ctl.gif" width="5">
                            </td>
                            <td class="Bg1" align="center" width="62">
                                <img height="6" src="images/arrow_black.gif" width="9">
                            </td>
                            <td width="27">
                                <img height="22" src="images/slice1.gif" width="27">
                            </td>
                            <td class="header2" nowrap>
                                <asp:Label ID="lblAddEdit" runat="server"></asp:Label>&nbsp; Video</td>
                            <td class="header2" nowrap align="right"></td>
                            <td class="header2" valign="top" align="right" width="13"></td>
                        </tr>
                        <tr>
                            <td class="Bg1" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                            <td class="Bg1" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                            <td width="27" height="3">
                                <img height="3" src="images/slice2.gif" width="27">
                            </td>
                            <td bgcolor="#ffffff" colspan="3" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                        </tr>
                        <tr>
                            <td class="Bg1" colspan="6" height="3">
                                <img height="1" src="images/spacer.gif" width="1">
                            </td>
                        </tr>
                    </table>
                    <img height="5" src="images/spacer.gif" width="1">
                    <img height="5" src="images/spacer.gif" width="1">
                    <table runat="server" id="tbl_DgrUserList" cellspacing="0" cellpadding="0" width="100%"
                        border="0">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblData" ForeColor="Red" runat="server"></asp:Label>
                                <asp:Button ID="btnExport" Visible="false" runat="server" Text="Export to Excel"
                                    Width="130px" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table class="Grid2" cellspacing="1" cellpadding="0" width="100%" align="center"
                                    border="0">
                                    <tr>
                                        <td valign="middle" class="GridAltItem" align="right" width="40%">Video Title:<font color="red">(*)</font>
                                        </td>
                                        <td class="GridItem" align="left" width="60%">
                                            <asp:TextBox ID="txtVideoTitle" runat="server" TabIndex="4" MaxLength="100" Width="500"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RFVWorkouttName" ControlToValidate="txtVideoTitle"
                                                ForeColor="Red" Display="Dynamic" ValidationGroup="n" runat="server" ErrorMessage="Please enter video Title."></asp:RequiredFieldValidator>
                                           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ForeColor="Red" Display="Dynamic" ValidationGroup="n" runat="server" ControlToValidate="txtWorkoutName"
                                                ValidationExpression="[a-zA-Z ]*$" ErrorMessage="Plese enter only alphabets ." />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="GridAltItem" align="right" width="40%">Video Description:<font color="red">(*)</font>
                                        </td>
                                        <td class="GridItem" align="left" width="60%">
                                            <asp:TextBox ID="txtVideoDescription" runat="server" TextMode="MultiLine" Rows="5" Width="725px" MaxLength="150"></asp:TextBox>
                                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtVideoDescription"
                                                ForeColor="Red" Display="Dynamic" ValidationGroup="n" runat="server" ErrorMessage="Please enter Short Description."></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td valign="middle" class="GridAltItem" align="right" width="40%">Video Id:
                                        </td>
                                        <td class="GridItem" align="left" width="60%">
                                            <asp:TextBox runat="server" ID="txtyoutubeId" MaxLength="1000" Width="500"></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtyoutubeId"
                                                ForeColor="Red" Display="Dynamic" ValidationGroup="n" runat="server" ErrorMessage="Please enter youtube video id."></asp:RequiredFieldValidator>
                                         <br />(Ex: https://www.youtube.com/embed/<font color="red">VideId</font>)
                                        </td>
                                           
                                       
                                    </tr>
                                    

                                    

                                     

                                    
                                    <tr>
                                        <td id="td2" runat="server" class="GridAltItem" valign="middle" align="right" width="40%">Active:
                                        </td>
                                        <td class="GridItem" align="left" width="60%">
                                            <asp:CheckBox ID="ChkActive" runat="server" Checked="true" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr class="GridHeader">
                                        <td></td>
                                        <td align="left">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnsubmit_Click" ValidationGroup="n"
                                                TabIndex="7" />
                                            &nbsp;<asp:Button ID="btnCancel" Visible="false" runat="server" Text="Cancel" TabIndex="7"
                                                OnClick="btnCancel_Click" />
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
     
       
        <input id="tmpDelete" type="hidden" name="tmpDelete" runat="server" />
    </form>
</body>
</html>
