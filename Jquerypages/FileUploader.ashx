﻿<%@ WebHandler Language="C#" Class="FileUploader" %>

using System;
using System.Web;
using System.Text;
using System.IO;
using System.Net;
using System.Configuration;
using System.Net.Mail;
public class FileUploader : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        string _fileName = string.Empty;
        Byte[] bytes = null;
        if (context.Request.Files.Count > 0)
        {
           
            HttpPostedFile file = context.Request.Files[0];
            string fileName,filePath=string.Empty;
            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
            {
                string[] files = file.FileName.Split(new char[] { '\\' });
                fileName = files[files.Length - 1];
            }
            else
            {
                fileName = file.FileName;
            }
            if (fileName != null && fileName != "")
            {
                string Extension = fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower();
                if (Extension == "doc" || Extension == "docx" || Extension == "pdf" || Extension == "jpg" || Extension == "png" || Extension == "jpeg")
                {

                    bytes = new byte[file.ContentLength];
                    file.InputStream.Read(bytes, 0, file.ContentLength);
                    //write your code here
                }

            }

        }
            
          
            string _name = context.Request.Form["name"];
            string _contact = context.Request.Form["phone"];
            string _email = context.Request.Form["email"];
            string _feedbacktype = context.Request.Form["feedbacktype"];
            string _comments = context.Request.Form["comments"];
            string _captchaText =Convert.ToString( context.Request.Form["sCaptchText"]);
            if (context.Session["splashImageText"] != null && Convert.ToString(context.Session["splashImageText"]) == _captchaText)
            {
                if (CheckInput(_name, _email, _contact, _feedbacktype, _comments))
                {

                    //////////////insert data in table ////////////////////////////

                    System.Data.SqlClient.SqlCommand osqlcmd;
                    System.Data.SqlClient.SqlTransaction osqltransaction;
                    System.Data.SqlClient.SqlConnection osqlconnection;
                    System.Data.SqlClient.SqlDataAdapter da;
                    string returnValue = string.Empty;
                    osqlconnection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppSettings["willslifeCon"]);
                    try
                    {
                        osqlconnection.Open();
                    }
                    catch (Exception ee)
                    {
                        osqlconnection.Close();

                    }
                    osqltransaction = osqlconnection.BeginTransaction();
                    osqlcmd = new System.Data.SqlClient.SqlCommand("InsertTradeEnquiries_sp", osqlconnection);
                    try
                    {
                        osqlcmd.Transaction = osqltransaction;
                        osqlcmd.CommandType = System.Data.CommandType.StoredProcedure;
                        osqlcmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@varEmail", _email));
                        osqlcmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@varMobile", _contact));
                        osqlcmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@varName", _name));
                        osqlcmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@varFeedback", _comments));
                        osqlcmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@varFeedBackType", _feedbacktype));
                        osqlcmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@varFilename", bytes));
                        returnValue = Convert.ToString(osqlcmd.ExecuteNonQuery());
                        osqltransaction.Commit();

                        System.Text.StringBuilder html = new System.Text.StringBuilder();
                        html.Append("<table width=573 border=0 cellpadding=2 cellspacing=1>"
                       + "<tr><td valign=middle width=16 align='right'></td>"
                       + "<td width=546><font face=Arial, Helvetica, sans-serif size=2 color=#991415></font></td>"
                       + "</tr><tr><td></td><td>"
                       + "<table width=547 border=0 cellpadding=2 cellspacing=1>"
                       + "<td width=141  bgcolor=#EBEBEB><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>Name</font></td> "
                       + "<td width=395  bgcolor=#EBEBEB><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>" + WebUtility.initCapMulti(_name) + "</font></td> "
                       + "</tr> "
                       + "<tr>  "
                       + "<td  bgcolor=#F9F9F9><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>Email</font></td> "
                       + "<td  bgcolor=#F9F9F9><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>" + _email + "</font></td> "
                       + "</tr> "
                       + "<tr>  "
                       + "<td  bgcolor=#F9F9F9><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>Mobile</font></td> "
                       + "<td  bgcolor=#F9F9F9><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>" + _contact + "</font></td> "
                       + "</tr> "
                       + "<tr>  "
                       + "<td  bgcolor=#EBEBEB><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>Nature of Enquiry </font></td> "
                       + "<td  bgcolor=#EBEBEB><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>" + _feedbacktype + "</font></td> "
                       + "</tr> "
                       + "<tr>     "
                       + "<td  valign=top bgcolor=#EBEBEB><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>Comments</font></td>  "
                       + "<td  bgcolor=#EBEBEB><font size=1 face='Verdana, Arial, Helvetica, sans-serif'>" + _comments + "</font></td> "
                       + "</tr>"
                       + "</table>"
                       + "</td>"
                       + "</table>");

                        string TOEmail = ConfigurationSettings.AppSettings["ToMailTrade"].ToString();
                        string FromMail = ConfigurationSettings.AppSettings["FromMailTrade"].ToString();
                       
                       
                        
                        context.Session["splashImageText"] = null;
                       // WebUtility.SendMail(TOEmail, FromMail, html.ToString(), "Trade Enquiry from WillsLifestyle.com");
                       
                        //web authenticated mail 
                        SendWebMail(TOEmail, FromMail, html.ToString(), "Trade Enquiry from WillsLifestyle.com");
                    

                    }
                    catch (Exception ee)
                    {
                        osqltransaction.Rollback();
                        
                    }
                    finally
                    {
                        osqltransaction.Dispose();
                        osqlcmd.Dispose();
                        osqlconnection.Close();
                        string msg = "{";
                        msg += string.Format("error:'{0}',\n", string.Empty);
                        msg += string.Format("msg:'{0}'\n", "Ok");
                        msg += "}";

                        context.Response.Write(msg);
                    }
                    ////////////////////////////////////////////////////////////


                    
                }
                else
                {

                    string msg = "{";
                    msg += string.Format("error:'{0}',\n", string.Empty);
                    msg += string.Format("msg:'{0}'\n", "invalid user input");
                    msg += "}";
                    context.Response.Write(msg);

                }
            }
                else
                {
                    string msg = "{";
                    msg += string.Format("error:'{0}',\n", string.Empty);
                    msg += string.Format("msg:'{0}'\n", "Captcha text mismatch!");
                    msg += "}";
                    context.Response.Write(msg);

                }
          


        
    }
    protected bool CheckInput(string FName, string email, string contact, string feedbacknature, string comments)
    {
        if (clsCommon.CheckLength(FName, 50) && clsCommon.IsAlphaSpace(FName) &&
            clsCommon.fnValidEmailID(email) && clsCommon.CheckLength(contact, 10) && clsCommon.IsInteger(contact) &&
            clsCommon.CheckLength(feedbacknature, 50) && clsCommon.IsAlphaSpace(feedbacknature) &&
             clsCommon.CheckLength(comments, 2000)
           )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void SendWebMail(string Toemail, string FromEmail, string Bodyhtml, string subject)
    {
        System.Net.Mail.MailMessage sendMail = new System.Net.Mail.MailMessage();
        sendMail.To.Add(Toemail);
        sendMail.From = new MailAddress(FromEmail);
        sendMail.Body = Bodyhtml;
        sendMail.IsBodyHtml = true;
        sendMail.Subject = subject;

        string smtpUserName = ConfigurationSettings.AppSettings["SmtpUserNameTrade"].ToString();
        string smtpPassword = ConfigurationSettings.AppSettings["SmtpPasswordTrade"].ToString();
        string smtpServer = ConfigurationSettings.AppSettings["SmtpIPTrade"].ToString();
        int smtpPort = Convert.ToInt32(ConfigurationSettings.AppSettings["SmtpPortTrade"]);
        if (smtpUserName != "")
        {
            SmtpClient SMTPServer = new SmtpClient(smtpServer, smtpPort);
            SMTPServer.Credentials = new System.Net.NetworkCredential(smtpUserName, smtpPassword);

            SMTPServer.Send(sendMail);

        }
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}