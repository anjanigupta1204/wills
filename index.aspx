﻿<%@ page language="C#" autoeventwireup="true" inherits="index, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Welcome to Wills Lifestyle</title>
    <meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
    <meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <![endif]-->
 
    <!--Favicon for the website-->
    <link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
    <!--Main CSS, containing struction and formating-->
    <link href="css/loader.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/slider.css" rel="stylesheet" type="text/css" />

    <link href="lightbox/facebox.css" rel="stylesheet" type="text/css" />

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
     <link href="css/skdslider.css" rel="stylesheet" />




  </head>
  <body>
    <form id="form1" runat="server" style="width:100%; height: 100%;">

         <uc:navigation id="navigation1"  runat="server"   />

        <div id="demo" class="skdslider showondesktop">
            <ul>
                <li> <img src="img/desktop/fullscreen7.jpg" alt=""/> 
                <!--Slider Description example--> 
                </li>
                <li> <img src="img/desktop/fullscreen3.jpg" class="homesliderimg1_mobile1" alt="" /> </li>
                <li> <img src="img/desktop/fullscreen6.jpg" class="homesliderimg1_mobile2" alt="" /> </li>
                <li> <img src="img/desktop/fullscreen5.jpg" alt="" /> </li>
                <li> <img src="img/desktop/fullscreen9.jpg" alt="" /> </li>
                <!--<li> <img src="img/desktop/fullscreen6.jpg" alt="" /> </li>
                <li> <img src="img/desktop/fullscreen7.jpg" alt="" /> </li>
                <li> <img src="img/desktop/fullscreen8.jpg" alt="" /> </li>-->
            </ul>
        </div>
        <div id="Div1" class="skdslider showonipad">
            <ul>
                <li> <img src="img/ipad/medium7.jpg" alt="" /> 
                <!--Slider Description example--> 
                </li>
                <li> <img src="img/ipad/medium3.jpg" class="homesliderimg1_mobile1" alt="" /> </li>
                <li> <img src="img/ipad/medium6.jpg" class="homesliderimg1_mobile2" alt="" /> </li>
                <li> <img src="img/ipad/medium5.jpg" alt="" /> </li>
                <li> <img src="img/ipad/medium9.jpg" alt="" /> </li>
                <!--<li> <img src="img/ipad/medium6.jpg" alt="" /> </li>
                <li> <img src="img/ipad/medium7.jpg" alt="" /> </li>
                <li> <img src="img/ipad/medium8.jpg" alt="" /> </li>-->
            </ul>
         </div>
         <div id="" class="skdslider showonmobile">
          <ul>
            <!--<li> <img src="img/2014_mobile.jpg"/> 
      
      
      
            </li>
           <li> <img src="img/WLS_Classic_new1_mobile.jpg" /> </li>
            <li> <img src="img/WLS_Classic_new2_mobile.jpg" /> </li>
            <li> <img src="img/WLS_Shine_new1_mobile.jpg"/> </li>
            <li> <img src="img/WLS_Sport_1_mobile.jpg"/> </li>-->
              <li> <img src="img/mobile/mobile7.jpg" alt="" /> 
      
              <!--Slider Description example--> 
      
            </li>
            <li><img src="img/mobile/mobile3.jpg" class="homesliderimg1_mobile1" alt="" /> </li>
            <li><img src="img/mobile/mobile6.jpg" class="homesliderimg1_mobile2" alt="" /> </li>
            <li><img src="img/mobile/mobile5.jpg" alt="" /> </li>
            <li><img src="img/mobile/mobile9.jpg" alt=""  /></li>
            <!--<li><img src="img/mobile/mobile6.jpg" alt=""  /></li>
            <li><img src="img/mobile/mobile7.jpg" alt=""  /></li>
            <li><img src="img/mobile/mobile8.jpg" alt=""  /></li>-->
          </ul>
        </div>

    </form>
    
    
    <!-- <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>-->
<script type="text/javascript"src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/customjs_home.js"></script>

<!--Animation work start here-->

<script type="text/javascript" src="js/skdslider.js"></script>  
<script type="text/javascript">

 $(function() {
        $('.skdslider').skdslider({ 'delay': 5000, 'fadeSpeed': 800, 'showNextPrev': true, 'showPlayButton': true, 'autoStart': true });
        $('.slide-navs li.slide-nav-0').children().append("<img src='img/desktop/fullscreen7_thumb.jpg'>")
        $('.slide-navs li.slide-nav-1').children().append("<img src='img/desktop/fullscreen3_thumb.jpg'>")
        $('.slide-navs li.slide-nav-2').children().append("<img src='img/desktop/fullscreen6_thumb.jpg'>")
        $('.slide-navs li.slide-nav-3').children().append("<img src='img/desktop/fullscreen5_thumb.jpg'>")
        $('.slide-navs li.slide-nav-4').children().append("<img src='img/desktop/fullscreen9_thumb.jpg'>")
        
        $('.slide-navs li a').fadeOut()
        $('.slide-navs li').hover(function() {
            $(this).children().stop().fadeIn()

        }, function() {
            $(this).children().stop().fadeOut()
        });   


        //$('#leftnav').removeClass('.smallwidth');


    });
</script>


</body>
</html>