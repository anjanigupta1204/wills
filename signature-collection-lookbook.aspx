﻿<%@ page language="C#" autoeventwireup="true" inherits="signature_collection_lookbook, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head id="Head1">
    <title>Wills Lifestyle</title>
    <meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
    <meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />
    <!--Favicon for the website-->
    <link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
    <!--Main CSS, containing struction and formating-->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/slider.css" rel="stylesheet" type="text/css" />
    <link href="lightbox/facebox.css" rel="stylesheet" type="text/css" />
    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
        rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/fakeLoader.css">
</head>
<body class="mobilebg">
    <form id="form1" runat="server">
    <uc:navigation id="navigation1" runat="server" />
    <!--#########################Body Start Here#########################-->
    <div class="cLeft" align="center">
        <div class="psRel">
            <div style="position: absolute; left: 42px;" id="rghtThumb">
                <ul class="thumblisting">
                    <li style="display: list-item;">
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">RITU KUMAR</strong><br>
                            Printed circular knitted dress in a unique dress silhouette
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">RITU KUMAR</strong><br>
                            Knitted shrug in western silhouette
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            An in trend bell sleeve in a shiny sequenced black embroidery
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            Pink ary embroidary over navy base to give that chic look
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">RITU KUMAR</strong><br>
                            Classic paisley printed dress by Ritu Kumar
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">RITU KUMAR</strong><br>
                            A ceremonial golden embellished embroidered black dress
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">RITU KUMAR</strong><br>
                            Pleated front construction and a stylish silhouette, A must have this season
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            A classic Pankaj & Nidhi creation with an ari embroidery in a new dress silhoutee
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            Printed dress by Pankaj & Nidhi
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            Ari embroidered dress from the house of Pankaj & Nidhi
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            An indo western dress with embroidery inspired from Kashmiri architecture
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">ROHIT GANDHI &amp; RAHUL KHANNA</strong><br>
                            A must have dress from Rohit Gandhi and Rahul Khanna
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">ROHIT GANDHI &amp; RAHUL KHANNA</strong><br>
                            Embellished embroidered black dress
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">ROHIT GANDHI &amp; RAHUL KHANNA</strong><br>
                            Classic RGRK embroidered jacket in a bell sleeve variation
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">RITU KUMAR</strong><br>
                            Navy printed tunic in an asymmetric hem
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            Long tunic in a patchwork amalgamated shiny embroidery
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">PANKAJ &amp; NIDHI</strong><br>
                            signature embroidered tunic
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">ROHIT GANDHI &amp; RAHUL KHANNA</strong><br>
                            Classic ombre style with shiny black sequence embroidery at sleeves
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">ROHIT GANDHI &amp; RAHUL KHANNA</strong><br>
                            Classic ombre style with shiny black sequence embroidery at neck
                        </p>
                    </li>
                    <li>
                        <img src="collection/signature/signature.gif">
                        <p>
                            <strong class="size16 mtop30">ROHIT GANDHI &amp; RAHUL KHANNA</strong><br>
                            Ombre tunic with sequenced embroidery at neck
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="cRight">
        <div class="abmain">
            <ul id="cslider" class="sliderlisting">
                <li><a href="collection/SCL/large/1.jpg" id="A1" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#1">
                    <img src="img/spacer.png"></a><img src="collection/SCL/1.jpg"></li>
                <li><a href="collection/SCL/large/2.jpg" id="A2" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#2">
                    <img src="img/spacer.png"></a><img src="collection/SCL/2.jpg"></li>
                <li><a href="collection/SCL/large/3.jpg" id="A3" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#3">
                    <img src="img/spacer.png"></a><img src="collection/SCL/3.jpg"></li>
                <li><a href="collection/SCL/large/4.jpg" id="A4" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#4">
                    <img src="img/spacer.png"></a><img src="collection/SCL/4.jpg"></li>
                <li><a href="collection/SCL/large/5.jpg" id="A5" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#5">
                    <img src="img/spacer.png"></a><img src="collection/SCL/5.jpg"></li>
                <li><a href="collection/SCL/large/6.jpg" id="A6" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#6">
                    <img src="img/spacer.png"></a><img src="collection/SCL/6.jpg"></li>
                <li><a href="collection/SCL/large/7.jpg" id="A7" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#7">
                    <img src="img/spacer.png"></a><img src="collection/SCL/7.jpg"></li>
                <li><a href="collection/SCL/large/8.jpg" id="A8" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#8">
                    <img src="img/spacer.png"></a><img src="collection/SCL/8.jpg"></li>
                <li><a href="collection/SCL/large/9.jpg" id="A9" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#9">
                    <img src="img/spacer.png"></a><img src="collection/SCL/9.jpg"></li>
                <li><a href="collection/SCL/large/10.jpg" id="A10" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#10">
                    <img src="img/spacer.png"></a><img src="collection/SCL/10.jpg"></li>
                <li><a href="collection/SCL/large/11.jpg" id="A11" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#11">
                    <img src="img/spacer.png"></a><img src="collection/SCL/11.jpg"></li>
                <li><a href="collection/SCL/large/12.jpg" id="A12" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#12">
                    <img src="img/spacer.png"></a><img src="collection/SCL/12.jpg"></li>
                <li><a href="collection/SCL/large/13.jpg" id="A13" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#13">
                    <img src="img/spacer.png"></a><img src="collection/SCL/13.jpg"></li>
                <li><a href="collection/SCL/large/14.jpg" id="A14" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#14">
                    <img src="img/spacer.png"></a><img src="collection/SCL/14.jpg"></li>
                <li><a href="collection/SCL/large/15.jpg" id="A15" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#15">
                    <img src="img/spacer.png"></a><img src="collection/SCL/15.jpg"></li>
                <li><a href="collection/SCL/large/16.jpg" id="A16" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#16">
                    <img src="img/spacer.png"></a><img src="collection/SCL/16.jpg"></li>
                <li><a href="collection/SCL/large/17.jpg" id="A17" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#17">
                    <img src="img/spacer.png"></a><img src="collection/SCL/17.jpg"></li>
                <li><a href="collection/SCL/large/18.jpg" id="A18" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#18">
                    <img src="img/spacer.png"></a><img src="collection/SCL/18.jpg"></li>
                <li><a href="collection/SCL/large/19.jpg" id="A19" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#19">
                    <img src="img/spacer.png"></a><img src="collection/SCL/19.jpg"></li>
                <li><a href="collection/SCL/large/20.jpg" id="A20" rel="disable-zoom: true;" class="MagicZoomPlus collectionzoom"
                    name="#20">
                    <img src="img/spacer.png"></a><img src="collection/SCL/20.jpg"></li>
            </ul>
            <div class="thumbnails">
                <ul>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/1.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/2.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/3.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/4.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/5.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/6.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/7.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/8.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/9.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/10.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/11.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/12.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/13.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/14.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/15.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/16.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/17.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/18.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/19.jpg" /></div>
                    </li>
                    <li>
                        <div>
                            <img src="collection/SCL/Thumbnail/20.jpg" /></div>
                    </li>
                </ul>
            </div>
            <!--<div class="shopnwBtn"><a href="http://www.shopwillslifestyle.com/look-book.html" target="_blank">Shop Now<span></span></a></div>-->
        </div>
    </div>
    <!--#########################Body End Here#########################-->

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>

    <script src="js/loader.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

    <script type="text/javascript" src="js/customjs.js"></script>

    <script type="text/javascript" src="lightbox/facebox.js"></script>

    <script type="text/javascript" src="js/abslider_lookbook.js"></script>

    <!--Zoom start here-->

    <script type="text/javascript" src="Gallery/magiczoomplus.js"></script>

    <link rel="stylesheet" href="Gallery/magiczoomplus.css" type="text/css" />

    <script type="text/javascript" src="js/master.js"></script>

    <script type="text/javascript">
$(function() {
	$('#cslider').cslider();
// end of main function
});
    </script>

    </form>
</body>
</html>
