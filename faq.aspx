




<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India�s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />

<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css"></head>
<body>
    <form name="form1" method="post" action="FAQ.aspx" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJLTczOTI2NTcyZGRk4EdnU97uWBoKx9sGyzjQskB8+g==" />
</div>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="0F4F4B68" />
</div>
   
<div class="fakeloader"></div>
<div class="commonnav">
<!-- Paste this code after body tag -->
<!--	<div class="se-pre-con"></div> -->
	<!-- Ends -->
<div class="left-nav" id="leftnav">
  <div class="wills_logo"><a href="index.aspx"><img src="img/wills_lifestyle.jpg" class="icon-big"> <img src="img/wills_logo.gif" class="icon-small"></a></div>
  <nav class="hideonmobile">
    <ul>
      <li><a href="classic.aspx" data-toggle="tooltip" title="Collection" data-placement="right"><span>Collection</span></a></li>
      <li><a href="lookbook.aspx" data-toggle="tooltip" title="Earth Collection" data-placement="right"><span>Earth Collection</span></a></li>
      <li><a href="http://www.shopwillslifestyle.com" target="_blank" data-toggle="tooltip" title="Shop Online" data-placement="right"><span>Shop Online</span></a></li>
      <li><a href="store.aspx" data-toggle="tooltip" title="Store Locator" data-placement="right"><span><img src="img/store-locator-Nw-icon.png" /></span></a></li>
      <li><a href="gifting.aspx" data-toggle="tooltip" title="Store Gifting" data-placement="right"><span>Gifting</span></a></li>
      <li><a href="club-itc.aspx" data-toggle="tooltip" title="Club ITC" data-placement="right"><span>Club ITC</span></a></li>
      <li><a href="video-gallery.aspx" data-toggle="tooltip" title="Video Gallery" data-placement="right"> <span>Video Gallery</span></a></li>
      <li><a href="contactus.aspx" data-toggle="tooltip" title="Contact Us" data-placement="right"><span>Contact Us</span></a></li>
      
    </ul>
  </nav>
  <div class="socialdiv">
    <ul>
      <li><a href="https://www.facebook.com/willslifestyleonline" target="_blank"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>
      <li><a href="https://twitter.com/willslifestyle" target="_blank"><i class="fa fa-twitter"></i> <span>Twitter</span></a></li>
       <li><a href="http://www.instagram.com/willslifestyle" target="_blank"><i class="fa fa-instagram"></i> <span>Instagram</span></a></li>
      <li><a href="http://www.pinterest.com/wlsonline/" target="_blank"><i class="fa fa-pinterest"></i> <span>Pinterest</span></a></li>
      <li><a href="http://www.youtube.com/willslifestyleonline" target="_blank"><i class="fa fa-youtube"></i> <span>Youtube</span></a></li>
     </ul>
  </div>
  <div class="itclogo"><a href="http://www.itcportal.com/" target="_blank"><img src="img/ITCLogo.gif"></a></div>
</div>
<!-- end left nav -->


<!-- Navigation --> 
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
  <ul class="sidebar-nav">
    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
    <li> Wills Lifestyle</li>
    <li><a href="profile.aspx">About us</a></li>
    <li><a href="store.aspx">Store Locator</a></li>
    
    <li><a href="club-itc.aspx">Club ITC</a></li>
    <li><a href="gifting.aspx">Gifting</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li> Collection</li>
   
    <li><a href="personalcare.aspx"> Personal Care</a></li>
    <li><a href="lookbook.aspx"> Earth Collection</a></li>
    <li><a href="http://www.shopwillslifestyle.com" target="_blank"> Shop Online</a></li>
    <li><a href="video-gallery.aspx"> Video Gallery</a></li>
  </ul>
  <ul class="sidebar-nav">
    <li>Customer Service</li>
    <li><a href="contactus.aspx">Contact Us</a></li>
    <li><a href="http://feedback.willslifestyle.com/default.aspx?bid=1" target="_blank">Feedback</a></li>
    <li><a href="return-policy.aspx">Return Policy</a></li>
    <li><a href="terms-use.aspx">Terms of use</a></li>
    <li><a href="http://www.itcportal.com/about-itc/policies/privacy-policy.aspx" target="_blank">Privacy Policy</a></li>
    <li><a href="FAQ.aspx">FAQs</a></li>
    <li><a href="sitemap.aspx">Sitemap</a></li>
  </ul>
</nav>

<img src="img/copyright.png" class="copyright">

</div>


<!--#########################Body Start Here#########################-->
  <div class="pageWidth"><section id="wrapper">
     
     <div class="container-fluid padding-0">
     <div class="row">
     	<div class="col-md-8 padding-0">
        <div class="about-left">
        	<div class="leftinner">
            <h1 class="black35">FAQs</h1>
           <nav class="famTreb pbprc"> 
                    	<a href="index.html">Home</a> | FAQs
                    </nav>
            
            <div class="about-content content">
           
           <div class="willsaccordionfaq">
           <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                 Products and Advertising
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body">
              <div class="scroll-content">
              <div class="faqbline">
              <h3><strong>Do you have a catalogue to showcase your product range?</strong></h3>
               <p>Please visit <a href="#" target="_blank">Lookbook</a></p>
              </div>
                <div class="faqbline">
               <h3><strong>Do you accept outside product/ marketing ideas?</strong></h3>
               <p>While we appreciate people's creativity and interest in designing and marketing Wills Lifestyle products, we do not accept unsolicited ideas. We have a staff of marketing, design and product development professionals dedicated exclusively to creating and marketing products.</p>
             </div>
             <div class="faqbline">
               <h3><strong>How can I appear in an advertisement for your company?</strong></h3>
               <p>Our advertising agency enlists the help of model agents when they produce our advertising. They suggest that anyone interested in modeling should contact a leading model agency. No specific model agency is used exclusively for our advertising.</p>
                </div>
                <div class="faqbline">
               <h3><strong>What is the product range being stocked currently in your stores?</strong></h3>
               <p>Currently showcased at Wills Lifestyle is the Autumn Winter'17 Collection, featuring a complete wardrobe of work wear, relaxed wear, club wear, designer wear, accessories and personal care products by Essenza Di Wills, Fiama Di Wills & Vivel.</p>
              </div>
              
               
               
        </div>
        
        
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                  Our Stores
                </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
              <div class="panel-body">
              
              <div class="scroll-content">
              
              <div class="faqbline">
              <h3>Where can I find a Wills Lifestyle store in my area / city?</h3>
                <p> We have a presence through Wills Lifestyle stores across the country. You can find the detailed addresses and contact details of these stores in the <a href="#" target="_blank">Our Stores</a> section on this site.</p>
                 </div>
                
                <div class="faqbline">
              <h3>Do you have similar products available in all the Wills Lifestyle stores?</h3>
                <p>The products sold at different stores are more or less similar. However, there could be some differences due to climatic conditions of certain markets. For e.g, we sell the heavy winter clothing only at our stores in winter markets. Other factors such as specific market preference and taste are also taken into account.</p>
                </div>
                
         
                
                </div>
                
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                  Purchase & Returns
                </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
              <div class="panel-body">
              
               <div class="scroll-content">  
                    <div class="faqbline">
                        <h3> Can I purchase your products on-line?</h3>
                        <p>Certainly, <a href="http://www.shopwillslifestyle.com/" target="_blank">click here</a> to buy Wills Lifestyle garments and accessories online.</p>
                    </div>
                    <div class="faqbline">
                        <h3>What are the accepted methods of payment at your stores?</h3>
                        <p>We accept payment made through Cash or Credit Cards. All major credit cards are accepted at our stores.</p>
                    </div>
                    <div class="faqbline">
                        <h3>In case I change my mind about the colour/ size picked up, can I have it exchanged?</h3>
                        <p>In case you change your mind about a colour or style, you can have the unused garment exchanged at any of our Wills Lifestyle stores within 30 days.</p>
                    </div>
                    <div class="faqbline">
                        <h3>Can I return or exchange a product by mail?</h3>
                        <p>We accept garments for return/exchange only at our stores, and not by mails.</p>
                    </div>
                    <div class="faqbline">
                        <h3>What is your Return policy?</h3>
                        <p>We make every effort to ensure that you truly enjoy the experience of each garment purchased from us. In the unlikely event that you are not satisfied with the quality of our garment, you can bring it to our stores and fill in a product feedback form. The garment will be sent for Quality Testing and our Customer Care will advise you with its decision within 21 working days. There is no provision of immediate exchange in case of product quality issues. Click here for details on our Exchange and Return policy.</p>
                    </div>  
                    <div class="faqbline">
                        <h3>Do you have any special offers/ schemes for customers who buy frequently from your stores?</h3>
                        <p>We truly value our patrons and therefore make every effort to ensure that they enjoy a very satisfying shopping experience. Visit clubitc.in to know more about our program. </p>
                    </div>  
                      <div class="faqbline">
                        <h3>To whom should i contact for complaints and queries?</h3>
                        <p>For complaint and queries You can e-mail us at customercare.executive@itc.in or call us at 0124-4101010 from Monday to Friday 9:00 AM to 5:00 PM</p>
                    </div>  
                          </div>
                
              </div>
            </div>
          
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                 Trade enquiries
                </a>
              </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
              <div class="panel-body">
              
               <div class="scroll-content">  
                    <div class="faqbline">
                        <h3> How can I apply to become an authorised retailer for your company?</h3>
        <p>For any such trade enquiries, you can get in touch with Customer Care at customercare.executive@itc.in or contact us at 0124-4101010</p>
        
                    </div>
                    
                      
                          </div>
                
              </div>
            </div>
            
          
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                Contact us
                </a>
              </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
              <div class="panel-body">
              
               <div class="scroll-content">  
                    <div class="faqbline">
                        <h3>How do I get information on your company for a project?</h3>
        <p>There is a great deal of information about our business on the web-site. You could also visit the ITC limited web-site, www.itcportal.com for any further information that you need.</p>
        </div>
        <div class="faqbline">
        <h3>What are the various ways by which I can contact you?</h3>
        <p>You can e-mail us at <a href="mailto:customercare.executive@itc.in" target="_blank">customercare.executive@itc.in</a> or call us at 0124-4101010 from Monday to Friday 9:00 AM to 5:00 PM
        </div>
                    </div>
                    
                      
                          </div>
                
              </div>
            </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
               Navigation on our website
                </a>
              </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
              <div class="panel-body">
              
               <div class="scroll-content">  
                    <div class="faqbline">
                        <h3> Which browser versions work best with Willslifestyle.com?</h3>
        <p>The site works best with Internet Explorer 8.0 and above, Mozilla Firefox 21.0, Google Chrome 27.0, and Safari 6.0.</p>
        </div>
        <div class="faqbline">
        <h3>What are the optimal monitor screen size and colour settings for willslifestyle.com?</h3>
        <p>The best colour settings for the site are 16 bit and 32 bit colours and the setting of 1024 x 768 and above pixels resolution.</p>
        
                    </div>
                    
                      
                          </div>
                
              </div>
            </div>
            
          
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                 HR & Careers
                </a>
              </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
              <div class="panel-body">
              
               <div class="scroll-content">  
                    <div class="faqbline">
                        <h3> I am unable to submit my resume on-line, what do I do ?</h3>
        <p>In case you are having any difficulty during submission of your resume online, please send in your resume as a word document to <a href="mailto:hr.1rbd@itc.in">hr.lrbd@itc.in</a></p>
        
                    </div>
                    
                      
                          </div>
                
              </div>
            </div>
            
          
          </div>
  
</div>
</div>
           </div>
           
         
           
            </div>
        </div>
        </div>
       
        <div class="col-md-4  padding-0">
        <div class="about-right">
        <img src="img/club-itc.jpg"></div></div>
     </div>
     </div>
  </section></div>
<!--#########################Body End Here#########################-->  
<script src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>

<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<!-- custom scrollbar plugin -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>


<script type="text/javascript">

	$(document).on('show','.accordion', function (e) {
         //$('.accordion-heading i').toggleClass(' ');
         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
        //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
    });
		
	</script>
    
    <script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
});
$(window).trigger('resize');

</script>

	<script>
	
	
	$(document).ready(function() {
  function setScroll() {
	  if ($(window).width() > 992) {
		  
    windowHeight = $(window).innerHeight() - 160;
    	$('.about-content').css('max-height', windowHeight);
		$('.scroll-content').css('height', windowHeight);
		$(".scroll-content").mCustomScrollbar({
					theme:"minimal"
		});	
	  }
	  else{
		  $('#wrapper').height($(window).height());
		  $('.about-content').css('max-height','none');
		$('.scroll-content').css('height', 'auto');
		$(".scroll-content").mCustomScrollbar("disable",true);
		
	  }
  };
  setScroll();
  
  $(window).resize(function() {
    setScroll();
  });
});
		
		



	</script>
    </form>
</body>
</html>
