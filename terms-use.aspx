﻿<%@ page language="C#" autoeventwireup="true" inherits="terms_use, App_Web_b26udrjz" %>
<%@ Register TagPrefix="uc" TagName="navigation"  Src="~/UserControl/Navigation.ascx" %>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome to Wills Lifestyle</title>
<meta name="Description" content="India’s most admired fashion brand, Wills Lifestyle offers the customer a complete wardrobe of fashion apparel and accessories." />
<meta name="Keywords" content="Wills Lifestyle Products, fashion brand, premium fashion brand, Wills Signature, Wills Classic, Wills Sport, Wills Clublife, Essenza Di Wills, Fiama Di Wills, designer wear, work wear, relaxed wear, evening wear, fashion accessories" />

<!--Favicon for the website-->
<link rel="shortcut icon" type="image/x-icon" href="img/wls.ico" />
<!--Main CSS, containing struction and formating-->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!-- Custom Fonts -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/loader.css">   
</head>

<body>
    <form id="form1" runat="server">
  

<uc:navigation id="navigation1"  runat="server"   />

<!--#########################Body Start Here#########################-->
  <div class="pageWidth"><section id="wrapper">
     
     <div class="container-fluid padding-0">
     <div class="row">
     	<div class="col-md-8 padding-0">
        <div class="about-left">
        	<div class="leftinner">
            <h1 class="black35">Terms-use</h1>
           <nav class="famTreb pbprc"> 
                    	<a href="index.html">Home</a> | Terms use
                    </nav>
            
            <div class="about-content content">
            <div class="scroll-content">
            <div class="terms">
            	<div class="row">
            	<div class="col-md-12">
                		<div class="termsuse">
                        	<ul>
                            	<li><span class="redcolor">Terms and Conditions of Website Use</span><br><br>

This page states the Terms and Conditions under which you (Visitor) may visit this Website. Please read this page carefully. If you do not accept the Terms and Conditions stated here, we would request you to exit this site. ITC Limited., any of its business divisions and / or its subsidiaries, associate companies or subsidiaries to subsidiaries or such other investment companies (in India or abroad) reserve their respective rights to revise these Terms and Conditions at any time by updating this posting. You should visit this page periodically to re-appraise yourself of the Terms and Conditions, because they are binding on all users of this Website.</li>
                                <li><span class="redcolor">Use of Content</span><br><br>
All logos, brands and marks appearing in this site, except as otherwise noted, are properties either owned, or used under license, by ITC Limited and / or its associate entities who feature on this website. The use of these properties or any other content on this site, except as provided in these terms and conditions or in the site content, is strictly prohibited.
You may not sell or modify the content of this Website or reproduce, display, publicly perform, distribute, or otherwise use the materials in any way for any public or commercial purpose without the respective organisation's or entity's written permission.</li>
                                <li><span class="redcolor">Acceptable Site Use</span><br><br>
<span class="bolder">(A)Security Rules </span>: Visitors are prohibited from violating or attempting to violate the security of the Website, including, without limitation, (1) accessing data not intended for such user or logging into a server or account which the user is not authorized to access, (2) attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorization, (3) attempting to interfere with service to any user, host or network, including, without limitation, via means of submitting a virus or "trojan horse" to the Website, overloading, "flooding", "mail bombing" or "crashing", or (4) sending unsolicited electronic mail, including promotions and/or advertising of products or services. Violations of system or network security may result in civil or criminal liability. ITC Limited and / or its associate entities will have the right to investigate occurrences that they suspect as involving such violations and will have the right to involve, and cooperate with, law enforcement authorities in prosecuting users who are involved in such violations. <br>
<br>


<span class="bolder">(B) General Rules</span> : Visitors may not use the Website in order to transmit, distribute, store or destroy material (a) that could constitute or encourage conduct that would be considered a criminal offence or violate any applicable law or regulation, (b) in a manner that will infringe the copyright, trademark, trade secret or other intellectual property rights of others or violate the privacy or publicity of other personal rights of others, or (c) that is libelous, defamatory, pornographic, profane, obscene, threatening, abusive or hateful.</li>
                                <li><span class="redcolor">Links to/from other Websites</span><br><br>
This Website contains links to other Websites. These links are provided solely as a convenience to you. Wherever such link/s lead to sites which do not belong to ITC Limited and / or its associate entities, ITC Limited is not responsible for the content of linked sites and does not make any representations regarding the correctness or accuracy of the content on such Websites. If you decide to access such linked Websites, you do so at your own risk. Similarly, this Website can be made accessible through a link created by other Websites. Access to this Website through such link/s shall not mean or be deemed to mean that the objectives, aims, purposes, ideas, concepts of such other Websites or their aim or purpose in establishing such link/s to this Website are necessarily the same or similar to the idea, concept, aim or purpose of our website or that such links have been authorised by ITC Limited and / or its associate entities. We are not responsible for any representation/s of such other Websites while affording such link and no liability can arise upon ITC Limited and / or its associate entities consequent to such representation, its correctness or accuracy. In the event that any link/s afforded by any other Website/s derogatory in nature to the objectives, aims, purposes, ideas and concepts of this Website is utilised to visit this Website and such event is brought to the notice or is within the knowledge of ITC Limited and / or its associate entities, civil or criminal remedies as may be appropriate shall be invoked.</li>
                                <li><span class="redcolor">Indemnity</span><br><br>
You agree to defend, indemnify, and hold harmless ITC Limited and/or its associate entities, their officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from your use of the Website material or your breach of these terms and conditions of Website use.</li>
                                <li><span class="redcolor">Liability</span><br><br>
While all reasonable care has been taken in providing the content on this Website, ITC Limited. and / or its associate entities shall not be responsible or liable as to the completeness or correctness of such information and any or all consequential liabilities arising out of use of any information or contents on this Website. No warranty is given that the Website will operate error-free or that this Website and its server are free of computer viruses or other harmful mechanisms. If your use of the Website results in the need for servicing or replacing equipment or data, ITC Limited and / or its associate entities are not responsible for those costs. The website is provided on an 'as is' basis without any warranties either express or implied whatsoever. ITC Limited. and / or its associate entities, to the fullest extent permitted by law, disclaims all warranties, including non-infringement of third parties rights, and the warranty of fitness for a particular purpose and makes no warranties about the accuracy, reliability, completeness, or timeliness of the content, services, software, text, graphics, and links.</li>
                                <li><span class="redcolor">Disclaimer of Consequential Damages</span><br><br>
In no event shall ITC Limited, or any parties, organisations or entities associated with the corporate brand name ITC or otherwise, mentioned at this Website be liable for any damages whatsoever (including, without limitations, incidental and consequential damages, lost profits, or damage to computer hardware or loss of data information or business interruption) resulting from the use or inability to use the Website and the Website material, whether based on warranty, contract, tort, or any other legal theory, and whether or not, such organisations or entities were advised of the possibility of such damages.</li>
                                <li>In the design of our website, we have taken care to draw your attention to this privacy policy so that you are aware of the terms under which you may decide to share your personal information with us. Accordingly, should you choose to share your personal information with us, ITC Limited will assume that you have no objections to the terms of this privacy policy.</li>
                                
                            </ul>
                        </div>
                </div>
                </div>
            </div>
            	
           </div>
            </div>
        </div>
        </div>
        </div>
        
        <div class="col-md-4 padding-0">
        <div class="about-right">
        <img src="img/Clubitc2.jpg"></div></div>
     </div>
     </div>
  </section></div>
<!--#########################Body End Here#########################-->  
<script src="js/jquery-2.2.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/loader.js"></script>
<script type="text/javascript" src="js/customjs.js"></script>
<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
<!-- custom scrollbar plugin -->
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
$(window).resize(function() {
	$('#wrapper').height($(window).height());
});
$(window).trigger('resize');

</script>

	<script>
	
	
	$(document).ready(function() {
  function setScroll() {
	  if ($(window).width() > 992) {
		  
    windowHeight = $(window).innerHeight() - 160;
    	$('.about-content').css('max-height', windowHeight);
		$('.scroll-content').css('height', windowHeight);
		$(".scroll-content").mCustomScrollbar({
					theme:"minimal"
		});	
	  }
	  else{
		  $('#wrapper').height($(window).height());
		  $('.about-content').css('max-height','none');
		$('.scroll-content').css('height', 'auto');
		$(".scroll-content").mCustomScrollbar("disable",true);
		
	  }
  };
  setScroll();
  
  $(window).resize(function() {
    setScroll();
  });
});
		
		



	</script>



    </form>
</body>
</html>